<?
   require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
   		use Bitrix\Main,
   		Bitrix\Main\Localization\Loc,
       Bitrix\Main\Page\Asset;
       IncludeTemplateLangFile(__FILE__);
   $APPLICATION->SetTitle("M�� �������");

   ?>
<style>
   .sidebar_menu .sidebar_menu_body{
   display: none;
   }
   .file_question{
   display: none !important;
   }
   .file-selectdialog-switcher span {
   font-size: 10px;
   color: #9f9f9f;
   padding: 0;
   height: auto;
   line-height: 1;
   border: none;
   -webkit-box-shadow: none;
   box-shadow: none;
   background: none;
   position:absolute;
   right: 20px;
   bottom: 10px;
   }
   .closequestion{
   margin-left: 50px;
   font-size: 12px;
   color: #8f2839;
   }
   .attach{
   float: right;
   }
</style>
<section class="personal_area">
   <div class="workspace">
      <?$APPLICATION->IncludeComponent(
         "bitrix:breadcrumb",
         "main",
         Array(
         	"PATH" => "",
         	"SITE_ID" => "s1",
         	"START_FROM" => "0"
         )
         );
				 global $USER;
         $id_user = $USER->GetID(); // ID ������������
         if (empty($id_user))
         {
         	$APPLICATION->AuthForm('', false, false, 'N', false);
         }
         else
         {
					 //��������, ����������� �� ������������ ������ �����������
         $property_enums = CIBlockPropertyEnum::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>5, "PROPERTY_ID" => "23"));
         while ($element = $property_enums->GetNext()) {
         $moderators[] = (int)($element["VALUE"]);
         }
         if ( CSite::InGroup( $moderators ) ){ $isAdmin = true;}

				 	$rsUser = CUser::GetByID($USER->GetID());
				 $currentUser = $rsUser->Fetch();
         //���������� ������� ������������
         $arFilter = Array("USER_ID" => $id_user);
         $sql = CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $arFilter);
         while ($result = $sql->Fetch()){
         if($result["CANCELED"]=="Y"){$userordescancel++;}elseif($result["STATUS_ID"]=="F"){$userordesfinish++;}elseif($result["STATUS_ID"]=="Y"){$userordesshipped++;}else{$userordesprocess++;}
         $userordestotal++;
         }
         //������ ������ � ��������� faq
         if (!$isAdmin){
         $faqcount = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 5, "NAME"=>$USER->GetFullName(), 'ACTIVE'=>"Y"), array(), false, array('ID', 'NAME'));
         }
         else {
         $faqcount = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 5), array(), false, array('ID', 'NAME'));
         }
         ?>
      <div class="pa_line_block">
         <a href="/personal/order/" class="pal_block">
            <div class="pal_inner_block pib1">
               <p class="pal_name"><?= getMessage('SPOL_CHECK_PIB1_NAME')?></p>
               <p class="pal_text"><?= getMessage('SPOL_CHECK_PIB1_TEXT')?></p>
               <span class="pal_namber"><?=$userordesshipped + $userordesprocess + $userordescancel?></span>
            </div>
         </a>
         <a href="/personal/order/index.php?filter_history=Y" class="pal_block">
            <div class="pal_inner_block pib2">
               <p class="pal_name"><?= getMessage('SPOL_CHECK_PIB2_NAME')?></p>
               <p class="pal_text"><?= getMessage('SPOL_CHECK_PIB2_TEXT')?></p>
            </div>
         </a>
         <a href="/personal/faq/" class="pal_block">
            <div class="pal_inner_block pib3">
               <p class="pal_name"><?= getMessage('SPOL_CHECK_PIB3_NAME')?></p>
               <p class="pal_text"><?= getMessage('SPOL_CHECK_PIB3_TEXT')?></p>
               <span class="pal_namber"><?=$faqcount?></span>
            </div>
         </a>
         <a href="/personal/questions/" class="pal_block">
            <div class="pal_inner_block pib4">
               <p class="pal_name"><?= getMessage('SPOL_CHECK_PIB4_NAME')?></p>
               <p class="pal_text"><?= getMessage('SPOL_CHECK_PIB4_TEXT')?></p>
            </div>
         </a>
      </div>
      <!--================================================================-->
      <section class="delivery_s1 pers_area">
         <div class="del_s1_left">
            <div class="pa_navigation">
               <a href="/personal/profile/"><?= getMessage('PA_NAVIGATION_PROFILE')?></a>
               <a href="/personal/passwd/"><?= getMessage('PA_NAVIGATION_PASSWORD')?></a>
               <a href="/personal/address/"><?= getMessage('PA_NAVIGATION_ADDRESS')?></a>
               <a href="<?=$APPLICATION->GetCurPageParam("logout=yes", Array("login"))?>"><?= getMessage('PA_NAVIGATION_LOGOUT')?></a>
            </div>
            <?/* ������ �����������, ���������� ��� �������������
               $APPLICATION->IncludeComponent(
               	"bitrix:system.auth.form",
               	"login_order",
               	array(
               		"PROFILE_URL" => "/personal/profile/",
               		"SHOW_ERRORS" => "N",
               		"COMPONENT_TEMPLATE" => "login_order",
               		"REGISTER_URL" => "",
               		"FORGOT_PASSWORD_URL" => ""
               	),
               	false
               );*/?>
         </div>
         <!--del_s1_left-->
				 <div class="margin_right_sidebar">
         <div class="wrap_my_profil">
					 <form class="formx" id="formx<?=$currentUser["ID"]?>" action="ipost.php" method="post" enctype="multipart/form-data">
           <input type="hidden" name="ID" value="<?=$currentUser["ID"]?>">
           <input type="hidden" name="curPhoto" value="<?=$currentUser["PERSONAL_PHOTO"]?>">
               <div class="my_profil_top">
                  <div class="my_profil_photo">
                     <img src="<?=CFile::GetPath($currentUser["PERSONAL_PHOTO"])?>" alt="">
                  </div>
                  <input class="my_profil_download" type="file" name="upfile">
                </div>
                <div class="my_profil_bottom">
                  <div class="wcf_body_line">
                    <p class="wcfbl_name"><span>*</span>���</p>
                     <input required="required" class="wcfb1_input" name="NAME" value="<?=$currentUser["NAME"]?>" placeholder="<?=$currentUser["NAME"]?>" type="text">
                  </div>
                  <div class="wcf_body_line">
                     <p class="wcfbl_name"><span>*</span>�������</p>
                     <input required="required" class="wcfb1_input" name="LAST_NAME" value="<?=$currentUser["LAST_NAME"]?>" placeholder="<?=$currentUser["LAST_NAME"]?>" type="text">
                  </div>
                  <div class="wcf_body_line">
                     <p class="wcfbl_name">��������</p>
                     <input class="wcfb1_input" name="SECOND_NAME" value="<?=$currentUser["SECOND_NAME"]?>" placeholder="<?=$currentUser["SECOND_NAME"]?>" type="text">
                  </div>
                  <div class="wcf_body_line">
                     <p class="wcfbl_name"><span>*</span>����������� �����</p>
                     <input required="required" class="wcfb1_input" name="EMAIL" value="<?=$currentUser["EMAIL"]?>" placeholder="<?=$currentUser["EMAIL"]?>" type="text">
                  </div>
                  <div class="wcf_body_line">
                     <p class="wcfbl_name">��������� �������</p>
                     <input id="tel" class="wcfb1_input" required="required" name="PERSONAL_MOBILE" value="<?=$currentUser["PERSONAL_MOBILE"]?>" placeholder="+7(___)_______" type="text">
                     <script>
                      window.addEventListener("DOMContentLoaded", function() {
                      function setCursorPosition(pos, elem) {
                          elem.focus();
                          if (elem.setSelectionRange) elem.setSelectionRange(pos, pos);
                          else if (elem.createTextRange) {
                              var range = elem.createTextRange();
                              range.collapse(true);
                              range.moveEnd("character", pos);
                              range.moveStart("character", pos);
                              range.select()
                          }
                      }

                      function mask(event) {
                          var matrix = "+7(___)_______",
                              i = 0,
                              def = matrix.replace(/\D/g, ""),
                              val = this.value.replace(/\D/g, "");
                          if (def.length >= val.length) val = def;
                          this.value = matrix.replace(/./g, function(a) {
                              return /[_\d]/.test(a) && i < val.length ? val.charAt(i++) : i >= val.length ? "" : a
                          });
                          if (event.type == "blur") {
                              if (this.value.length == 2) this.value = ""
                          } else setCursorPosition(this.value.length, this)
                      };
                          var input = document.querySelector("#tel");
                          input.addEventListener("input", mask, false);
                          input.addEventListener("focus", mask, false);
                          input.addEventListener("blur", mask, false);
                      });
                        </script>
                  </div>
                  <!-- <div class="wcf_body_line">
                     <p class="wcfbl_name">Skype</p>
                     <input class="wcfb1_input" name="PERSONAL_PAGER" value="<?=$currentUser["PERSONAL_PAGER"]?>" placeholder="<?=$currentUser["PERSONAL_PAGER"]?>" type="text">
                  </div> -->
                  <div class="par_submit">
                     <button class="button2" type="submit">��������� ������</button>
                     <div class="pars_check">
                        <input class="wcfb1_input" type="checkbox" name="" required="required">
                        <span>� ����������, ��� �������� ������ �������� �������, � ��� �������� �� �� ���������.</span>
                     </div>
                  </div>
               </div>
               <!--my_profil_bottom-->
            </form>
         </div>

         </div>
         <!--margin_right_sidebar-->
      </section>
   </div>
   <?}?>
   <!-- <script>
      function call(elem) {
        var msg   = (elem).serialize();
				console.log(elem);
            $.ajax({
              type: 'POST',
              url: 'ipost.php',
              data: msg,
              success: function(data) {
      //location.reload();
               // elem.find('.results').html("<?=getMessage('QUESTIONS_FAQ_SEND')?>");
      //elem.find('.results').html(data);
      console.log(data);
      $('.file-selectdialog').slideUp();
              },
              error:  function(xhr, str){
         alert('�������� ������: ' + xhr.responseCode);

           }
      });
        }
      $(".button2" ).click(function() {
      call($('.formx'));
      });
   </script> -->
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
