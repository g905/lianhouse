<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
		use Bitrix\Main,
		Bitrix\Main\Localization\Loc,
		Bitrix\Main\Page\Asset;
$APPLICATION->SetTitle("M�� �������");
IncludeTemplateLangFile(__FILE__);
?>
<style>
.hide {
	display: none;
}
.sidebar_menu .sidebar_menu_body{
	display: none;
}
.file_question{
	display: none !important;
}
.file-selectdialog-switcher span {
    font-size: 10px;
    color: #9f9f9f;
    padding: 0;
    height: auto;
    line-height: 1;
    border: none;
    -webkit-box-shadow: none;
    box-shadow: none;
    background: none;
	position:absolute;
	right: 20px;
	bottom: 10px;
}
.closequestion{
	margin-left: 50px;
	font-size: 12px;
    color: #8f2839;
}
.attach{
	float: right;
}
    </style> 
<section class="personal_area">
<div class="workspace">
	 <?$APPLICATION->IncludeComponent(
	"bitrix:breadcrumb",
	"main",
	Array(
		"PATH" => "",
		"SITE_ID" => "s1",
		"START_FROM" => "0"
	)
); 
global $USER;
$id_user = $USER->GetID(); // ID ������������
$name_user = $USER->GetFullName();
$adminemail = $USER->GetEmail();

//��������, ����������� �� ������������ ������ �����������
//$moderators = new Array();
$property_enums = CIBlockPropertyEnum::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>5, "PROPERTY_ID" => "23"));
while ($element = $property_enums->GetNext()) {
$moderators[] = (int)($element["VALUE"]);
}

if ( CSite::InGroup( $moderators ) ){ $isAdmin = true;}

//var_dump($isAdmin);

if (empty($id_user))
	{
		$APPLICATION->AuthForm('', false, false, 'N', false);
	}

else
{
//���������� ������� ������������
$arFilter = Array("USER_ID" => $id_user);
$sql = CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $arFilter);
while ($result = $sql->Fetch()){
	if($result["CANCELED"]=="Y"){$userordescancel++;}elseif($result["STATUS_ID"]=="F"){$userordesfinish++;}elseif($result["STATUS_ID"]=="Y"){$userordesshipped++;}else{$userordesprocess++;}
	$userordestotal++;
	}
//echo "<pre>";
//var_dump ($USER->GetFullName());
//echo "</pre>";
//������ ������ � ��������� faq
if (!$isAdmin){
$faqcount = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 5, "NAME"=>$USER->GetFullName()), array(), false, array('ID', 'NAME')); 
$faqclosecount = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 5, "NAME"=>$USER->GetFullName(), 'ACTIVE'=>"N"), array(), false, array('ID', 'NAME')); 
$faqmanagercount = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 5, "NAME"=>$USER->GetFullName(),  "PROPERTY_statusanswer" => 0, 'ACTIVE'=>"Y"), array(), false, array('ID', 'NAME')); 
$faqusercount = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 5, "NAME"=>$USER->GetFullName(),  "PROPERTY_statusanswer" => 1, 'ACTIVE'=>"Y"), array(), false, array('ID', 'NAME')); 
}
else {
$faqcount = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 5), array(), false, array('ID', 'NAME')); 
$faqclosecount = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 5, 'ACTIVE'=>"N"), array(), false, array('ID', 'NAME')); 
$faqmanagercount = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 5, "PROPERTY_statusanswer" => 0, 'ACTIVE'=>"Y"), array(), false, array('ID', 'NAME')); 
$faqusercount = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 5, "PROPERTY_statusanswer" => 1, 'ACTIVE'=>"Y"), array(), false, array('ID', 'NAME')); 
}
?>

					<div class="pa_line_block">
						<a href="/personal/order/" class="pal_block">
							<div class="pal_inner_block pib1">
								<p class="pal_name"><?= getMessage('SPOL_CHECK_PIB1_NAME')?></p>
								<p class="pal_text"><?= getMessage('SPOL_CHECK_PIB1_TEXT')?></p>
								<span class="pal_namber"><?=$userordesshipped + $userordesprocess + $userordescancel?></span>
							</div>
						</a>
						<a href="/personal/order/index.php?filter_history=Y" class="pal_block">
							<div class="pal_inner_block pib2">
								<p class="pal_name"><?= getMessage('SPOL_CHECK_PIB2_NAME')?></p>
								<p class="pal_text"><?= getMessage('SPOL_CHECK_PIB2_TEXT')?></p>
							</div>
						</a>
						<a href="/personal/faq/" class="pal_block">
							<div class="pal_inner_block pib3">
								<p class="pal_name"><?= getMessage('SPOL_CHECK_PIB3_NAME')?></p>
								<p class="pal_text"><?= getMessage('SPOL_CHECK_PIB3_TEXT')?></p>
								<span class="pal_namber"><?=$faqcount?></span>
							</div>
						</a>
						<a href="/personal/questions/" class="pal_block">
							<div class="pal_inner_block pib4">
								<p class="pal_name"><?= getMessage('SPOL_CHECK_PIB4_NAME')?></p>
								<p class="pal_text"><?= getMessage('SPOL_CHECK_PIB4_TEXT')?></p>
							</div>
						</a>
					</div>
<!--================================================================-->
<section class="delivery_s1 pers_area">
				<div class="del_s1_left">
			<div class="pa_navigation">
				<a href="/personal/profile/"><?= getMessage('PA_NAVIGATION_PROFILE')?></a>
				<a href="/personal/passwd/"><?= getMessage('PA_NAVIGATION_PASSWORD')?></a>
				<a href="/personal/address/"><?= getMessage('PA_NAVIGATION_ADDRESS')?></a>
				<a href="<?=$APPLICATION->GetCurPageParam("logout=yes", Array("login"))?>"><?= getMessage('PA_NAVIGATION_LOGOUT')?></a>
			</div>
			<?/* ������ �����������, ���������� ��� �������������
			$APPLICATION->IncludeComponent(
				"bitrix:system.auth.form", 
				"login_order", 
				array(
					"PROFILE_URL" => "/personal/profile/",
					"SHOW_ERRORS" => "N",
					"COMPONENT_TEMPLATE" => "login_order",
					"REGISTER_URL" => "",
					"FORGOT_PASSWORD_URL" => ""
				),
				false
			);*/?>

		</div><!--del_s1_left-->



	<div class="margin_right_sidebar">
							
							<div class="pa_my_questions">
								
								<div class="pamq_top">
									<a class="pamq_a1" href="<?=$APPLICATION->GetCurPage();?>?stat=manager">������� ������ ���������  <span>(<?=$faqusercount?>)</span></a>
									<a class="pamq_a2" href="<?=$APPLICATION->GetCurPage();?>?stat=user">������� ������ ������  <span>(<?=$faqmanagercount?>)</span></a>
									<a class="pamq_a3" href="<?=$APPLICATION->GetCurPage();?>?stat=closed">������ �����  <span>(<?=$faqclosecount?>)</span></a>
									<a class="pamq_a4" href="<?=$APPLICATION->GetCurPage();?>">��� <span>(<?=$faqcount?>)</span></a>
								</div>
								
								<div class="pamq_bottom">
								<?if (($faqcount == 0) and (!$isAdmin)) {
									echo (getMessage('QUESTIONS_FAQ_NONE'));
								}
								else{?>
								<?
//$arSelect = array("ID","NAME","ACTIVE");
if($_REQUEST["stat"]=="manager")
{
	if (!$isAdmin) {$arFilter = array("IBLOCK_ID"=>5, "NAME"=>$USER->GetFullName(), "ACTIVE"=>"Y", "PROPERTY_STATUSANSWER"=>1);}
	else {$arFilter = array("IBLOCK_ID"=>5, "ACTIVE"=>"Y", "PROPERTY_STATUSANSWER"=>1);}

} elseif($_REQUEST["stat"]=="user")
{
	if (!$isAdmin) {$arFilter = array("IBLOCK_ID"=>5, "NAME"=>$USER->GetFullName(), "ACTIVE"=>"Y", "PROPERTY_STATUSANSWER"=>0);}
	else {$arFilter = array("IBLOCK_ID"=>5, "ACTIVE"=>"Y", "PROPERTY_STATUSANSWER"=>0);}
} elseif($_REQUEST["stat"]=="closed")
{
	if (!$isAdmin) {$arFilter = array("IBLOCK_ID"=>5, "NAME"=>$USER->GetFullName(), "ACTIVE"=>"N");}
	else {$arFilter = array("IBLOCK_ID"=>5, "ACTIVE"=>"N");}
} else {
	if (!$isAdmin) {$arFilter = array("IBLOCK_ID"=>5, "NAME"=>$USER->GetFullName());}
	else {$arFilter = array("IBLOCK_ID"=>5);}
}

$rsItems = CIBlockElement::GetList(array("ID"=>"DESC"), $arFilter, false, false, $arSelect);
$rsItems->NavStart(10);
while($ob = $rsItems->GetNextElement())
{
 $arFields = $ob->GetFields();
 $arProps = $ob ->GetProperties();
$rsUser = CUser::GetByID($arProps["manager"]["VALUE"]);

$arUser = $rsUser->Fetch();

$manager = ($arUser["NAME"]." ".$arUser["LAST_NAME"]);
//���������� �������
$faqanswercount = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 10, 'ACTIVE'=>"Y", "PROPERTY_userid"=>$arFields["ID"]), array(), false, array('ID', 'NAME')); 


 //echo "<pre>";
 //var_dump($faqanswercount);
 //echo "</pre>";statusanswer
  switch ($arProps["statusanswer"]["VALUE"]){
	 case "0":
	 $statusanswer="O������ ������ ������";
	 break;
	 case "1":
	 $statusanswer="������� ������ ���������";
	 break;
 }
 switch ($arProps["status"]["VALUE"]){
	 case "0":
	 $status="������ �����";
	 break;
	 case "1":
	 $status="������ ������";
	 break;
 }
 ?>

<?
		/*$iblock_permission = CIBlock::GetPermission($id_user);
		echo "<h1>perm: ".$iblock_permission."</h1>";
		if($iblock_permission >= "W")
		{
		    $DB->StartTransaction();
			    if(!CIBlockElement::Delete($ELEMENT_ID))
			    {
			        $strWarning .= 'Error!';
			        $DB->Rollback();
			    }
			    else {
			        $DB->Commit();
			    }
			
		} else {
			return false;
		}*/
		//echo "<pre>"; print_r($arFields);/*print_r($_REQUEST);*/ echo "</pre>";
		//if($arFields["ID"] == "520"){

		if($_REQUEST["close"] == "Y" && $_REQUEST["el"] != "")
		{
			$obEl = new CIBlockElement;
			$v = $obEl->Update($_REQUEST["el"],array('ACTIVE' => 'N'));
			if(!v)
			{
				echo "still active";
			} else {
				LocalRedirect("/personal/faq/");
			}
		}

               // �������� �������
               /*$DB->StartTransaction();
               if(!CIBlockElement::Delete($arFields["ID"]))
               {
                  $DB->Rollback();
                  echo "error";
               }
               else
               {
                  $DB->Commit();
                  echo "success";
               }*/
        //}
?>

			<div class="chat_block">
				<div class="cb_name_block">
					<p class="cb_name">������ � <span><?=$arFields["ID"]?></span></p>
					<span class="cb_expectation"><?=$statusanswer?></span>
					<span class="cb_themename">����: <?=$arProps["themes"]["VALUE"]?></span>
					<?if($arFields["ACTIVE"] != "N"):?>
						<a class="closequestion" href="?close=Y&el=<?=$arFields['ID']?>">������� ������</a>
					<?else:?>
						<a class="closequestion" style="color: #aaa; cursor: default" href="javascript: void(0)?>">������ ������</a>
					<?endif;?>
					<span class="cb_smallname">��� ��������: <?=$manager?></span>
					<p><?=($arFields["PREVIEW_TEXT"])?></p>
					
					<? 
					foreach ($arProps["files"]["VALUE"] as $file){
						?>
						<? $filename = (CFile::GetFileArray($file)["ORIGINAL_NAME"]);?>
						<p><a href="<?=CFile::GetPath($file);?>"><?=$filename?></a></p>
						<?
					}
					?>
				</div>
				<div class="cb_hide_block">
					<div class="cbhb_top">
						<span class="cb_line2"></span>
						<div class="cbnb_name_block">
							<p class="cbhb_name">��� �� ������� � <span><?=$arFields["ID"]?></span></p>
							<span class="cbnb_status"><?=$status?></span>
						</div>
						<span <?if ($faqanswercount == 0) echo ("style=''")?> class="cbnb_show_answer">�������� ��� ������ (<?=$faqanswercount?>)</span>
						<div class="clear"></div>
					</div>
	
<div class="cbnb_bottom">
<div>
	<!---->
<?
if ($faqanswercount > 0){
$arFilter1 = array("IBLOCK_ID"=>10, 'ACTIVE'=>"Y", "PROPERTY_userid"=>$arFields["ID"]);
$rsItems1 = CIBlockElement::GetList(array("ID"=>"ASC"), $arFilter1, false, false, $arSelect1);
while($ob1 = $rsItems1->GetNextElement())
{	
	$arFields1 = $ob1->GetFields();
	$arProps1 = $ob1 ->GetProperties();
	$rsUser1 = CUser::GetByID($arFields1["CREATED_BY"]);
	$arUser1 = $rsUser1->Fetch();
	$manager1 = ($arUser1["NAME"]." ".$arUser1["LAST_NAME"]);
	$lastuser = $arUser1["ID"];
	
	
	//echo "<pre>";
	//var_dump($arFields1);
	//echo "</pre>";
	//echo CFile::GetPath($arUser1["PERSONAL_PHOTO"]);
	?>
	<div class="cbnb_message">
		<div class="cbnbm_img">
			<img src="<?=CFile::GetPath($arUser1["PERSONAL_PHOTO"])?>" alt="">	
		</div>
		<div class="cbnbm_block_text">
			<p class="cbnbm_name"><?=$manager1?><span class="cbnbm_date"><i class="fa fa-clock-o" aria-hidden="true"></i> <?=$arFields1["DATE_CREATE"]?></span></p>
			<div class="cbnbm_text">
				<?=$arFields1["PREVIEW_TEXT"]?>
				<div class="attach">
				<? 
					foreach ($arProps1["files"]["VALUE"] as $file1){
						?>
						<? $filename1 = (CFile::GetFileArray($file1)["ORIGINAL_NAME"]);?>
						<p><a href="<?=CFile::GetPath($file1);?>"><?=$filename1?></a></p>
						<?
					}
					?>
					</div>
			</div>
		</div>
	</div>
	<?
}
}
?>
</div>

<form class="wrap_question" id="formx<?=$arFields["ID"]?>" action="javascript:void(null);" method="post" enctype="multipart/form-data">
	<div class="bq_elem">
	<p class="results"></p>
		<textarea name="textarea"></textarea>
		<div class="file_question">
			<input style="display:none" class="file_question" name="name" type="file">
			
		</div>
		<?$APPLICATION->IncludeComponent("bitrix:main.file.input", "drag_n_drop",
						   array(
							  "INPUT_NAME"=>"DOPFILE".$arFields["ID"],
							  "MULTIPLE"=>"Y",
							  "MODULE_ID"=>"main",
							  "MAX_FILE_SIZE"=>"",
							  "ALLOW_UPLOAD"=>"F", 
							  "ALLOW_UPLOAD_EXT"=>""
						   ),
						   false
						);?>
	</div>
	<?if ($isAdmin) {?>
	<input type="hidden" name="FAQANSWERCOUNT" value="<?=$faqanswercount?>">
	<?}?>
	<input type="hidden" name="ADMINEMAIL" value="<?=$adminemail?>">
	<input type="hidden" name="USERID" value="<?=$id_user?>">
	<input type="hidden" name="LASTUSER" value="<?=$lastuser?>">
	<input type="hidden" name="PARENTID" value="<?=$arFields["ID"]?>">
	<input type="hidden" name="POSTID" value="<?=$arFields["ID"]?>">
	<input type="hidden" name="USERNAME" value="<?=$name_user?>">
	<button class="button2" type="submit">������ ������</button>
	<div class="clear"></div>				
</form>



</div>
										</div>
										<div class="cb_hide_show">
											<span class="cb_line"></span>
											���������� ���<i class="fa fa-angle-down" aria-hidden="true"></i>
										</div>	

									</div><!--chat_block-->



									<?}
									//echo $rsItems->NavPrint("�������","true","","pagination.php");
									$NAV_STRING = $rsItems->GetPageNavStringEx($navComponentObject, "�������", "/local/templates/main/components/bitrix/system.pagenavigation/catalog", 'N');
									echo $NAV_STRING."<br>";
 

								?>

								<?}//����� ������� � ������� ��������?>

								</div><!--pamq_bottom-->
							</div>



</div><!--margin_right_sidebar-->
</section>
</div>
<?}?>
    <script type="text/javascript" language="javascript">
     	function call(elem) {
     	  var msg   = (elem).serialize();
		  
            $.ajax({
              type: 'POST',
              url: 'ipost.php',
              data: msg,
              success: function(data) {
								location.reload();
               // elem.find('.results').html("<?=getMessage('QUESTIONS_FAQ_SEND')?>");
				//elem.find('.results').html(data);
				//console.log(data);
				$('.file-selectdialog').slideUp();
              },
              error:  function(xhr, str){
    	    alert('�������� ������: ' + xhr.responseCode);
         }
			});
        }
		$(".button2" ).click(function() {
	  call($( this ).parent());
	});
    </script>
 </section>
 <?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>