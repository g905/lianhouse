<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php"); 
		use Bitrix\Main,
		Bitrix\Main\Localization\Loc,
		Bitrix\Main\Page\Asset;
		IncludeTemplateLangFile(__FILE__);
$APPLICATION->SetTitle(getMessage('PERSONAL__ADDRESSES_TITLE')); 
?>
			<style>
					.sidebar_menu .sidebar_menu_body{
						display: none;
					}
					.empty_field {
  			background-color: red !important;
					}
			</style>
			<section class="personal_area">
				<div class="workspace">

<?$APPLICATION->IncludeComponent(
         "bitrix:breadcrumb",
         "main",
         Array(
         	"PATH" => "",
         	"SITE_ID" => "s1",
         	"START_FROM" => "0"
         )
         ); 
				 global $USER;
         $id_user = $USER->GetID(); // ID user
         if (empty($id_user))
         {
         	$APPLICATION->AuthForm('', false, false, 'N', false);
         }
         else
         {
					 //user is moderator?
         $property_enums = CIBlockPropertyEnum::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>5, "PROPERTY_ID" => "23"));
         while ($element = $property_enums->GetNext()) {
         $moderators[] = (int)($element["VALUE"]);
         }
         if ( CSite::InGroup( $moderators ) ){ $isAdmin = true;}
					
				 $rsUser = CUser::GetByID($USER->GetID());
				 $currentUser = $rsUser->Fetch();
				 $UserFullName = $currentUser["LAST_NAME"]." ".$currentUser["NAME"]." ".$currentUser["SECOND_NAME"];
         //count of orders
         $arFilter = Array("USER_ID" => $id_user);
         $sql = CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $arFilter);
         while ($result = $sql->Fetch()){
         if($result["CANCELED"]=="Y"){$userordescancel++;}elseif($result["STATUS_ID"]=="F"){$userordesfinish++;}elseif($result["STATUS_ID"]=="Y"){$userordesshipped++;}else{$userordesprocess++;}
         $userordestotal++;
         }
         //faq info
         if (!$isAdmin){
         $faqcount = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 5, "NAME"=>$USER->GetFullName()), array(), false, array('ID', 'NAME')); 
         }
         else {
         $faqcount = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 5), array(), false, array('ID', 'NAME')); 
         }
         ?>

      <div class="pa_line_block">
         <a href="/personal/order/" class="pal_block">
            <div class="pal_inner_block pib1">
               <p class="pal_name"><?= getMessage('SPOL_CHECK_PIB1_NAME')?></p>
               <p class="pal_text"><?= getMessage('SPOL_CHECK_PIB1_TEXT')?></p>
               <span class="pal_namber"><?=$userordesshipped + $userordesprocess + $userordescancel ?></span>
            </div>
         </a>
         <a href="/personal/order/index.php?filter_history=Y" class="pal_block">
            <div class="pal_inner_block pib2">
               <p class="pal_name"><?= getMessage('SPOL_CHECK_PIB2_NAME')?></p>
               <p class="pal_text"><?= getMessage('SPOL_CHECK_PIB2_TEXT')?></p>
            </div>
         </a>
         <a href="/personal/faq/" class="pal_block">
            <div class="pal_inner_block pib3">
               <p class="pal_name"><?= getMessage('SPOL_CHECK_PIB3_NAME')?></p>
               <p class="pal_text"><?= getMessage('SPOL_CHECK_PIB3_TEXT')?></p>
               <span class="pal_namber"><?=$faqcount?></span>
            </div>
         </a>
         <a href="/personal/questions/" class="pal_block">
            <div class="pal_inner_block pib4">
               <p class="pal_name"><?= getMessage('SPOL_CHECK_PIB4_NAME')?></p>
               <p class="pal_text"><?= getMessage('SPOL_CHECK_PIB4_TEXT')?></p>
            </div>
         </a>
      </div>
<!--================================================================-->
					<section class="delivery_s1 pers_area">
						<div class="del_s1_left">

						<div class="pa_navigation">
               <a href="/personal/profile/"><?= getMessage('PA_NAVIGATION_PROFILE')?></a>
               <a href="/personal/passwd/"><?= getMessage('PA_NAVIGATION_PASSWORD')?></a>
               <a href="/personal/address/"><?= getMessage('PA_NAVIGATION_ADDRESS')?></a>
               <a href="<?=$APPLICATION->GetCurPageParam("logout=yes", Array("login"))?>"><?= getMessage('PA_NAVIGATION_LOGOUT')?></a>
            </div>

							<!-- <div class="bottom_sidebar">
								<div class="sale_for_day">
									<div class="sfd_top">
										<div class="sfd_top_block">
											-31%
										</div>
										<div class="sfd_top_block">
											������ ���
										</div>
									</div>
									<img src="img/sale_for_day.png" alt="">
									<p class="sfd_name">���� bluetooth ��������</p>
									<div class="sfd_popular_like">
										<div class="sfd_popular">
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
											<i class="fa fa-star" aria-hidden="true"></i>
										</div>
										<div class="sfd_like">
											<a href="/#"><i class="fa fa-heart" aria-hidden="true"></i></a>
										</div>
										<div class="clear"></div>
									</div>
									<div class="sfd_price_cart">
										<p>? 1202,29</p>
										<a href="/#">� �������</a>
										<div class="clear"></div>
									</div>
								</div>
							</div> -->

						</div><!--del_s1_left-->
						<div class="margin_right_sidebar">
							<div class="par_top">
<?

if ($currentUser["UF_ADDRESS_DELIVERY"]){
$deliveryaddreses = (\Bitrix\Main\Web\Json::decode($currentUser["UF_ADDRESS_DELIVERY"]));
$i=0;

foreach($deliveryaddreses as $deliveryaddres){
	//var_dump($deliveryaddreses[$i]);

?>
							
								<div class="wcf_adress">
								<form id="del_addr<?=$i?>" class="wcf_body1" action="javascript:void(null);" action="" method="">
									<p class="wcfa_name"><?=$deliveryaddres["LAST_NAME"]." ".$deliveryaddres["NAME"]." ".$deliveryaddres["MIDDLE_NAME"]?></p>
									<p class="wcfa_location"><?=$deliveryaddres["ADDRESS"]." ".$deliveryaddres["COUNTRY"]." ".$deliveryaddres["AREA"]." ".$deliveryaddres["CITY"]." ".$deliveryaddres["POSTCODE"]?>
									</p>
									<p class="wcfa_tel"><?=$currentUser["PERSONAL_MOBILE"]?></p>
									<?if ($deliveryaddres["main_adress"] == "yes") {?><p class="wcfa_main_adress"><?= getMessage('PERSONAL__ADDRESSES_MAINADDR')?></p><?} else {?><p class="wcfa_main_adress"></p><?}?>
									<div class="wcfa_wrap_a">
										<a class="changeaddr blue_but" href="javascript:void(null);"><?= getMessage('PERSONAL__ADDRESSES_CHANGE')?></a>
										<a class="deladdr red_but" href="javascript:void(null);"><?= getMessage('PERSONAL__ADDRESSES_DEL')?></a>
										<div class="clear"></div>
									</div>
									<input type="hidden" name="ADDRID" class="ADDRID" value="<?=$i?>">
									<input type="hidden" name="USERID" value="<?=$id_user?>">
									</form>
								</div> 
							
<?
	$i++;
	}
}
?>		
<input type="hidden" id="countadresses" value="<?=$i?>">		
							</div>
							<div class="par_bottom">
								<form id="del_addr" class="wcf_body1" action="javascript:void(null);" action="" method="">
									<div class="wcf_body_line">
										<p class="wcfbl_name"><span>*</span><?= getMessage('PERSONAL__ADDRESSES_NAME')?></p>
										<input class="wcfb1_input" name="NAME" placeholder="<?echo (getMessage('PERSONAL__ADDRESSES_PREF3')." ".getMessage('PERSONAL__ADDRESSES_NAME'))?>" type="text">
									</div>
									<div class="wcf_body_line">
										<p class="wcfbl_name"><span>*</span><?= getMessage('PERSONAL__ADDRESSES_LAST_NAME')?></p>
										<input class="wcfb1_input" name="LAST_NAME" placeholder="<?echo (getMessage('PERSONAL__ADDRESSES_PREF2')." ".getMessage('PERSONAL__ADDRESSES_LAST_NAME'))?>" type="text">
									</div>
									<div class="wcf_body_line">
										<p class="wcfbl_name"><span>*</span><?= getMessage('PERSONAL__ADDRESSES_MIDDLE_NAME')?></p>
										<input class="wcfb1_input" name="MIDDLE_NAME" placeholder="<?echo (getMessage('PERSONAL__ADDRESSES_PREF3')." ".getMessage('PERSONAL__ADDRESSES_MIDDLE_NAME'))?>" type="text">
									</div>
									<div class="wcf_body_line">
										<p class="wcfbl_name"><span>*</span><?= getMessage('PERSONAL__ADDRESSES_COUNTRY')?></p>
										<input class="wcfb1_input" name="COUNTRY" placeholder="<?echo (getMessage('PERSONAL__ADDRESSES_PREF2')." ".getMessage('PERSONAL__ADDRESSES_COUNTRY'))?>" type="text">
									</div>
									<div class="wcf_body_line">
										<p class="wcfbl_name"><span>*</span><?= getMessage('PERSONAL__ADDRESSES_AREA')?></p>
										<input class="wcfb1_input" name="AREA" placeholder="<?echo (getMessage('PERSONAL__ADDRESSES_PREF2')." ".getMessage('PERSONAL__ADDRESSES_AREA'))?>" type="text">
									</div>
									<div class="wcf_body_line">
										<p class="wcfbl_name"><span>*</span><?= getMessage('PERSONAL__ADDRESSES_CITY')?></p>
										<input class="wcfb1_input" name="CITY" placeholder="<?echo (getMessage('PERSONAL__ADDRESSES_PREF1')." ".getMessage('PERSONAL__ADDRESSES_CITY'))?>" type="text">
									</div>
									<div class="wcf_body_line">
										<p class="wcfbl_name"><span>*</span><?= getMessage('PERSONAL__ADDRESSES_POSTCODE')?></p>
										<input class="wcfb1_input" name="POSTCODE" placeholder="<?echo (getMessage('PERSONAL__ADDRESSES_PREF1')." ".getMessage('PERSONAL__ADDRESSES_POSTCODE'))?>" type="text">
									</div>
									<div class="wcf_body_line">
										<p class="wcfbl_name"><span>*</span><?= getMessage('PERSONAL__ADDRESSES_ADDRESS')?></p>
										<input class="wcfb1_input" name="ADDRESS" placeholder="<?echo (getMessage('PERSONAL__ADDRESSES_PREF1')." ".getMessage('PERSONAL__ADDRESSES_ADDRESS'))?>" type="text">
									</div>
									<div class="par_checkbox_block">
										<p class="wcfbl_name"><?= getMessage('PERSONAL__ADDRESSES_ISMAIN')?></p>
										<div class="par_checkbox">
											<input id="main_adress" class="wcfb1_input" name="main_adress" type="radio" value="yes">
											<span class="par_checkbox_yes"><?= getMessage('PERSONAL__ADDRESSES_YES')?></span>
											<input id="not_main_adress" class="wcfb1_input" name="main_adress" type="radio" value="no">
											<span><?= getMessage('PERSONAL__ADDRESSES_NO')?></span>
										</div>
									</div>
									<input type="hidden" name="USERID" value="<?=$id_user?>">
									<input type="hidden" id="CHADDRID" name="CHADDRID" class="CHADDRID" >
									<div class="par_submit">
										<button class="button2" type="submit"><?= getMessage('PERSONAL__ADDRESSES_BTNADD')?></button>
										<div class="pars_check">
											<input class="wcfb1_input" type="checkbox" name="">
											<span><?= getMessage('PERSONAL__ADDRESSES_GARANT')?></span>
										</div>
									</div>
								</form>
							</div>
						</div><!--margin_right_sidebar-->

					</section>
<!--================================================================-->
			
				</div>
				<?}?>
			</section>

 <script type="text/javascript" language="javascript">

     	function call(elem) {
     	  var msg   = (elem).serialize();
            $.ajax({
              type: 'POST',
              url: 'ipost.php',
              data: msg,
              success: function(data) {
								 location.reload();
								// console.log(data)
              },
              error:  function(xhr, str){
    	    alert('�������� ������: ' + xhr.responseCode);
              
           }
				});
      }

		function change(elem) {
     	  var msg   = (elem).serialize();
            $.ajax({
              type: 'POST',
              url: 'ichange.php',
              data: msg,
              success: function(data) {
								//location.reload();
								// console.log(data);
								var json = $.parseJSON(data);
								$(json).each(function(i,val){
										$.each(val,function(k,v){
												$("input[name*="+ k +"]").val(v); 
									});
								});
								$('#main_adress').val("yes");
              },
              error:  function(xhr, str){
    	    alert('�������� ������: ' + xhr.responseCode);
           }
				});
      }
			
			$(".changeaddr").click(function() {
				$('.CHADDRID').val($(this).parent().parent().find(".ADDRID").val());
				console.log($(this).parent().parent().find(".ADDRID").val());
				change($(this).parent().parent());
			});

			function remove(elem) {
     	  var msg   = (elem).serialize();
            $.ajax({
              type: 'POST',
              url: 'idel.php',
              data: msg,
              success: function(data) {
								location.reload();
								// console.log(data);
              },
              error:  function(xhr, str){
    	    alert('�������� ������: ' + xhr.responseCode);
           }
				});
      }
			
			$(".deladdr").click(function() {
				if ($('#countadresses').val() != "1"){
				remove($(this).parent().parent());
				}
				else {alert("�� �� ������ ������� ������������ �����");}
			});

		$('.wcfb1_input').click(function(){
			$(this).removeClass('empty_field');
		})

		$(".button2").click(function() {
			var checkedradio = ($("input:radio:checked").length);
			var checkedcheckbox = ($("input:checkbox:checked").length);
			var chk1 = 0;
			$('#del_addr').find('.wcfb1_input').each(function(){
				if($(this).val() == ''){
			$(this).addClass('empty_field'); 
					} else {
			$(this).removeClass('empty_field');
			chk1 ++;
					}
					
			if(($(this).is(':radio'))&(checkedradio != 0)){
			$(this).parent().removeClass('empty_field'); 
					} 
			if(($(this).is(':checkbox'))&(checkedcheckbox != 0)){
			$(this).parent().removeClass('empty_field'); 
					} 
			});
		if ((chk1 == 11) & (checkedradio != 0) & (checkedcheckbox != 0)){
			call($('#del_addr'));
			}
	});
    </script>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>