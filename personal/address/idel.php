<?

if($_SERVER['REQUEST_METHOD'] == 'POST') {
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
IncludeTemplateLangFile(__FILE__);
CModule::IncludeModule("iblock");
global $USER;
define("AUTH", true);
$rsUser = CUser::GetByID($_POST["USERID"]);
$arUser = $rsUser->Fetch();

//  var_dump($_POST);
if (!empty($arUser["UF_ADDRESS_DELIVERY"])) $old_delivery_address = \Bitrix\Main\Web\Json::decode($arUser["UF_ADDRESS_DELIVERY"]);

// var_dump($old_delivery_address);
unset($old_delivery_address[$_POST["ADDRID"]]);
sort($old_delivery_address);
// var_dump($old_delivery_address);
// $old_delivery_address[] = $delivery_address;

$new_delivery_address = \Bitrix\Main\Web\Json::encode($old_delivery_address);

$user = new CUser;
$fields = Array(
  "UF_ADDRESS_DELIVERY"          => $new_delivery_address,
  );
$user->Update($_POST["USERID"], $fields);
$strError .= $user->LAST_ERROR;
echo ($strError);
}
?>