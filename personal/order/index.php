<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("������");
?>
    <style>
        .sidebar_menu .sidebar_menu_body{
            display: none;
        }
    </style>
    <section class="personal_area">
        <div class="workspace">
            <?$APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                "main",
                Array(
                    "PATH" => "",
                    "SITE_ID" => "s1",
                    "START_FROM" => "0"
                )
            );?> <br>
<?$APPLICATION->IncludeComponent(
	"bitrix:sale.personal.order", 
	"main-personal-order2", 
	array(
		"SEF_MODE" => "N",
		"ORDERS_PER_PAGE" => "5",
		"PATH_TO_PAYMENT" => "/personal/order/payment/",
		"PATH_TO_BASKET" => "/personal/cart/",
		"SET_TITLE" => "Y",
		"COMPONENT_TEMPLATE" => "main-personal-order2",
		"DETAIL_HIDE_USER_INFO" => array(
			0 => "PERSON_TYPE_NAME",
		),
		"PROP_1" => array(
			0 => "1",
			1 => "2",
			2 => "3",
			3 => "4",
			4 => "5",
			5 => "6",
			6 => "8",
		),
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "3600",
		"CACHE_GROUPS" => "Y",
		"PATH_TO_CATALOG" => "/catalog/",
		"SAVE_IN_SESSION" => "Y",
		"NAV_TEMPLATE" => "",
		"CUSTOM_SELECT_PROPS" => array(
		),
		"HISTORIC_STATUSES" => array(
			0 => "F",
		),
		"RESTRICT_CHANGE_PAYSYSTEM" => array(
			0 => "0",
		),
		"ORDER_DEFAULT_SORT" => "STATUS",
		"ALLOW_INNER" => "Y",
		"ONLY_INNER_FULL" => "N",
		"STATUS_COLOR_F" => "gray",
		"STATUS_COLOR_N" => "green",
		"STATUS_COLOR_PSEUDO_CANCELLED" => "red",
		"STATUS_COLOR_Y" => "gray",
		"REFRESH_PRICES" => "N",
		"STATUS_COLOR_C" => "gray",
		"STATUS_COLOR_S" => "gray"
	),
	false
);?>

        </div>
    </section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>