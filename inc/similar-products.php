<?$products = getSimilarProducts($arResult["PROPERTIES"]["SIMILAR_PRODUCTS"]["VALUE"]);?>
<?
if (!is_array($products) || !count($products))
    return;
?>
<div class="cdb1_similar_products">
    <p class="cdb1_sp_name">������� ��������</p>

    <div id="cdb1_similar_carousel" class="cdb1_similar_carousel owl-carousel" >
        <!---->
        <?foreach ($products as $id => $product){?>
            <div class="cb_block tabs_catalog" data-id="<?= $product["ID"]?>">
                <div class="cbb_inner cbb_left">
                    <img src="<?= CFile::GetPath($product["PREVIEW_PICTURE"])?>" alt="<?= $product["NAME"]?>">
                    <p class="cbb_name"><?= $product["NAME"]?></p>
                </div>
                <div class="cbb_inner cbb_right">
                    <div class="cbb_wrap">
                        <p class="cbb_name"><?= $product["NAME"]?></p>
                        <div class="cbb_popular">
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                        </div>
                        <p class="ccb_artikul">�������: <?= $product["PROPERTY_ARTICLE_VALUE"]?></p>
                        <p class="cbb_delivery">�������� �������� � �. ���������</p>
                    </div>
                    <div class="cbb_bottom_block">
                        <p class="cbb_pricce"><span><?= CustomCurrencyFormat($product["CATALOG_CURRENCY_".BASE_PRICE_ID])?></span><?= $product["CATALOG_PRICE_".BASE_PRICE_ID]?></p>
                        <div class="cbb_icons">
                            <a href="/#"><i class="icon1 fa fa-heart" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        <?}?>
    </div>

</div>