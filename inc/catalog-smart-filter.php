<? $_REQUEST["SECTION_CODE"] = getRequestCode(); ?>
<?$APPLICATION->IncludeComponent(
    "bitrix:catalog.smart.filter",
    ".default",
    Array(
        "CACHE_GROUPS" => "Y",
        "CACHE_TIME" => "36000000",
        "CACHE_TYPE" => "A",
        "CONVERT_CURRENCY" => "Y",
        "CURRENCY_ID" => "RUB",
        "DISPLAY_ELEMENT_COUNT" => "Y",
        "FILTER_NAME" => "arrFilter",
        "FILTER_VIEW_MODE" => "vertical",
        "HIDE_NOT_AVAILABLE" => "N",
        "IBLOCK_ID" => "2",
        "IBLOCK_TYPE" => "sections_elements",
        "PAGER_PARAMS_NAME" => "arrPager",
        "POPUP_POSITION" => "right",
        "PRICE_CODE" => array("RUB", "USD"),
        "SAVE_IN_SESSION" => "N",
        "SECTION_CODE" => $_REQUEST["SECTION_CODE"],
        "SECTION_DESCRIPTION" => "-",
        "SECTION_ID" => $_REQUEST["SECTION_ID"],
        "SECTION_TITLE" => "-",
        "SEF_MODE" => "N",
        "TEMPLATE_THEME" => "blue",
        "XML_EXPORT" => "N"
    )
);?>
