<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("��������");
?>
    <section class="delivery_s1 payment">
        <div class="workspace">
            <div class="del_s1_left"></div>
            <div class="margin_right_sidebar">
					<div class="dr_wrap_content">

                 
								<p>

	&nbsp;</p>
<h2>
	<span style="color:#800000;"><span style="font-family: times new roman,times,serif;">��������: ��������� ������� �� ����� lianhouse.ru ������� ��� ����� �������� �� �����</span></span></h2>
<p>
	&nbsp;</p>
<p>
	<span style="font-family:times new roman,times,serif;"><strong><span style="font-size: 20px;">������� �������� :</span></strong></span></p>
<p>
	&nbsp;</p>
<p>
	<span style="font-family:times new roman,times,serif;"><span style="font-size: 16px;">������ ��� ������ ������������ �� �����, ��� ����������� �� ������������ ����������� ������� ����� � ����������, ����� ����� ������������� � ������������, ������ ���������� �������� ��� ������, ����� ����� ����� � �������� � ������ � ������ ��� ( � ������ ������ �������� �������� ����� � �������) </span></span></p>
<p>
	&nbsp;</p>
<p>
	<span style="font-family:times new roman,times,serif;"><span style="font-size: 16px;">�� ��������� ���� ���� ��� ��������� �������� � ��������� ��������, �������� ��������� ��������, ��� ���� ��� � ���� ��������. ������� ������ ( �� 10 �����) ����� ���������� �������� ������������, �������������� ��� ���� �������� ����� ��� ��������� �� ����� � ���������� �����������</span></span></p>
<p>
	<span style="font-family:times new roman,times,serif;"><span style="font-size: 16px;">��������� �� ������ ���� �������� � ����������.</span></span></p>
<p>
	&nbsp;</p>
<p>
	<span style="color:#800000;"><span style="font-size: 18px;"><span style="font-family: times new roman,times,serif;">�������� : ���������� ������������ ������������/�������� � ��� ���, �.� �� ����� � ������ �� �������! �� ���� ���� ����������� ��� ������ ����� ����� �������� �� ��������������</span></span></span></p>
<p>
	&nbsp;</p>
<p>
	<span style="font-family:times new roman,times,serif;"><span style="font-size: 20px;"><strong>��������� �������� ������� �� ����� ������� �� :</strong></span></span></p>
<p>
	<span style="font-family:times new roman,times,serif;">&nbsp;-���� ������</span></p>
<p>
	<span style="font-family:times new roman,times,serif;">&nbsp;-����</span></p>
<p>
	<span style="font-family:times new roman,times,serif;">&nbsp;-������</span></p>
<p>
	<span style="font-family:times new roman,times,serif;">&nbsp;-��������� ��������</span></p>
<p>
	<span style="font-family:times new roman,times,serif;">&nbsp;-�������� ��������</span></p>
<p>
	&nbsp;</p>
<p>
	<span style="font-family:times new roman,times,serif;"><strong><span style="font-size: 20px;">��� �������������� �������� :</span></strong></span></p>
<p>
	<span style="font-family:times new roman,times,serif;"><span style="font-size: 14px;">&nbsp; 1. �������� �� ���������� . ����� �� ���������� ������������ �� �����. ��������� �� ������� ���������� �� ������ ������, ��� ������� �� ����, ��� ��������� ���������, ���� � ������ ������. ��� ������ �����������, ��� ������ �������� ��������� �������� �� ������</span></span></p>
<p>
	<span style="font-family:times new roman,times,serif;"><span style="font-size: 14px;">&nbsp; 2.&nbsp; ��������� ��������.&nbsp; ����� ������������� � ��������� ���-�� ������ ( ���� ) � ����������� �� ���� � ������</span></span></p>
<p>
	<span style="font-family:times new roman,times,serif;"><span style="font-size: 14px;">&nbsp; 3.&nbsp; ��������� ������, 2 % �� ��������� ������</span></span></p>
<p>
	<span style="font-family:times new roman,times,serif;"><span style="font-size: 14px;">&nbsp; 4.&nbsp; ��������� �������� �� ������ � ����������.&nbsp;&nbsp; � ������, �������������, ������������ � ������������ �� ������ ���������� �������� ������ ��� ���������</span></span></p>
<p>
	<span style="font-family:times new roman,times,serif;"><span style="font-size: 14px;">&nbsp; 5.&nbsp; �������� �� ������ ������ ����������� ������������� ����������. ( ��� �������� �� ������ ���������� ��� ���������)</span></span></p>
<p>
	<span style="font-family:times new roman,times,serif;">��� ������� ������������ � ������� �� ���-�� ���������, ������ ���������� ��������� �� 1 ��</span></p>
<p>
	&nbsp;</p>
<p>
	<span style="font-family:times new roman,times,serif;">*<em>�� ������ ������ ����������� ������� ���������� �� ������ � ���� �� 12 ����, � �����������, ������������ � ���� �� 18 ����, ����������� � ���� �� 12 ����</em></span></p>
<p>
	&nbsp;</p>
<p>
	<span style="font-family:times new roman,times,serif;">����� ���������� ������ �������������� ��������������� ��������� ��������. �� �� ������ ���������� ����� ������� � ������ ��������</span></p>
<p>
	<span style="font-family:times new roman,times,serif;">����� �������� ����� ���������� ����� ������, �.� �������� ������ �� ���������� �������, � ������ �� ������ ������� ��������� ������ ���������,</span></p>
<p>
	<span style="font-family:times new roman,times,serif;">�� �� �������������� ������ ��������� ������� ����������� ����������� � ��������� ����</span></p>
<p>
	&nbsp;</p>
<p>
	<u><strong><span style="font-family:times new roman,times,serif;">����� �������� �� ������ � ����� �� ��������� ��������� ��� ����� ������� �������� ����� �������� �� ����� ���������. ���� �� ���������� � ����� �� 4 ������� ��������, �� ������ �� ���� ��������� ������� ���� � ��������� </span></strong></u></p>
<p>
	&nbsp;</p>
<p>
	<span style="font-family:times new roman,times,serif;">������� ����� ������ ���� ������ �� ������, ������ ��� ������������ � ������� ����������� �� : ���, ������-������, ������</span></p>
<p>
	&nbsp;</p>
<p>
	<span style="font-family:times new roman,times,serif;"><strong>������� ��������� �������� ������� ���������� 5-7 $ �� �� ��� �� ������ ������</strong></span></p>
<p>
	&nbsp;</p>
<p>
	&nbsp;</p>

						
						
						<div class="dr_blocks">
							<div class="dr_block">
								<p class="dr_block_name">
									�������� ���������
									<span>(10-15 ��)</span>
								</p>
								<div class="dr_block_img">
									<img src="img/delivery1.png" alt="">
								</div>
							</div>
							<div class="dr_block">
								<p>
									�������� � ������� �������� - ��� ����� ������� ������ �������� ������ �� ����� � ������. ������� ���� �������� ���������� 10-15 ���� �� ������.<br> ����� ������ ������ � ���������� ��� ���� ��������������� ������������ �� ������� �����, ����� ����������� ���������� ���������, ���� ���������� ���� �����������, � �� ��������� ���� ���� � ��������� ����������.
								</p>
							</div>
						</div>

						<div class="dr_blocks">
							<div class="dr_block">
								<p class="dr_block_name">
									�������� �������
									<span>(20-25 ��)</span>
								</p>
								<div class="dr_block_img">
									<img src="img/delivery2.png" alt="">
								</div>
							</div>
							<div class="dr_block">
								<p>
									�������� � ������� ���������������� ���������� - ��� ����� ���������������� � �������� ������ �������� ����� �� �����. ������� ���� �������� ���������� 20-25�� �� ������.<br> ����� ������ ������ � ����������, ���� ������������ �� ������� �����, ����� ����������� ���������� ��������, � ����� �������� �������, ���� ���������� ������������ �������� � ������. �� ��������� ���� ���� � ������ ������ ������������ ��������.
								</p>
							</div>
						</div>

						

						<p class="dr_small_text">
							<span>����������!</span> ���������� ����� ������ � �������� ������ �����, ����������� � �������� 3-5 ����!<br> ��� �����, ����� �� �������� ���� ������ ��� ����� � � �����������.
						</p>


					</div>

				</div>
			</div>
		</section>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>