<?php
require('config.php');

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;use HL\HighloadBlockTable;
CModule::IncludeModule("highloadblock"); // подключить инфоблоки
CModule::IncludeModule('iblock');

function pars($url, $data=[])
{

  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HEADER, 0);
  // Прокси через Fiddler
  //curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');

  $result = curl_exec($curl);

  $today = date("d m Y H:i:s")."\n";
  //file_put_contents($_SERVER["DOCUMENT_ROOT"].'/otapi_log.txt', $today, FILE_APPEND);
  $log_data = $today.(string)$url."\n";
  file_put_contents($_SERVER["DOCUMENT_ROOT"].'/otapi_log.txt', $log_data, FILE_APPEND);

  if ($data):
    //file_put_contents($_SERVER["DOCUMENT_ROOT"].'/otapi_log.txt', print_r($data, true), FILE_APPEND);
  endif;

  if ($result === false) {
      echo "cURL Error: " . curl_error($curl); die();
  }
  $xmlObject = simplexml_load_string($result);
  curl_close($curl);

  if ((string)$xmlObject->ErrorCode !== 'Ok') {
      echo "Error: " . $xmlObject->ErrorDescription; /*die();*/
  }

  return $xmlObject;
}

//запись файла
function writeToFile($fileName, $arr)
{

	$fp = fopen($fileName, 'w');

	fputcsv($fp, $arr);

	fclose($fp);
}

//получить id (api) категорий по уровням вложенности
function getCategoriesId($depthLevel)
{

	$arFilter = Array('IBLOCK_ID' => 8, 'DEPTH_LEVEL' => $depthLevel);
	$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true, Array('IBLOCK_ID ', 'UF_ID_CATEGORY'));

	$ids = [];
	$count = 0;
	$i = 1;

	while($ar_result = $db_list->GetNext())
	{
		$count++;

		$ids[] = $ar_result['UF_ID_CATEGORY'];

		/*if ($count == 10) {
			writeToFile('categoryId/CategoryIdSub'. $i++ .'.csv', $ids);

			$count = 0;
			$ids = [];
			continue;
		}*/

	}

	return $ids;
}

//получить id (api) всех категорий
function getAllCategoriesId()
{

  $arFilter = Array('IBLOCK_ID' => 8);
  $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true, Array('IBLOCK_ID ', 'UF_ID_CATEGORY'));

  $ids = [];
  $count = 0;
  $i = 1;

  while($ar_result = $db_list->GetNext()) {
    $count++;

    $ids[] = $ar_result['UF_ID_CATEGORY'];

   }

  return $ids;
}

//получить id одной категории
function getCategory($id) {

  $arFilter = Array('IBLOCK_ID' => 8, 'UF_ID_CATEGORY' => $id);
  $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true, Array('IBLOCK_ID ', 'UF_ID_CATEGORY'));

  while($ar_result = $db_list->GetNext()) {

    $id = $ar_result['ID'];

   }

   return $id;
}

//получить id (api) всех товаров из переданной категории
function getProduct($cat, $count, &$productId = [])
{

    $count = $count + 50;

    if ($count < 3000) {

	    $productItems = 'http://otapi.net/OtapiWebService2.asmx/FindCategoryItemSimpleInfoListFrame?instanceKey=' . CFG_SERVICE_INSTANCEKEY
	    . '&language=' . CFG_REQUEST_LANGUAGE
	   . '&categoryId=' . $cat
	   . '&categoryItemFilter='
	   . '&framePosition=' . $count
	   . '&frameSize=' . '50'
	   . '&sessionId=&blockList=';

	    $productItemsInfo = pars($productItems)->OtapiItemInfoSubList->Content->Item;
    } else {
    	unset($productItemsInfo);
    }

    if (count($productItemsInfo) > 0){

        foreach ($productItemsInfo as $product) {

            $productId[] = (string)$product->Id;

        }

        getProduct($cat, $count, $productId);

        return $productId;

    } else {
        return $productId;
    }

}

//получить полную информацию о товаре
function getProductFull($id)
{
  $productItems = 'http://otapi.net/OtapiWebService2.asmx/BatchGetItemFullInfo?instanceKey=' . CFG_SERVICE_INSTANCEKEY
  . '&language=' . CFG_REQUEST_LANGUAGE
  . '&itemId=' . $id
  . '&sessionId=&blockList=';

  $productInfo = pars($productItems)->Result->Item;

  return $productInfo;
}

//получить все методы работы с блоками HighloadBlock
function getEntityDataClass($hlbl)
{

    $hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    $entityDataClass = $entity->getDataClass();

    return $entityDataClass;
}

//Функция обновления цен. Делаем запрос GetItemFullInfo, обновляем цены и табличку конфигурации (см. детальную страницу товара)
function refreshPrices ($product, $ext_cat) {

  $res = CIBlockElement::GetProperty (8, $product, array('SORT' => 'ASC'), array('CODE' => 'ARTICLE'));
  if ($arArticle = $res->Fetch()) {
    $article = $arArticle["VALUE"];
  }
  $res = null;

//Здесь должна быть проверка, изменилась ли цена. Проблема в том, что GetItemPrice возвращает вообще другую цену, независимо от количества и конфигурации.

/*
  $resPrice = CPrice::GetList(
  array(),
  array(
    "PRODUCT_ID" => $product,
    "CATALOG_GROUP_ID" => 3
  )
  );

  while ($arr_ = $resPrice->GetNext())
  {
  $arPrices[] = $arr_["PRICE"];
    echo "<pre>";
    print_r($arr_);
    echo "</pre>";
  }
  echo "<pre>";
    print_r($arPrices);
    echo "</pre>";

  $query = 'http://otapi.net/OtapiWebService2.asmx/GetItemPrice?'
      . 'instanceKey='. CFG_SERVICE_INSTANCEKEY
      . '&language=' . CFG_REQUEST_LANGUAGE
      . '&categoryId=' . $ext_cat
      . '&quantity=' . 3
      . '&itemId=' . $article
      . '&promotionId=&configurationId=';

      //print_r($query);

    $a = pars($query);
    $new_price = (float)$a->Result->ConvertedPriceWithoutSign;

    $different_price = false;

    foreach ($arPrices as $pr) {
      if ($pr !== (float)$new_price) {
        $different_price = true;
      } else {
        continue;
      }
    }
*/
//    echo "<pre>New: ".$new_price."; Old: ".print_r($arPrices)."; Diff: ".$different_price."</pre>";

$different_price = true; //Это вместо нормальной проверки на изменение цены

if ($different_price) {
  //echo "refreshing starts";
  $query = 'http://otapi.net/OtapiWebService2.asmx/GetItemFullInfo?instanceKey=' . CFG_SERVICE_INSTANCEKEY
  . '&language=' . CFG_REQUEST_LANGUAGE
  . '&categoryId=' . $ext_cat
  . '&itemId=' . $article;

  $extproductItemsInfo = pars($query, $_SERVER)->OtapiItemFullInfo;

  $d = array();

  if($extproductItemsInfo->QuantityRanges):
      foreach($extproductItemsInfo->QuantityRanges->Range as $range){
        $a = Array(
          "quant"=>(int)$range->MinQuantity,
          "price"=>(float)$range->Price->ConvertedPriceWithoutSign,
        );
        array_push($d, $a);
      }
    else:
      $a = (string)$extproductItemsInfo->Price->ConvertedPriceWithoutSign;
      //array_push($d, $a);
      $d['master'] = $a;
    endif;
//echo "<pre>";
  //print_r($d);
//echo "</pre>";

$PRICE_TYPE_ID = 3;
if(count($d)>0){
  foreach($d as $k=>$range_item){
    //echo $k;
    if($k+1==count($d)){
      $arFields[] = Array(
        "PRODUCT_ID" => $product,
        "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
        "PRICE" => $d[$k]["price"],//$extproductItemsInfo->Price->ConvertedPriceWithoutSign,
        "CURRENCY" => "RUB",
        "QUANTITY_FROM" => $d[$k]["quant"],
        "QUANTITY_TO" => "INF"
      );
      //echo "<pre>"; print_r($arFields); echo "</pre>";
    } else {
      $arFields[] = Array(
        "PRODUCT_ID" => $product,
        "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
        "PRICE" => $d[$k]["price"],//$extproductItemsInfo->Price->ConvertedPriceWithoutSign,
        "CURRENCY" => "RUB",
        "QUANTITY_FROM" => $d[$k]["quant"],
        "QUANTITY_TO" => $d[$k+1]["quant"]-1,
      );
      //echo "<pre>"; print_r($arFields); echo "</pre>";
    }
  }
} else {
  $arFields[] = Array(
    //"ID" => 3,
    "PRODUCT_ID" => $product,
    "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
    "PRICE" => $d['master'],
    "CURRENCY" => "RUB",
    //"QUANTITY_FROM" => $arRanges["RANGE3"]["quantity"],
    //"QUANTITY_TO" => "INF"
  );
  //echo "<pre>"; print_r($arFields); echo "</pre>";
}
//echo "res";
//echo "<pre>"; print_r($arFields); echo "</pre>";
$res = CPrice::GetList(
  array(),
  array(
    "PRODUCT_ID" => $product,
    "CATALOG_GROUP_ID" => $PRICE_TYPE_ID
  )
);

while ($arr = $res->GetNext()) {
  CPrice::Delete($arr["ID"]);
  
}
foreach($arFields as $k=>$arField){
    CPrice::Add($arField);
  }

//Обновляем табличку цен, скопипащено из local/ajax/iaddextprod.php
//Все это работает адски долго
$confItems = array();
foreach($extproductItemsInfo->ConfiguredItems->OtapiConfiguredItem as $k=>$confItem){
  $d[$k] = Array(
      "configurators" => Array(),//$confItem->Configurators,
      "prices"=> Array(),
      "id"=>(string)$confItem->Id
    );
    foreach($confItem->Configurators->ValuedConfigurator as $co){
      $c = Array(
        "Pid" => $co->attributes()["Pid"],
        "Vid" => htmlentities(mb_convert_encoding($co->attributes()["Vid"], 'UTF-8', 'auto')),
        //"Col" => $extproductItemsInfo->Attributes->ItemAttribute[$k]
      );
      foreach($extproductItemsInfo->Attributes->ItemAttribute as $atr){
        if((string)$atr->attributes()["Pid"]==(string)$co->attributes()["Pid"]){
          if($atr->ImageUrl){
            $c["pic"] = 'true';
          } else {
            $c["pic"] = 'false';
          }
        }
      }
      array_push($d[$k]["configurators"], $c);
    }
    if($confItem->QuantityRanges):
      foreach($confItem->QuantityRanges->Range as $range){
        $a = Array(
          "quant"=>(int)$range->MinQuantity,
          "price"=>(float)$range->Price->ConvertedPriceWithoutSign,
        );
        array_push($d[$k]["prices"], $a);
      }
    else:
      $a = (string)$confItem->Price->ConvertedPriceWithoutSign;
      array_push($d[$k]["prices"], $a);
    endif;
    array_push($confItems, $d);
};

$string = \Bitrix\Main\Web\Json::encode($confItems);

CIBlockElement::SetPropertyValuesEx($product, 8, Array("SERIAL"=>$string));
}
}
