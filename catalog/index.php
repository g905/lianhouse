<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("������� �������");
?>
	<section class="catalog">
	<div class="workspace">
		<div class="cs_s1_left">
		<div class="bottom_sidebar">



								<!--catalog control filter-->
								<div class="catalog_control">

									<div class="cc_block1">
										<p class="cc_b1_name">���� ������ <i class="fa fa-angle-down" aria-hidden="true"></i></p>

										<div class="cc_b1_body">	
											<input type="text" class="cs_minCost" id="cs1_minCost" value="0"/>
											<p class="cc_input_line"><span></span></p>
											<input type="text" class="cs_maxCost" id="cs1_maxCost" value="10000"/>
											<div class="wrap_catalog_slider">
												<div class="catalog_slider" id="catalog_slider1"></div>
											</div>
											<span class="cc_exit">������</span>
											<div class="clear"></div>
										</div>
									</div>
									
									<div class="cc_block1">
										<p class="cc_b1_name">����������� ����� <i class="fa fa-angle-down" aria-hidden="true"></i></p>

										<div class="cc_b1_body">	
											<input type="text" class="cs_minCost" id="cs2_minCost" value="1"/>
											<p class="cc_input_line"><span></span></p>
											<input type="text" class="cs_maxCost" id="cs2_maxCost" value="1000"/>
											<div class="wrap_catalog_slider">
												<div class="catalog_slider" id="catalog_slider2"></div>
											</div>
											<span class="cc_exit">������</span>
											<div class="clear"></div>
										</div>
									</div>

									<!--<div class="cc_block1">
										<p class="cc_b1_name">�������� �����<i class="fa fa-angle-down" aria-hidden="true"></i></p>

										<div class="cc_b1_body">	
											<input type="text" class="cs_minCost" id="cs3_minCost" value="1"/>
											<p class="cc_input_line"><span></span></p>
											<input type="text" class="cs_maxCost" id="cs3_maxCost" value="8"/>
											<div class="wrap_catalog_slider">
												<div class="catalog_slider" id="catalog_slider3"></div>
											</div>
											<span class="cc_exit">������</span>
											<div class="clear"></div>
										</div>
									</div> -->

									<div class="cc_block_bottom">
										<button id="searchfilter" class="button1">��������</button>
										 <!--<p class="cc_destroy">�������� ������</p> -->
									</div>

								</div>
								<!--/catalog control filter-->



							</div>
		</div>
		<div class="margin_right_sidebar">
			 

			<?$APPLICATION->IncludeComponent(
			"bitrix:catalog", 
			"simple", 
			array(
				"ACTION_VARIABLE" => "action",
				"ADD_ELEMENT_CHAIN" => "Y",
				"ADD_PICT_PROP" => "-",
				"ADD_PROPERTIES_TO_BASKET" => "Y",
				"ADD_SECTIONS_CHAIN" => "Y",
				"AJAX_MODE" => "N",
				"AJAX_OPTION_ADDITIONAL" => "",
				"AJAX_OPTION_HISTORY" => "Y",
				"AJAX_OPTION_JUMP" => "N",
				"AJAX_OPTION_STYLE" => "Y",
				"BASKET_URL" => "/personal/cart/",
				"BIG_DATA_RCM_TYPE" => "personal",
				"CACHE_FILTER" => "N",
				"CACHE_GROUPS" => "Y",
				"CACHE_TIME" => "36000000",
				"CACHE_TYPE" => "A",
				"COMMON_ADD_TO_BASKET_ACTION" => "ADD",
				"COMMON_SHOW_CLOSE_POPUP" => "N",
				"COMPATIBLE_MODE" => "Y",
				"COMPONENT_TEMPLATE" => "simple",
				"CONVERT_CURRENCY" => "N",
				"DETAIL_ADD_DETAIL_TO_SLIDER" => "N",
				"DETAIL_ADD_TO_BASKET_ACTION" => "",
				"DETAIL_ADD_TO_BASKET_ACTION_PRIMARY" => "",
				"DETAIL_BACKGROUND_IMAGE" => "-",
				"DETAIL_BRAND_USE" => "N",
				"DETAIL_BROWSER_TITLE" => "-",
				"DETAIL_CHECK_SECTION_ID_VARIABLE" => "N",
				"DETAIL_DETAIL_PICTURE_MODE" => "IMG",
				"DETAIL_DISPLAY_NAME" => "Y",
				"DETAIL_DISPLAY_PREVIEW_TEXT_MODE" => "H",
				"DETAIL_IMAGE_RESOLUTION" => "16by9",
				"DETAIL_MAIN_BLOCK_OFFERS_PROPERTY_CODE" => "",
				"DETAIL_MAIN_BLOCK_PROPERTY_CODE" => "",
				"DETAIL_META_DESCRIPTION" => "-",
				"DETAIL_META_KEYWORDS" => "-",
				"DETAIL_OFFERS_FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"DETAIL_OFFERS_PROPERTY_CODE" => array(
					0 => "",
					1 => "",
				),
				"DETAIL_PRODUCT_INFO_BLOCK_ORDER" => "sku,props",
				"DETAIL_PRODUCT_PAY_BLOCK_ORDER" => "rating,price,priceRanges,quantityLimit,quantity,buttons",
				"DETAIL_PROPERTY_CODE" => array(
					0 => "ARTICLE",
					1 => "MIN_QUANTITY",
					2 => "CHARACTERISTICS",
					3 => "",
				),
				"DETAIL_SET_CANONICAL_URL" => "N",
				"DETAIL_SET_VIEWED_IN_COMPONENT" => "N",
				"DETAIL_SHOW_BASIS_PRICE" => "Y",
				"DETAIL_SHOW_MAX_QUANTITY" => "N",
				"DETAIL_SHOW_POPULAR" => "Y",
				"DETAIL_SHOW_SLIDER" => "N",
				"DETAIL_SHOW_VIEWED" => "Y",
				"DETAIL_STRICT_SECTION_CHECK" => "Y",
				"DETAIL_USE_COMMENTS" => "N",
				"DETAIL_USE_VOTE_RATING" => "N",
				"DISABLE_INIT_JS_IN_COMPONENT" => "N",
				"DISPLAY_BOTTOM_PAGER" => "Y",
				"DISPLAY_TOP_PAGER" => "N",
				"ELEMENT_SORT_FIELD" => "shows",
				"ELEMENT_SORT_FIELD2" => "name",
				"ELEMENT_SORT_ORDER" => "desc",
				"ELEMENT_SORT_ORDER2" => "",
				"FILE_404" => "",
				"FILTER_FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"FILTER_HIDE_ON_MOBILE" => "N",
				"FILTER_NAME" => "searchFilter",
				"FILTER_OFFERS_FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"FILTER_OFFERS_PROPERTY_CODE" => array(
					0 => "37",
					1 => "38",
					2 => "VARIANT",
					3 => "",
				),
				"FILTER_PRICE_CODE" => array(
					0 => "BASE_PRICE",
				),
				"FILTER_PROPERTY_CODE" => array(
					0 => "VENDOR_NAME",
					1 => "MIN_QUANTITY",
					2 => "",
				),
				"FILTER_VIEW_MODE" => "VERTICAL",
				"GIFTS_DETAIL_BLOCK_TITLE" => "�������� ���� �� ��������",
				"GIFTS_DETAIL_HIDE_BLOCK_TITLE" => "N",
				"GIFTS_DETAIL_PAGE_ELEMENT_COUNT" => "4",
				"GIFTS_DETAIL_TEXT_LABEL_GIFT" => "�������",
				"GIFTS_MAIN_PRODUCT_DETAIL_BLOCK_TITLE" => "�������� ���� �� �������, ����� �������� �������",
				"GIFTS_MAIN_PRODUCT_DETAIL_HIDE_BLOCK_TITLE" => "N",
				"GIFTS_MAIN_PRODUCT_DETAIL_PAGE_ELEMENT_COUNT" => "4",
				"GIFTS_MESS_BTN_BUY" => "�������",
				"GIFTS_SECTION_LIST_BLOCK_TITLE" => "������� � ������� ����� �������",
				"GIFTS_SECTION_LIST_HIDE_BLOCK_TITLE" => "N",
				"GIFTS_SECTION_LIST_PAGE_ELEMENT_COUNT" => "4",
				"GIFTS_SECTION_LIST_TEXT_LABEL_GIFT" => "�������",
				"GIFTS_SHOW_DISCOUNT_PERCENT" => "Y",
				"GIFTS_SHOW_IMAGE" => "Y",
				"GIFTS_SHOW_NAME" => "Y",
				"GIFTS_SHOW_OLD_PRICE" => "Y",
				"HIDE_NOT_AVAILABLE" => "Y",
				"HIDE_NOT_AVAILABLE_OFFERS" => "Y",
				"IBLOCK_ID" => "8",
				"IBLOCK_TYPE" => "sections_elements",
				"INCLUDE_SUBSECTIONS" => "Y",
				"INSTANT_RELOAD" => "N",
				"LABEL_PROP" => "-",
				"LABEL_PROP_MOBILE" => "",
				"LABEL_PROP_POSITION" => "top-left",
				"LAZY_LOAD" => "N",
				"LINE_ELEMENT_COUNT" => "3",
				"LINK_ELEMENTS_URL" => "link.php?PARENT_ELEMENT_ID=#ELEMENT_ID#",
				"LINK_IBLOCK_ID" => "",
				"LINK_IBLOCK_TYPE" => "",
				"LINK_PROPERTY_SID" => "",
				"LIST_BROWSER_TITLE" => "-",
				"LIST_ENLARGE_PRODUCT" => "STRICT",
				"LIST_META_DESCRIPTION" => "-",
				"LIST_META_KEYWORDS" => "-",
				"LIST_OFFERS_FIELD_CODE" => array(
					0 => "ID",
					1 => "CODE",
					2 => "",
				),
				"LIST_OFFERS_LIMIT" => "0",
				"LIST_OFFERS_PROPERTY_CODE" => array(
					0 => "37",
					1 => "38",
					2 => "",
				),
				"LIST_PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
				"LIST_PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
				"LIST_PROPERTY_CODE" => array(
					0 => "VENDOR_ID",
					1 => "ARTICLE",
					2 => "MAIN_PHOTO",
					3 => "VENDOR_NAME",
					4 => "MIN_QUANTITY",
					5 => "MORE_PHOTO_LINK",
					6 => "CHARACTERISTICS",
					7 => "NEXT_LOT_QUANTITY",
					8 => "",
				),
				"LIST_PROPERTY_CODE_MOBILE" => "",
				"LIST_SHOW_SLIDER" => "Y",
				"LIST_SLIDER_INTERVAL" => "3000",
				"LIST_SLIDER_PROGRESS" => "N",
				"LOAD_ON_SCROLL" => "N",
				"MESSAGE_404" => "",
				"MESS_BTN_ADD_TO_BASKET" => "� �������",
				"MESS_BTN_BUY" => "������",
				"MESS_BTN_COMPARE" => "���������",
				"MESS_BTN_DETAIL" => "���������",
				"MESS_BTN_SUBSCRIBE" => "�����������",
				"MESS_COMMENTS_TAB" => "�����������",
				"MESS_DESCRIPTION_TAB" => "��������",
				"MESS_NOT_AVAILABLE" => "��� � �������",
				"MESS_PRICE_RANGES_TITLE" => "����",
				"MESS_PROPERTIES_TAB" => "��������������",
				"OFFERS_CART_PROPERTIES" => array(
				),
				"OFFERS_SORT_FIELD" => "shows",
				"OFFERS_SORT_FIELD2" => "shows",
				"OFFERS_SORT_ORDER" => "asc",
				"OFFERS_SORT_ORDER2" => "asc",
				"OFFER_ADD_PICT_PROP" => "-",
				"OFFER_TREE_PROPS" => "",
				"PAGER_BASE_LINK_ENABLE" => "N",
				"PAGER_DESC_NUMBERING" => "N",
				"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
				"PAGER_SHOW_ALL" => "N",
				"PAGER_SHOW_ALWAYS" => "Y",
				"PAGER_TEMPLATE" => "catalog",
				"PAGER_TITLE" => "������",
				"PAGE_ELEMENT_COUNT" => "48",
				"PARTIAL_PRODUCT_PROPERTIES" => "N",
				"PRICE_CODE" => array(
					0 => "BASE_PRICE",
				),
				"PRICE_VAT_INCLUDE" => "Y",
				"PRICE_VAT_SHOW_VALUE" => "N",
				"PRODUCT_DISPLAY_MODE" => "N",
				"PRODUCT_ID_VARIABLE" => "id",
				"PRODUCT_PROPERTIES" => array(
					0 => "CHARACTERISTICS",
				),
				"PRODUCT_PROPS_VARIABLE" => "prop",
				"PRODUCT_QUANTITY_VARIABLE" => "QUANTITY",
				"PRODUCT_SUBSCRIPTION" => "Y",
				"SEARCH_CHECK_DATES" => "Y",
				"SEARCH_NO_WORD_LOGIC" => "Y",
				"SEARCH_PAGE_RESULT_COUNT" => "50",
				"SEARCH_RESTART" => "N",
				"SEARCH_USE_LANGUAGE_GUESS" => "Y",
				"SECTIONS_SHOW_PARENT_NAME" => "Y",
				"SECTIONS_VIEW_MODE" => "LIST",
				"SECTION_ADD_TO_BASKET_ACTION" => "BUY",
				"SECTION_BACKGROUND_IMAGE" => "-",
				"SECTION_COUNT_ELEMENTS" => "Y",
				"SECTION_ID_VARIABLE" => "SECTION_ID",
				"SECTION_TOP_DEPTH" => "4",
				"SEF_FOLDER" => "/catalog/",
				"SEF_MODE" => "Y",
				"SET_LAST_MODIFIED" => "Y",
				"SET_STATUS_404" => "Y",
				"SET_TITLE" => "Y",
				"SHOW_404" => "N",
				"SHOW_DEACTIVATED" => "N",
				"SHOW_DISCOUNT_PERCENT" => "N",
				"SHOW_MAX_QUANTITY" => "N",
				"SHOW_OLD_PRICE" => "N",
				"SHOW_PRICE_COUNT" => "1",
				"SHOW_TOP_ELEMENTS" => "N",
				"SIDEBAR_DETAIL_SHOW" => "N",
				"SIDEBAR_PATH" => "",
				"SIDEBAR_SECTION_SHOW" => "Y",
				"TEMPLATE_THEME" => "site",
				"TOP_ADD_TO_BASKET_ACTION" => "BUY",
				"TOP_ELEMENT_COUNT" => "9",
				"TOP_ELEMENT_SORT_FIELD" => "sort",
				"TOP_ELEMENT_SORT_FIELD2" => "id",
				"TOP_ELEMENT_SORT_ORDER" => "asc",
				"TOP_ELEMENT_SORT_ORDER2" => "desc",
				"TOP_ENLARGE_PRODUCT" => "STRICT",
				"TOP_LINE_ELEMENT_COUNT" => "3",
				"TOP_OFFERS_FIELD_CODE" => array(
					0 => "",
					1 => "",
				),
				"TOP_OFFERS_LIMIT" => "5",
				"TOP_OFFERS_PROPERTY_CODE" => array(
					0 => "",
					1 => "",
				),
				"TOP_PRODUCT_BLOCKS_ORDER" => "price,props,sku,quantityLimit,quantity,buttons,compare",
				"TOP_PRODUCT_ROW_VARIANTS" => "[{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false},{'VARIANT':'2','BIG_DATA':false}]",
				"TOP_PROPERTY_CODE" => array(
					0 => "",
					1 => "",
				),
				"TOP_PROPERTY_CODE_MOBILE" => "",
				"TOP_ROTATE_TIMER" => "30",
				"TOP_SHOW_SLIDER" => "Y",
				"TOP_SLIDER_INTERVAL" => "3000",
				"TOP_SLIDER_PROGRESS" => "N",
				"TOP_VIEW_MODE" => "BANNER",
				"USE_ALSO_BUY" => "N",
				"USE_BIG_DATA" => "N",
				"USE_COMMON_SETTINGS_BASKET_POPUP" => "N",
				"USE_COMPARE" => "N",
				"USE_ELEMENT_COUNTER" => "Y",
				"USE_ENHANCED_ECOMMERCE" => "N",
				"USE_FILTER" => "N",
				"USE_GIFTS_DETAIL" => "N",
				"USE_GIFTS_MAIN_PR_SECTION_LIST" => "N",
				"USE_GIFTS_SECTION" => "N",
				"USE_MAIN_ELEMENT_SECTION" => "Y",
				"USE_PRICE_COUNT" => "Y",
				"USE_PRODUCT_QUANTITY" => "Y",
				"USE_SALE_BESTSELLERS" => "N",
				"USE_STORE" => "N",
				"USER_CONSENT" => "N",
				"USER_CONSENT_ID" => "0",
				"USER_CONSENT_IS_CHECKED" => "Y",
				"USER_CONSENT_IS_LOADED" => "N",
				"SEF_URL_TEMPLATES" => array(
					"sections" => "",
					"section" => "#SECTION_CODE_PATH#/",
					"element" => "#SECTION_CODE_PATH#/#ELEMENT_CODE#/",
					"compare" => "compare.php?action=#ACTION_CODE#",
					"smart_filter" => "#SECTION_ID#/filter/#SMART_FILTER_PATH#/apply/",
				),
				"VARIABLE_ALIASES" => array(
					"compare" => array(
						"ACTION_CODE" => "action",
					),
				)
			),
			false
		);?>
	</div>
	</div>

	<script>
	function getUrlVars()
	{
	    var vars = [], hash;
	    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	    for(var i = 0; i < hashes.length; i++)
	    {
	        hash = hashes[i].split('=');
	        vars.push(hash[0]);
	        vars[hash[0]] = hash[1];
	    }
	    return vars;
	}

	$(function(){
		//if($('.cb_block').size()<12) $('.wrap_pagenation').hide();
			var p = getUrlVars();
			//alert(p['maxprice']);
			if(p['minprice']) $('#cs1_minCost').val(p['minprice']);
			if(p['maxprice']) $('#cs1_maxCost').val(p['maxprice']);
			if(p['minquant']) $('#cs2_minCost').val(p['minquant']);
			if(p['maxquant']) $('#cs2_maxCost').val(p['maxquant']);
			if(typeof(p['sort']) != "function"){
				srt = p['sort'];
			} else {
				srt='';
			}
			if(!srt){srt=''};
			if(p['order']){ord=p['order']} else {ord=''}

			p['page']?page=p['page']:page=1;
		});
	$('#searchfilter').click(function(){
		var p=getUrlVars();
		if(p.length>0)
		{
			var currhref = window.location.href.substr(0, window.location.href.indexOf('?'));
		} else {
			var currhref = window.location.href;
		}
				last_char = currhref.charAt(currhref.length - 1)
				if (last_char == '/'){
					  getstr = window.location.href.substr(0, window.location.href.indexOf('?')) + '?'+'&page=' + page + '&minprice=' + $('#cs1_minCost').val() + '&maxprice='+ $('#cs1_maxCost').val() + '&minquant=' + $('#cs2_minCost').val() + '&maxquant=' + $('#cs2_maxCost').val() + '&sort=' + srt + '&order='+ord;}
				else {getstr = window.location.href.substr(0, window.location.href.indexOf('?')) + '?'+'&page=' + page + '&minprice=' + $('#cs1_minCost').val() + '&maxprice='+ $('#cs1_maxCost').val() + '&minquant=' + $('#cs2_minCost').val() + '&maxquant=' + $('#cs2_maxCost').val() + '&sort=' + srt + '&order='+ord;}
				location.replace(getstr);
			});
	</script>

	</section> 
<br>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>