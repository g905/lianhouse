<?
define("NEED_AUTH", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Восстановить пароль");
?>
<section class="content">
<div class="workspace">

<?
if(!$USER->IsAuthorized())
$APPLICATION->IncludeComponent(
  "bitrix:system.auth.forgotpasswd",
  "main",
  Array(
  "SHOW_ERRORS"=>"Y"
  )
);
elseif( !empty( $_REQUEST["backurl"] ) ){ LocalRedirect( $_REQUEST["backurl"] );}
	else{ LocalRedirect(SITE_DIR.'personal/profile');}
?>
</div>
</section>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
