//preloader
$(window).load(function () {
    $(".loader_inner").fadeOut();
    $(".loader").delay(400).fadeOut("slow");
});

$(function () {
    $('.support-button').on('click', function (ev) {
        ev.preventDefault();

        $(this).attr('disabled', 'disabled');

        var
            $button = $(this),
            $form = $button.closest('form'),
            obFields = {
                mail: $form.find('[name="mail"]'),
                message: $form.find('[name="message"]')
            },
            error = false;

        Object.keys(obFields).forEach(function (key) {
            var $field = obFields[key];
            if (!$field.val().trim()) {
                $field.addClass('error-input');
                error = true;
            }
            else {
                $field.removeClass('error-input');
            }
        });

        if (error) {
            $button.attr('disabled', false);
            return;
        }

        $.post('/ajax/support.php', {mail: obFields.mail.val(), message: obFields.message.val()}, function (data) {
            $button.attr('disabled', false);
            if (data.result) {
                alert('��� ������ ����� ��������� � ��������� �����. �������.');
                $form[0].reset();
                $form.find('.exit_nsf').click();
            }
            else if ('error-fields' == data.message) {
                alert('����������� ��������� ����');
            }
            else {
                alert(data.message);
            }
        }, 'json');
    });
});

//bxSlider
$('#main_slider').bxSlider({
    mode: 'horizontal',
    speed: 500,
    slideMargin: 0,
    startSlide: 0,
    randomStart: false,
    captions: false,
    ticker: false,
    tickerHover: false,
    adaptiveHeight: true,
    adaptiveHeightSpeed: 500,
    responsive: true,

    // PAGER
    pager: true,
    pagerType: 'full',
    pagerShortSeparator: ' / ',

    // CONTROLS
    controls: true,

    // AUTO
    auto: false,
    pause: 4000,

    // CAROUSEL
    minSlides: 1,
    maxSlides: 1,
    slideWidth: 0
});

//owlcarusel
$("#ms2_carousel").owlCarousel({
    // �������� ������ ����������� owl
    items: 3,
    responsive: { //��������� � ����������� �� ���������� ������
        0: {
            items: 1
        },
        600: {
            items: 2
        },
        850: {
            items: 3
        },
        900: {
            items: 3
        },
        1000: {
            items: 3
        }
    },

    // ���������
    nav: true,
    navText: ["<i class='fa fa-angle-left' aria-hidden='true'></i>", "<i class='fa fa-angle-right' aria-hidden='true'></i>"],
    rewindNav: true,
    scrollPerPage: false,

    // ���������
    pagination: false,
    paginationNumbers: false,

});


//$("#ms2_carousel .owl-item").equalHeights();


//slide menu goods (no slide if width window browser < 992px)
//win_w = $(window).width();
//if(win_w < 992){
$(".sidebar_m_top").on('click', function () {
    $('.sidebar_menu_body').slideToggle(200);
});
//}

//burger menu
$("#burger").click(function () {
    $(this).toggleClass("on");
    $("#burger_menu").slideToggle(200);
});


//slideToggle footerblock
$(".footer_but").click(function () {
    var el = $(this);
    el.parent().prev(".footer_top").toggle(200, function () {
        var el_class = el.children().children("i").attr("class");
        if (el_class == "fa fa-angle-down") {
            el.children().html('��������<i class="fa fa-angle-up" aria-hidden="true"></i>');
        } else {
            el.children().html('����������<i class="fa fa-angle-down" aria-hidden="true"></i>');
        }
    });
});


//hide - visible form registration
//visible
$(".registration").click(function (e) {
	e.preventDefault();
    $(".obertka_nsf").css('display', 'block').delay(100).queue(function () {  // delay() ��������� ������� �����
        $(".obertka_nsf").css('opacity', '1').css('overflow-y', 'hidden');
        $(".obertka_nsf").dequeue();
    });
});
$(".login").click(function (e) {
	e.preventDefault();
    $(".obertka_nsf3").css('display', 'block').delay(100).queue(function () {  // delay() ��������� ������� �����
        $(".obertka_nsf3").css('opacity', '1').css('overflow-y', 'hidden');
        $(".obertka_nsf3").dequeue();
    });
});

$(".login_btn").click(function(event) {
    event.preventDefault();
    var error = false;

    $(this).parents('form').find('.answer').html('');
    if ($(this).parents('form').find('input[name=login]').val() == '') {
        error = true;
        $(this).parents('form').find('.answer').append("<br>������� email");
    }
    if ($(this).parents('form').find('input[name=password]').val() == '') {
        error = true;
        $(this).parents('form').find('.answer').append("<br>������� ������");
    }


    if(error) return;

    $.ajax({
        url: '/local/ajax/login.php',
        type: 'POST',
        dataType: 'json',
        data: $(this).parents('form').serialize(),
    }).done(function(res) {
        if (res) {
            window.location.reload();
        } else {
            $('.obertka_nsf3').find('.answer').append("<br>������������ email ��� ������");
        }
    });

});

//registration
$(".registerBtn").click(function(event) {
    event.preventDefault();
    var error = false;
    var pattern = /^[-a-z0-9!#$%&'*+/=?^_`{|}~]+(?:\.[-a-z0-9!#$%&'*+/=?^_`{|}~]+)*@(?:[a-z0-9]([-a-z0-9]{0,61}[a-z0-9])?\.)*(?:aero|arpa|asia|biz|cat|com|coop|edu|gov|info|int|jobs|mil|mobi|museum|name|net|org|pro|tel|travel|[a-z][a-z])$/i;

    $(this).parents('form').find('.answer').html('');

    if ($(this).parents('form').find('input[name=email]').val() == '') {
        error = true;
        $(this).parents('form').find('.answer').append("������� email<br>");
    }
    if ($(this).parents('form').find('input[name=email]').val().search(pattern) != 0) {
        error = true;
        $(this).parents('form').find('.answer').append("������������ ������ email<br>");
    }
    if ($(this).parents('form').find('input[name=password]').val() == '') {
        error = true;
        $(this).parents('form').find('.answer').append("������� ������<br>");
    }
    if ($(this).parents('form').find('input[name=confirumPassword]').val() == '') {
        error = true;
        $(this).parents('form').find('.answer').append("������� ������������� ������<br>");
    }
    if ($(this).parents('form').find('input[name=confirumPassword]').val() != $(this).parents('form').find('input[name=password]').val()) {
        error = true;
        $(this).parents('form').find('.answer').append("������ �� ���������<br>");
    }


    if(error) return;

    $.ajax({
        url: '/local/ajax/registration.php',
        type: 'POST',
        dataType: 'json',
        data: $(this).parents('form').serialize(),
    }).done(function(res) {
//console.log(res);

        // $(this).parents('form').find('.answer').append('�� ������� ������������������, ��� ���������� ������ ��� ������������');
            $(res).each(function(index, el){
                var div = $('<div></div>');
                div.text(el);
                $('#answerRegister').append(div);
            });


//setTimeout(function(){window.location.reload()});
            if (res == 'success') {

//setTimeout(function(){window.location.reload()});
                $(".obertka_nsf").css('opacity', '0').delay(200).queue(function () {  // delay() ��������� ������� ����� ����� ����������� �������
                    $(".obertka_nsf").css('display', 'none');
                    $(".obertka_nsf").dequeue();
                });
                $(".obertka_nsf4").css('display', 'block').delay(100).queue(function () {  // delay() ��������� ������� �����
                    $(".obertka_nsf4").css('opacity', '1').css('overflow-y', 'hidden');
                    $(".obertka_nsf4").dequeue();
                });
                window.location.href = '/personal/profile/';
            } else {
                console.log(res);
            }
    });

});

//hide
$('.obertka_nsf,.obertka_nsf2,.obertka_nsf3,.obertka_nsf4,.exit_nsf').click(function () {         // ������ �������� ���������� �� ��������
    $(".obertka_nsf,.obertka_nsf2,.obertka_nsf3,.obertka_nsf4").css('opacity', '0').delay(200).queue(function () {  // delay() ��������� ������� ����� ����� ����������� �������
        $(".obertka_nsf,.obertka_nsf2,.obertka_nsf3,.obertka_nsf4").css('display', 'none');
        $(".obertka_nsf,.obertka_nsf2,.obertka_nsf3,.obertka_nsf4").dequeue();
    });
}).children().click(function (e) {        // ������ �� ��������
    e.stopPropagation();   // ������������� ��������������� �� ��������
});

//hide - visible form Ask a Question
//visible
$("#ask_question").click(function () {
    $(".obertka_nsf2").css('display', 'block').delay(100).queue(function () {  // delay() ��������� ������� �����
        $(".obertka_nsf2").css('opacity', '1').css('overflow-y', 'hidden');
        $(".obertka_nsf2").dequeue();
    });
});

$('.openLogin').click(function(event) {
    event.preventDefault();

    $('.obertka_nsf').hide();
    $(".obertka_nsf3").css('display', 'block').delay(100).queue(function () {  // delay() ��������� ������� �����
        $(".obertka_nsf3").css('opacity', '1').css('overflow-y', 'hidden');
        $(".obertka_nsf3").dequeue();
    });
});

$('.openRegister').click(function(event) {
    event.preventDefault();

    $('.obertka_nsf3').hide();
    $(".obertka_nsf").css('display', 'block').delay(100).queue(function () {  // delay() ��������� ������� �����
        $(".obertka_nsf").css('opacity', '1').css('overflow-y', 'hidden');
        $(".obertka_nsf").dequeue();
    });
});


// show-hide popup header location city
$(".hs1_city").click(function (e) {
    $(".location_popup").css('display', 'block').delay(100).queue(function () {  // delay() ��������� ������� �����
        $(".location_popup").css('opacity', '1').css('overflow-y', 'hidden');
        $(".location_popup").dequeue();
        $(".hide-popup").css("display", "block");
    });
});

$(".hide-popup").click(function (e) {
    $(".location_popup").css('opacity', '0').delay(200).queue(function () {  // delay() ��������� ������� ����� ����� ����������� �������
        $(".location_popup").css('display', 'none');
        $(".location_popup").dequeue();
        $(".hide-popup").css("display", "none");
    });
});
function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

//======================= catalog_slider1 =======================
var price_params = getUrlVars();

if(!price_params['minprice'])
{minpr=0;} else {minpr=price_params['minprice'];}

if(!price_params['maxprice'])
{maxpr=50000;} else {maxpr=price_params['maxprice'];}

if(!price_params['minquant'])
{minquant=1;} else {minquant=price_params['minquant'];}
if(!price_params['maxquant'])
{maxquant=1000;} else {maxquant=price_params['maxquant'];}

$("#catalog_slider1").slider({
    range: true,
    min: 0,
    max: 50000,
    values: [minpr, maxpr],
    stop: function (event, ui) {
        //if(jQuery("input#cs1_minCost").val() != d['minprice'])jQuery("input#cs1_minCost").val(jQuery("#catalog_slider1").slider("values", 0));
        jQuery("input#cs1_minCost").val(jQuery("#catalog_slider1").slider("values", 0));
        jQuery("input#cs1_maxCost").val(jQuery("#catalog_slider1").slider("values", 1));
    },
    slide: function (event, ui) {
        jQuery("input#cs1_minCost").val(jQuery("#catalog_slider1").slider("values", 0));
        jQuery("input#cs1_maxCost").val(jQuery("#catalog_slider1").slider("values", 1));
    }
});


jQuery("input#cs1_minCost").change(function () {
    var value1 = jQuery("input#cs1_minCost").val();
    var value2 = jQuery("input#cs1_maxCost").val();
    if (parseInt(value1) > parseInt(value2)) {
        value1 = value2;
        jQuery("input#cs1_minCost").val(value1);
    }
    jQuery("#catalog_slider1").slider("values", 0, value1);
});

jQuery("input#cs1_maxCost").change(function () {
    var value1 = jQuery("input#cs1_minCost").val();
    var value2 = jQuery("input#cs1_maxCost").val();
    if (value2 < value1) {
        value2 = value1;
        jQuery("input#cs1_maxCost").val(value1)
    }
    if (parseInt(value1) > parseInt(value2)) {
        value2 = value1;
        jQuery("input#cs1_maxCost").val(value2);
    }
    jQuery("#catalog_slider1").slider("values", 1, value2);
});


//======= catalog_slider2 =======================
$("#catalog_slider2").slider({
    range: true,
    min: 1,
    max: 1000,
    values: [minquant, maxquant],
    stop: function (event, ui) {
        jQuery("input#cs2_minCost").val(jQuery("#catalog_slider2").slider("values", 0));
        jQuery("input#cs2_maxCost").val(jQuery("#catalog_slider2").slider("values", 1));
    },
    slide: function (event, ui) {
        jQuery("input#cs2_minCost").val(jQuery("#catalog_slider2").slider("values", 0));
        jQuery("input#cs2_maxCost").val(jQuery("#catalog_slider2").slider("values", 1));
    }
});


jQuery("input#cs2_minCost").change(function () {
    var value1 = jQuery("input#cs2_minCost").val();
    var value2 = jQuery("input#cs2_maxCost").val();
    if (parseInt(value1) > parseInt(value2)) {
        value1 = value2;
        jQuery("input#cs2_minCost").val(value1);
    }
    jQuery("#catalog_slider2").slider("values", 0, value1);
});

jQuery("input#cs2_maxCost").change(function () {
    var value1 = jQuery("input#cs2_minCost").val();
    var value2 = jQuery("input#cs2_maxCost").val();
    if (value2 > 1000) {
        value2 = 1000;
        jQuery("input#cs2_maxCost").val(1000)
    }
    if (parseInt(value1) > parseInt(value2)) {
        value2 = value1;
        jQuery("input#cs2_maxCost").val(value2);
    }
    jQuery("#catalog_slider2").slider("values", 1, value2);
});


//======= catalog_slider3 =======================
$("#catalog_slider3").slider({
    range: true,
    min: 1,
    max: 8,
    values: [1, 8],
    stop: function (event, ui) {
        jQuery("input#cs3_minCost").val(jQuery("#catalog_slider3").slider("values", 0));
        jQuery("input#cs3_maxCost").val(jQuery("#catalog_slider3").slider("values", 1));
    },
    slide: function (event, ui) {
        jQuery("input#cs3_minCost").val(jQuery("#catalog_slider3").slider("values", 0));
        jQuery("input#cs3_maxCost").val(jQuery("#catalog_slider3").slider("values", 1));
    }
});


jQuery("input#cs3_minCost").change(function () {
    var value1 = jQuery("input#cs3_minCost").val();
    var value2 = jQuery("input#cs3_maxCost").val();
    if (parseInt(value1) > parseInt(value2)) {
        value1 = value2;
        jQuery("input#cs3_minCost").val(value1);
    }
    jQuery("#catalog_slider3").slider("values", 0, value1);
});

jQuery("input#cs3_maxCost").change(function () {
    var value1 = jQuery("input#cs3_minCost").val();
    var value2 = jQuery("input#cs3_maxCost").val();
    if (value2 > 1000) {
        value2 = 1000;
        jQuery("input#cs3_maxCost").val(1000)
    }
    if (parseInt(value1) > parseInt(value2)) {
        value2 = value1;
        jQuery("input#cs3_maxCost").val(value2);
    }
    jQuery("#catalog_slider3").slider("values", 1, value2);
});


// catalog hide-show block panel filter
/*$(".cc_exit").on("click", function () {
    $(this).parent(".cc_b1_body").css("max-height", "0");
});*/


$(".cc_b1_name").on("click", function () {
    $(this).next(".cc_b1_body").slideToggle(200);
    $(this).find('.fa-angle-down').toggleClass('rotated');
});


/*
 //catalog detail all variants hide-show
 $(".cdb1_all_variants").click(function(){
 $(this).parent().prev(".cdb1_variants_hide").toggleClass("cdb1_variants_show",function(){
 var el = $(this);
 //alert(el);
 var el_class = el.next().children().children().attr("class");
 //alert(el_class);
 if(el_class == "fa fa-caret-down"){
 el.next().children().html('������<i class="fa fa-caret-up" aria-hidden="true"></i>');
 }else{
 el.next().children().html('��� ��������<i class="fa fa-caret-down" aria-hidden="true"></i>');
 }
 });
 });*/



//$(function(){??????????????????????????????????????????????????????????????????????


$(".cc_destroy").on("click", function () {
    jQuery("#catalog_slider3").slider("values", 1, 8);
});

//});??????????????????????????????????????????????????????????????????????????


///////////////catalog sorting///////////////
// input select catalog
$('#catalog-sort').styler({
    selectPlaceholder: '������������ (�� ���������)',//$.cookie('url'),
    selectVisibleOptions: 5, // ���-�� ������������ ������� � ������� ��� ���������.
    singleSelectzIndex: 50, // ������� ���� � ��������������.
    selectSmartPositioning: true // "�����" ���������������� ����������� ������.
});

$('#catalog-limit').styler({
    selectPlaceholder: 18,
    selectVisibleOptions: 5, // ���-�� ������������ ������� � ������� ��� ���������.
    singleSelectzIndex: 50, // ������� ���� � ��������������.
    selectSmartPositioning: true, // "�����" ���������������� ����������� ������.
});

$('#catalog-sort').change(function() {
    var option = $(this).find('option:selected'),
		sort = $(option).data('sort'),
		prmName1 = 'sort',
		prmName2 = 'order',
		order = $(option).data('order'),
		res = '',
		d = location.href.split("#")[0].split("?"),
		base = d[0],
		query = d[1];
	$.cookie('sort', $(option).text());

	if(query) {
		var params = query.split("&");
		for(var i = 0; i < params.length; i++) {
			var keyval = params[i].split("=");
			if(keyval[0] != prmName1 && keyval[0] != prmName2) {
				res += params[i] + '&';
			}
		}
	}
	res += prmName1 + '=' + sort + '&' + prmName2 + '=' + order;
	window.location.href = base + '?' + res;
});

$('#catalog-limit').change(function() {
    var option = $(this).find('option:selected'),
		limit = $(option).data('limit'),
		prmName = 'limit',
		res = '',
		d = location.href.split("#")[0].split("?"),
		base = d[0],
		query = d[1];
	$.cookie('limit', $(option).text());

	if(query) {
		var params = query.split("&");
		for(var i = 0; i < params.length; i++) {
			var keyval = params[i].split("=");
			if(keyval[0] != prmName) {
				res += params[i] + '&';
			}
		}
	}
	res += prmName + '=' + limit;
	window.location.href = base + '?' + res;
});

function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

if(getUrlVars()['sort'] == 'date') {
	$('.cbt_wrap_select-sort .jq-selectbox__select-text').text('���� ���������� (����� ������ ������)');
} else if(getUrlVars()['sort'] == 'price' && getUrlVars()['order'] == 'asc') {
	$('.cbt_wrap_select-sort .jq-selectbox__select-text').text('���� �� �����������');
} else if(getUrlVars()['sort'] == 'price' && getUrlVars()['order'] == 'desc') {
	$('.cbt_wrap_select-sort .jq-selectbox__select-text').text('���� �� ��������');
}

$('.cbt_wrap_select-limit .jq-selectbox__select-text').text(getUrlVars()['limit']);

$('.cbt_wrap_select .jq-selectbox__dropdown li').each(function() {

    $(this).removeClass('sel selected');

    if ($(this).text() == $('.cbt_wrap_select-sort .jq-selectbox__select-text').text() || $(this).text() == $('.cbt_wrap_select-limit .jq-selectbox__select-text').text()) {
        $(this).addClass('sel selected');
    }
});


// input select question personal-area
$('.question_select').styler({
    selectPlaceholder: "�������� ���� �������",
    selectVisibleOptions: 5, // ���-�� ������������ ������� � ������� ��� ���������.
    singleSelectzIndex: 50, // ������� ���� � ��������������.
    selectSmartPositioning: true // "�����" ���������������� ����������� ������.
});
// input select question personal-area
$('.file_question').styler({
    filePlaceholder: "���������� ����"
});

//personal_area adress delivery form checkbox
$('.wcfb1_input').styler({});

//my profil download photo
$(".my_profil_download").styler({
    filePlaceholder: "��������� ����"
});


// hide messsage catalog
$(".cm_hide").on("click", function () {
    var el = $(this).children();
    var el_text = $(this).children().text();
    $(this).next(".dr_wrap_content").toggleClass('cat_message_dnone', function () {
        //alert(el_text);
        if (el_text == "������ ����������") {
            el.text("�������� ����������");
        } else {
            el.text("������ ����������");
        }
    });
})


// catalog lines-tabs
$(".cbv1").on("click", function () {
    $(".cbv2").removeClass("active_catalog");
    $(this).addClass("active_catalog");
    $(".catalog .cb_block").removeClass("line_catalog");
    $(".catalog .cb_block").addClass("tabs_catalog");
})

$(".cbv2").on("click", function () {
    $(".cbv1").removeClass("active_catalog");
    $(this).addClass("active_catalog");
    $(".catalog .cb_block").removeClass("tabs_catalog");
    $(".catalog .cb_block").addClass("line_catalog");
})


//cart plus minus quantity
$(".tb5_minus").on("click", function () {
    var numb = parseInt($(this).next().val()) - 1;
    if (numb >= 0) {
        $(this).next().val(numb);
    }
});
$(".tb5_plus").on("click", function () {
    var numb = parseInt($(this).prev().val()) + 1;
    $(this).prev().val(numb);
});


//personal_area top line
$(".pal_block").equalHeights();


// personal_area my_questions top a
$(".pamq_top a").on("click", function () {
    $(".pamq_top a").removeClass("pamq_active");
    $(this).addClass("pamq_active");
})


// personal_area chat show-hide
$(".pa_my_questions .cb_hide_show").click(function () {
    var el = $(this);
    el.prev(".cb_hide_block").toggle(200, function () {
        var el_class = el.children("i").attr("class");
        if (el_class == "fa fa-angle-up") {
            el.html('<span class="cb_line"></span>���������� ���<i class="fa fa-angle-down" aria-hidden="true"></i>');
        } else {
            el.html('<span class="cb_line"></span>�������� ���<i class="fa fa-angle-up" aria-hidden="true"></i>');
        }
    });
});


//personal_area chat show-hide
$(function () {
    $(".cbnb_show_answer").click(function () {
        $(this).parent().next().toggleClass("cbnb_bottom_show", function () {
        });
    });

});


/*//personal_area chat show-hide
 $(function(){
 $(".cbnb_show_answer").data("counter",0).click(function(){
 var counter = $(this).data('counter');
 $(this).data('counter', counter + 1);
 var qqq = $(this).data('counter');
 if(qqq%2){
 //alert(qqq);
 var el_text = $(this).text();
 }
 alert(el_text);
 //alert(el_text);
 var el = $(this);
 $(this).parent().next().toggleClass("cbnb_bottom_show",function(){
 if(el.text()==el_text){
 el.text("������ ������");
 }else{
 el.text(el_text);
 }
 });
 });

 });*/












// personal_area chat show-hide my order
$(".chat_order_wrap .cb_hide_show").click(function () {
    var el = $(this);
    el.prev(".cb_hide_block").toggle(200, function () {
        var el_class = el.children("i").attr("class");
        if (el_class == "fa fa-angle-up") {
            el.html('<span class="cb_line"></span>��������� ��������<i class="fa fa-angle-down" aria-hidden="true"></i>');
        } else {
            el.html('<span class="cb_line"></span>������ ��������<i class="fa fa-angle-up" aria-hidden="true"></i>');
        }
    });
});


//catalog detail magnific-popup gallery and owl-carusel

$('.magnific_popup_carousel').magnificPopup({
    mainClass: 'mfp-zoom-in',
    type: 'image',
    tLoading: '',
    gallery: {
        enabled: true,
    },
    removalDelay: 300,
    callbacks: {
        beforeChange: function () {
            this.items[0].src = this.items[0].src + '?=' + Math.random();
        },
        open: function () {
            $.magnificPopup.instance.next = function () {
                var self = this;
                self.wrap.removeClass('mfp-image-loaded');
                setTimeout(function () {
                    $.magnificPopup.proto.next.call(self);
                }, 120);
            }
            $.magnificPopup.instance.prev = function () {
                var self = this;
                self.wrap.removeClass('mfp-image-loaded');
                setTimeout(function () {
                    $.magnificPopup.proto.prev.call(self);
                }, 120);
            }
        },
        imageLoadComplete: function () {
            var self = this;
            setTimeout(function () {
                self.wrap.addClass('mfp-image-loaded');
            }, 16);
        }
    }
});


/*
 $(function() {
 $("#cdb1_carousel_img").owlCarousel({
 items : 4,
 itemsDesktop : [1199,3],
 itemsMobile:	[550,3],
 // ���������
 nav : true,
 navigationText : ["<i class='fa fa-chevron-left' aria-hidden='true'></i>","<i class='fa fa-chevron-right' aria-hidden='true'></i>"],
 rewindNav : true,
 scrollPerPage : false,
 // ���������
 pagination : false,
 paginationNumbers: false,
 // ������������
 responsive: true
 });
 });
 */

//
$("#cdb1_carousel_img").owlCarousel({
    items: 4,
    nav: true,
    navText: ["<i class='fa fa-chevron-left' aria-hidden='true'></i>", "<i class='fa fa-chevron-right' aria-hidden='true'></i>"],
    rewindNav: true,
    margin: 3
});


//catalog detail variants color radio
$(".cdb1_mt_color input").styler();


//catalog detail all variants hide-show
$(".cdb1_all_variants").click(function () {
    $(this).parent().prev(".cdb1_variants_hide").toggleClass("cdb1_variants_show", function () {
        var el = $(this);
        //alert(el);
        var el_class = el.next().children().children().attr("class");
        //alert(el_class);
        if (el_class == "fa fa-caret-down") {
            el.next().children().html('������<i class="fa fa-caret-up" aria-hidden="true"></i>');
        } else {
            el.next().children().html('��� ��������<i class="fa fa-caret-down" aria-hidden="true"></i>');
        }
    });
});


//catalog detail tabs
$(function () {
    $("#cdb1_tabs").tabs();
});


//catalog detail magnific-popup tabs imgs
$('.cdb1_w_img .cdb1_wrap_img').magnificPopup({
    mainClass: 'mfp-zoom-in',
    type: 'image',
    tLoading: '',
    gallery: {
        enabled: true,
    },
    removalDelay: 300,
    callbacks: {
        beforeChange: function () {
            this.items[0].src = this.items[0].src + '?=' + Math.random();
        },
        open: function () {
            $.magnificPopup.instance.next = function () {
                var self = this;
                self.wrap.removeClass('mfp-image-loaded');
                setTimeout(function () {
                    $.magnificPopup.proto.next.call(self);
                }, 120);
            }
            $.magnificPopup.instance.prev = function () {
                var self = this;
                self.wrap.removeClass('mfp-image-loaded');
                setTimeout(function () {
                    $.magnificPopup.proto.prev.call(self);
                }, 120);
            }
        },
        imageLoadComplete: function () {
            var self = this;
            setTimeout(function () {
                self.wrap.addClass('mfp-image-loaded');
            }, 16);
        }
    }
});


//catalog detail similar-products carousel
$(document).ready(function () {
    $("#cdb1_similar_carousel").owlCarousel({
        items: 4,
        margin: 30,
        nav: true,
        navText: ["<i class='fa fa-chevron-left' aria-hidden='true'></i>", "<i class='fa fa-chevron-right' aria-hidden='true'></i>"],
        responsive: { //��������� � ����������� �� ���������� ������
            0: {
                items: 1
            },
            500: {
                items: 2
            },
            550: {
                items: 2
            },
            750: {
                items: 3
            },
            900: {
                items: 3
            },
            1000: {
                items: 4
            }
        }
    });
});

$(document).ready(function() {
//catalog detail plus minus quantity
    $('#QUANTITY').keypress(function (event) {
        var key, keyChar;

        if (!event) var event = window.event;

        if (event.keyCode) key = event.keyCode;
        else if (event.which) key = event.which;

        if (key == null || key == 0 || key == 8 || key == 13 || key == 9 || key == 46 || key == 37 || key == 39) return true;
        keyChar = String.fromCharCode(key);

        if (!/\d/.test(keyChar))    return false;
    });
    $('#QUANTITY').on('change', function () {
        var input = $(this),
            maxVal = $(this).data('max');

        if (input.val() <= 0) {
            return input.val(1);
        }
    });

    $('.add_form, #basket_form').keydown(function (event) {
        if (event.keyCode == 13) {
            event.preventDefault();
            return false;
        }
    });

    // $('#basket_form .input__basket_form--block').keydown(function(e){
    //     e.preventDefault()
    // });

    $('.popup-with-move-anim').magnificPopup({
        //type: 'inline',
        fixedContentPos: false,
        fixedBgPos: false,

        overflowY: 'auto',

        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-slide-bottom'
    });


    var wishcount = $('.empty_wishlist').data('wishcount');

    if (wishcount == 0) {
        $('.wishlist_top').css('display', 'none');
        $('.clean_wishlist').css('display', 'none');
    } else {
        $('.wishlist_top').css('display', 'block');
        $('.clean_wishlist').css('display', 'block');
    }
});

function add2wish(p_id, pp_id, p, name, dpu, th) {
	if ($(th).find(".wishbtn .fa-heart").hasClass("add_favorive")) {
		$.ajax({
	        type: "POST",
	        url: "/local/ajax/clean_product.php",
	        data: "p_id=" + p_id + "&pp_id=" + pp_id + "&p=" + p + "&name=" + name + "&dpu=" + dpu,
	        success: function(html){
                $(th).find('.wishbtn .fa-heart').removeClass('add_favorive');
				$(th).find('.wishbtn a').attr('href', '#add_to_wish');
                $(th).find('.wishbtn_card_product a').html('<i class="fa fa-heart" aria-hidden="true"></i>���������');
	        }
	    });
	}
	else {
		$.ajax({
	        type: "POST",
	        url: "/local/ajax/wishlist.php",
	        data: "p_id=" + p_id + "&pp_id=" + pp_id + "&p=" + p + "&name=" + name + "&dpu=" + dpu,
	        success: function(html){
				$(th).find('.wishbtn .fa-heart').addClass('add_favorive');
                $(th).find('.wishbtn a').attr('href', '#remove_to_wish');
                $(th).find('.wishbtn_card_product a').html('<i class="fa fa-heart add_favorive" aria-hidden="true"></i>� ���������');
	        }
	    });
	}
}

   $(document).ready(
      function()
      {
         var url = $('#basket-container a').data('url');
         var options = {
         url: url + '?RND=' + Math.random(),
         type: "POST",
         target: '#basket-container',
         success:
            function(responseText) {
               $.jGrowl("����� �������� � �����.");

            }
         };
         $(".add_form").ajaxForm(options);
      }
   );

   $(document).ready(function() {
       var listMenu = $('.sidebar_menu_body');

       if($(window).width() < 481) {
            listMenu.hide();
       }
   });

var lis = document.getElementsByClassName('smb_li1');
$(document).ready(function() {

    for(var i=0; i<=lis.length-1; i++){
        if(i>=12){
            if(window.localStorage["cat_show"] != "Y"){
                lis[i].classList.add('hide');
            }
        } else {
            lis[i].classList.add('active');
        }
}
});

function sh(){
    if(window.localStorage["cat_show"] == "Y"){
        window.localStorage["cat_show"] = "N";
    } else {
        window.localStorage["cat_show"] = "Y";
    }
        for(var i=0; i<lis.length; i++){
            if(!lis[i].classList.contains('active'))
            {
                lis[i].classList.toggle('hide');
            }
        }
        return null;
        }
