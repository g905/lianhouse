<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

//---------- class.php ----------//
$MESS['AQAL_COMP_DATE_FORMAT']    = 'j F \� H:i';
$MESS['AQAL_COMP_DATE_FORMAT_IN'] = '�';

$MESS['alert_labelOk']     = '��';
$MESS['alert_labelCancel'] = '������';


//---------- script.js ----------//
$MESS['AQAL_JS_MESS_ALLOW_USER']   = '�������� ����� ������ �������������� ������������';
$MESS['AQAL_JS_MESS_ALLOW_EDITOR'] = '�������� ����� ������ ��������� �����';

//---------- ajax.php ----------//

//Post fields errors
$MESS['API_QALIST_AJAX_ERROR_TEXT']        = '������ ������������';
$MESS['API_QALIST_AJAX_ERROR_GUEST_NAME']  = '���� ��� ������������';
$MESS['API_QALIST_AJAX_ERROR_GUEST_EMAIL'] = '��. ����� ������������';
$MESS['API_QALIST_AJAX_ERROR_EMAIL_VALID'] = '��. ����� ��������� �������';

//Some errors
$MESS['API_QALIST_AJAX_ERROR_SESSION_EXPIRED'] = '���� ������ �������, �������� ��������, ���� ������ ����� �����������, �������� ��������� �����';
$MESS['API_QALIST_AJAX_ERROR_EMPTY_FORM']      = '����� �������/������ ������, �������� ��������� �����';

