<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
global $USER;
$id_user = $USER->GetID(); // ID ������������
$group_user = $USER->GetUserGroupArray();
IncludeTemplateLangFile(__FILE__);
//if ($USER->IsAdmin()) echo "�� �������������!";
//var_dump($id_user);
/**
 * Bitrix vars
 *
 * @var CBitrixComponentTemplate $this
 * @var ApiQaList                $component
 *
 * @var array                    $arParams
 * @var array                    $arResult
 *
 * @var string                   $templateName
 * @var string                   $templateFile
 * @var string                   $templateFolder
 * @var array                    $templateData
 *
 * @var string                   $componentPath
 * @var string                   $parentTemplateFolder
 *
 * @var CDatabase                $DB
 * @var CUser                    $USER
 * @var CMain                    $APPLICATION
 */

//$this - ������ �������
//$component - ������ ����������

//$this->GetFolder()
//$tplId = $this->GetEditAreaId($arResult['ID']);

//������ ������������� ����������
//$parent = $component->getParent();
//$parentPath = $parent->getPath();

use Bitrix\Main\Web\Json,
	 Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

if(method_exists($this, 'setFrameMode'))
	$this->setFrameMode(true);

//plugins
//$this->addExternalCss($templateFolder . '/plugins/modal/api.modal.css');
$this->addExternalCss($templateFolder . '/plugins/form/api.form.css');
$this->addExternalCss($templateFolder . '/plugins/button/api.button.css');
//$this->addExternalJs($templateFolder . '/plugins/modal/api.modal.js');

if($arParams['INCLUDE_CSS'] == 'Y') {
	$this->addExternalCss($templateFolder . '/theme/' . $arParams['THEME'] . '/style.css');
}
//������, ������� ����� ������������� ��� ��������� � �������� �� ���
$group_admin = $arParams["HASH"];
$group_admin = str_replace ("?question=#ID#", "", $group_admin);
$admins = explode(",",$group_admin);
foreach($admins as $admin){ 
	foreach ($group_user as $user){
		if ($user == $admin){$is_editor = true;}
	}
}
?>

	<div class="api-qa api-qa-theme-<?=$arParams['THEME']?> api-qa-color-<?=$arParams['COLOR']?>">

	<!--api-qa-form-->
<form class="wrap_question" action="" method="" enctype="multipart/form-data">
	<div class="api-qa-form">
		<div class="api-title"><?//=$arParams['FORM_QUESTION_MESS_TITLE']?></div>
		<div class="api-form">
			<input class="api-field" type="hidden" name="TYPE" value="Q">
			<? if(!$USER->IsAuthorized()): ?>
				<div class="api-row api-guest">
					<div class="api-controls">
						<div class="api-control">
							<input type="text" name="GUEST_NAME" class="api-field" value="" placeholder="<?=$arParams['FORM_QUESTION_MESS_NAME']?>">
						</div>
						<div class="api-control">
							<input type="text" name="GUEST_EMAIL" class="api-field" value="" placeholder="<?=$arParams['FORM_QUESTION_MESS_EMAIL']?>">
						</div>
					</div>
				</div>
			<? endif ?>

			<div class="api-row">
			<div class="top_question">
				<div class="api-controls">
				<div class="api-control">
				<p><?=GetMessage('QUESTIONS_FAQ_THEMESELECT')?></p>
							<select class="question_select" name="" id="q_theme" name="q_theme" onChange="ChangeTheme()">
							<?
							$q_themes = explode(",",$arParams['FORM_QUESTION_MESS_NAME']);
							foreach($q_themes as $q_theme){ 
							?>
							
							  <option><?=$q_theme?></option>
							
							<?}?>
							</select>
							
						<div class="api-control">
							<input id="iq_theme" type="hidden" name="GUEST_NAME" class="api-field" value="<?=$q_themes[0]?>" placeholder="<?=$arParams['FORM_QUESTION_MESS_NAME']?>">
						</div>
						</div>
					</div>
				</div>
			<div class="bottom_question">
				<p class="bq_name"><?= getMessage('QUESTIONS_FAQ_HEADER')?></p>
				<div class="bq_elem">
						<div class="api-control">
						
						</div>						
					<div class="api-control" style="position:relative">
						<textarea id="question_text" name="TEXT" class="api-field" data-autoresize="" placeholder=""></textarea>
				<div class="file" style="position:relative">
						<?$APPLICATION->IncludeComponent("bitrix:main.file.input", "drag_n_drop",
						   array(
							  "INPUT_NAME"=>"DOPFILE",
							  "MULTIPLE"=>"Y",
							  "MODULE_ID"=>"main",
							  "MAX_FILE_SIZE"=>"",
							  "ALLOW_UPLOAD"=>"I", 
							  "ALLOW_UPLOAD_EXT"=>""
						   ),
						   false
						);?>
						</div>
						
					</div>
				</div>
				
			<form id="linksform">
							<input type="hidden" name="LANG" class="api-field" value="<?= getMessage('FAQ_ATTACHMENT')?>">
							 
						</form>
			

			11111111111
			
			<div class="api-row api-buttons">
				<div class="api-controls">
					<div class="api-control">
						<? if(!$USER->IsAuthorized()): ?>
							<? if($arParams['USE_PRIVACY'] && $arParams['MESS_PRIVACY']): ?>
								<div class="api-privacy">
									<input type="checkbox" name="PRIVACY_ACCEPTED" value="Y" class="api-field" <?=$arResult['PRIVACY_ACCEPTED'] == 'Y' ? ' checked' : ''?>>
									<? if($arParams['MESS_PRIVACY_LINK']): ?>
										<a rel="nofollow" href="<?=$arParams['MESS_PRIVACY_LINK']?>" target="_blank"><?=$arParams['MESS_PRIVACY']?></a>
									<? else: ?>
										<?=$arParams['MESS_PRIVACY']?>
									<? endif ?>
								</div>
							<? endif ?>
						<? endif ?>
						<button type="button" class="button2"
						        onclick="$.fn.apiQaList('question',this)"><?= getMessage('QUESTIONS_FAQ_BUTTON')?></button>
</div>
					</div>
				</div>
				</div>
			</div>
<div class="clear"></div>
			<p class="question_faq">
									<?= getMessage('QUESTIONS_FAQ_DESC')?>
								</p>
		</div>
	</div>
</form>
<div style="display:none">
	<!--api-qa-list-->
	<div class="api-qa-list">
		<div class="api-items">
			<? if($arResult['ITEMS']): ?>
				<? foreach($arResult['ITEMS'] as $arItem): ?>
					<?
					if ($is_editor || ($arItem['USER_ID'] == $id_user)){include 'ajax_tpl.php';}
					?>
				<? endforeach; ?>
			<? endif ?>
		</div>
	</div>

	<!--api-qa-unswer-->
	<?if($arParams['IS_ALLOW']):?>
	<div class="api-qa-form-unswer">
		<div class="api-form">
			<input class="api-field" type="hidden" name="TYPE" value="<?=($arParams['IS_EDITOR'] ? 'A' : 'C')?>">
			<input class="api-field" type="hidden" name="LEVEL" value="">
			<input class="api-field" type="hidden" name="PARENT_ID" value="">
			<? if(!$USER->IsAuthorized()): ?>
				<div class="api-row api-guest">
					<div class="api-controls">
						<div class="api-control">
							<input type="text" name="GUEST_NAME" class="api-field" value="" placeholder="<?=$arParams['FORM_ANSWER_MESS_NAME']?>">
						</div>
						<div class="api-control">
							<input type="text" name="GUEST_EMAIL" class="api-field" value="" placeholder="<?=$arParams['FORM_ANSWER_MESS_EMAIL']?>">
						</div>
					</div>
				</div>
			<? endif ?>
			<div class="api-row">
				<div class="api-controls">
					<div class="api-control">
						<textarea name="TEXT" class="api-field" data-autoresize="" placeholder="<?=$arParams['FORM_ANSWER_MESS_TEXT']?>"></textarea>
					</div>
				</div>
			</div>
			<div class="api-row api-buttons">
				<div class="api-controls">
					<? if(!$USER->IsAuthorized()): ?>
						<? if($arParams['USE_PRIVACY'] && $arParams['MESS_PRIVACY']): ?>
							<div class="api-control api-privacy">
								<input type="checkbox" name="PRIVACY_ACCEPTED" value="Y" class="api-field" <?=$arResult['PRIVACY_ACCEPTED'] == 'Y' ? ' checked' : ''?>>
								<? if($arParams['MESS_PRIVACY_LINK']): ?>
									<a rel="nofollow" href="<?=$arParams['MESS_PRIVACY_LINK']?>" target="_blank"><?=$arParams['MESS_PRIVACY']?></a>
								<? else: ?>
									<?=$arParams['MESS_PRIVACY']?>
								<? endif ?>
							</div>
						<? endif ?>
					<? endif ?>
					<div class="api-control">
						<button type="button"
						        class="api-button api-form-submit"
						        onclick="$.fn.apiQaList('answer',this)"><?=$arParams['FORM_ANSWER_MESS_SUBMIT']?></button>
						<input type="checkbox" name="NOTIFY" class="api-field" checked=""> <?=$arParams['FORM_ANSWER_MESS_REPLY']?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<? endif ?>
</div><!--invisible-->
</div>
<script>
	jQuery(document).ready(function ($) {
		$.fn.apiQaList(<?=Json::encode($component->getAjaxParams($this))?>);
		<?if($arResult['SCROLL_TO']):?>
		$(window).on('load',function(){
			var api_qa_item = '#api_qa_item_<?=$arResult['SCROLL_TO']?>';
			$('html, body').animate({
				scrollTop: $(api_qa_item).offset().top
			}, 400, function () {
				$(api_qa_item).addClass('api-active');
			});
		});
		<?endif?>
	});
	function ChangeTheme(){
    		var theme = $('#q_theme').val();
    		$('#iq_theme').val(theme);
    	}
</script>
