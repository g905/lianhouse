<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

$APPLICATION->SetAdditionalCSS($templateFolder."/style_cart.css");
$APPLICATION->SetAdditionalCSS($templateFolder."/style.css");

CJSCore::Init(array('fx', 'popup', 'window', 'ajax'));
?>

<a name="order_form"></a>

<div id="order_form_div" class="wrap_cart_form_blocks">
<NOSCRIPT>
	<div class="errortext"><?=GetMessage("SOA_NO_JS")?></div>
</NOSCRIPT>

<?
if (!function_exists("getColumnName"))
{
	function getColumnName($arHeader)
	{
		return (strlen($arHeader["name"]) > 0) ? $arHeader["name"] : GetMessage("SALE_".$arHeader["id"]);
	}
}
?>

<div class="bx_order_make">
	<?
	if(!$USER->IsAuthorized() && $arParams["ALLOW_AUTO_REGISTER"] == "N")
	{
		if(!empty($arResult["ERROR"]))
		{
			foreach($arResult["ERROR"] as $v)
				echo ShowError($v);
		}
		elseif(!empty($arResult["OK_MESSAGE"]))
		{
			foreach($arResult["OK_MESSAGE"] as $v)
				echo ShowNote($v);
		}

		include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/auth.php");
	}
	else
	{
		if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y")
		{
			if(strlen($arResult["REDIRECT_URL"]) > 0)
			{
				?>
				<script type="text/javascript">
				window.top.location.href='<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';

				</script>
				<?
				die();
			}
			else
			{
				include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/confirm.php");
			}
		}
		else
		{
			?>
			<script type="text/javascript">
			function submitForm(val)
			{
				if(val != 'Y')
					BX('confirmorder').value = 'N';

				var orderForm = BX('ORDER_FORM');

				BX.ajax.submitComponentForm(orderForm, 'order_form_content', true);
				BX.submit(orderForm);

				return true;
			}

			function SetContact(profileId)
			{
				BX("profile_change").value = "Y";
				submitForm();
			}
			</script>
			<?if($_POST["is_ajax_post"] != "Y")
			{
				?><form action="<?=$APPLICATION->GetCurPage();?>" method="POST" name="ORDER_FORM" id="ORDER_FORM" enctype="multipart/form-data">
				<?=bitrix_sessid_post()?>
				<div id="order_form_content">
				<?
			}
			else
			{
				$APPLICATION->RestartBuffer();
			}
			if(!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y")
			{
				foreach($arResult["ERROR"] as $v)
					echo ShowError($v);

				?>
				<script type="text/javascript">
					top.BX.scrollToNode(top.BX('ORDER_FORM'));
				</script>
				<?
			}

			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/person_type.php");
			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props.php");

			include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/related_props.php");

			//include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/summary.php");
			if(strlen($arResult["PREPAY_ADIT_FIELDS"]) > 0)
				echo $arResult["PREPAY_ADIT_FIELDS"];
			?>
                    <div class="wcf_block wcf_block2">
                        <div class="wcf_block_inner">
                            <p class="wcf_top"><?=GetMessage("SOA_TEMPL_SUM_COMMENTS")?></p>
                            <div class="wcf_body2"><textarea name="ORDER_DESCRIPTION" id="ORDER_DESCRIPTION" placeholder="�������� ��������� ��� ������ ���������"><?=$arResult["USER_VALS"]["ORDER_DESCRIPTION"]?></textarea></div>
                            <input type="hidden" name="" value="">
                            <div style="clear: both;"></div><br />
                        </div>
                    </div>
                    <div class="wcf_block_b wcf_block_b1">
                        <?
                        if ($arParams["DELIVERY_TO_PAYSYSTEM"] == "p2d")
                        {
                            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/paysystem.php");
                            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");
                        }
                        else
                        {
                            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");
                            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/paysystem.php");
                        }
                        ?>
                    </div>

                    <div class="bx_ordercart_order_pay_center"><a href="javascript:void();" onClick="submitForm('Y'); return false;" class="checkout"><?=GetMessage("SOA_TEMPL_BUTTON")?></a></div>


			<?if($_POST["is_ajax_post"] != "Y")
			{
				?>
                    <div class="wcf_block_b wcf_block_b2">
                        <p class="wcfbb2_p1"><span>��������� ������� (<?=count($arResult["BASKET_ITEMS"])?>): </span><i id="order--total-amount" style="font-style: normal;"><?=$arResult["ORDER_TOTAL_PRICE_FORMATED"]?></i></p>
                        <p class="wcfbb2_p2"><span>��������� ��������: </span>�� ���������</p>
                        <div class="wcfbb2_wrap_but">
<!--                            <a href="javascript:void();" onClick="submitForm('Y'); return false;">--><?//=GetMessage("SOA_TEMPL_BUTTON")?><!--</a>-->
                            <p><span>*</span>��������� �������� ��������������<br> ������������� ����� ����������.</p>
                        </div>
                    </div>
            </div>
			</div>
			        <input type="hidden" name="confirmorder" id="confirmorder" value="Y">
					<input type="hidden" name="profile_change" id="profile_change" value="N">
					<input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y">
                </form>
				<?
				if($arParams["DELIVERY_NO_AJAX"] == "N")
				{
					$APPLICATION->AddHeadScript("/bitrix/js/main/cphttprequest.js");
					$APPLICATION->AddHeadScript("/bitrix/components/bitrix/sale.ajax.delivery.calculator/templates/.default/proceed.js");
				}
			}
			else
			{
				?>
					<script type="text/javascript">
						top.BX('confirmorder').value = 'Y';
						top.BX('profile_change').value = 'N';
					</script>
				<?
				die();
			}
		}
	}
	?>

</div>