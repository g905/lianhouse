<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */

$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();

foreach ($arResult["OFFERS"] as $i => $offer) {
    foreach ($offer["PROPERTIES"] as $j => $offerProperty) {
        if (!$offerProperty["VALUE_ENUM_ID"])
            continue;

        $offerProperty["OFFER"] = $offer;
        $arResult["CUSTOM_PROPERTIES"][$offerProperty["CODE"]][$offerProperty["VALUE_ENUM_ID"]]["OFFERS"][$offer["ID"]] = $offer;
        $arResult["CUSTOM_PROPERTIES"][$offerProperty["CODE"]][$offerProperty["VALUE_ENUM_ID"]]["PROPERTY"] = $offerProperty;
    }
}