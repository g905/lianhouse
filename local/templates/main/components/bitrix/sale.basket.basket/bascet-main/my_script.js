function recalculate_ranges () {

  table = $('#basket_items');
  rows = table.find('tr');
  sum = 0;
  rows.each(function(index) {

    if (el_id = $(this).attr("id")) {
      div = $(this).find(".final_price_usd");
      div_ru = $(this).find("#current_price_"+el_id);
      div_all_usd = $(this).find(".allSum_usd");
      div_all_rub = $(this).find("#sum_"+el_id);

      quant = parseFloat($(this).find("#QUANTITY_INPUT_"+el_id).val());

      if(rr = $(this).find(".ranges").length > 0)
      {
        rr = $(this).find(".ranges");
        rr.each(function(index1) {
          qu = parseFloat($(this).find('.quant').text());
          //console.log($(this).find('.quant'));
          if(quant >= qu) {
            text_usd = $(this).find('.price_usd').text();
            text_ru = $(this).find('.price_ru').text();
            div.text("$"+text_usd);
            div_ru.text(text_ru+"�.");
            div_all_usd.text("$"+(text_usd*quant).toFixed(2));
            div_all_rub.text((text_ru*quant).toFixed(2)+"���.");

            sum += (text_ru*quant);

            //return;
          }
        });
      } else if ($(this).find('.master_price')) {
        //console.log($(this).find('.master_rub').text());
        text_usd = $(this).find('.master_usd').text();
        text_ru = parseFloat($(this).find('#current_price_'+el_id).text());
        //$(this).find('.master_rub').text()
        inner_txt_usd = "$"+(parseFloat($(this).find("#QUANTITY_INPUT_"+el_id).val())*text_usd).toFixed(2);
        inner_txt_ru = (parseFloat($(this).find("#QUANTITY_INPUT_"+el_id).val())*text_ru).toFixed(2)+"���.";
        $(this).find('.allSum_usd').text(inner_txt_usd);
        $(this).find("#sum_"+el_id).text(inner_txt_ru);
        sum += parseFloat($(this).find("#QUANTITY_INPUT_"+el_id).val())*text_ru;
      }
    }
  });

  $("#allSum_FORMATED").text(sum.toFixed(2)+"�");
  $("#order--total-amount").text(sum.toFixed(2)+"�");

}

function recalculate () {

  table = $('#basket_items');
  rows = table.find('tr');
  sum = 0;
  rows.each(function(index) {

    if (el_id = $(this).attr("id")) {
      quant = parseFloat($(this).find("#QUANTITY_INPUT_"+el_id).val());

      div = $(this).find(".final_price_usd");
      div_ru = $(this).find("#current_price_"+el_id);
      div_all_usd = $(this).find(".allSum_usd");
      div_all_rub = $(this).find("#sum_"+el_id);



      if(rr = $(this).find(".ranges").length > 0)
      {
        rr = $(this).find(".ranges");
        range1 = parseFloat(rr.find('.price_usd')[0].innerText);
        div_all_usd.text("$"+(range1*quant).toFixed(2));
        div.text("$"+range1);
        sum += (range1*quant);
      } else if ($(this).find('.master_price')) {
        text_usd = $(this).find('.master_usd').text();
        text_ru = parseFloat($(this).find('#current_price_'+el_id).text());
        inner_txt_usd = "$"+(parseFloat($(this).find("#QUANTITY_INPUT_"+el_id).val())*text_usd).toFixed(2);
        inner_txt_ru = (parseFloat($(this).find("#QUANTITY_INPUT_"+el_id).val())*text_ru).toFixed(2)+"���.";
        $(this).find('.allSum_usd').text(inner_txt_usd);
        $(this).find("#sum_"+el_id).text(inner_txt_ru);
        sum += parseFloat(quant*text_usd);
      }
    }
  });

  $("#allSum_FORMATED_usd").text("$"+sum.toFixed(2));

  fl = $(".order_usd").text();
  if (!fl) {
    $("<div class='order_usd'>$"+sum.toFixed(2)+"</div>").insertBefore("#order--total-amount");
  } else {
    $(".order_usd").text("$"+sum.toFixed(2));
  }


}

jQuery(function($) {

  recalculate();

});
