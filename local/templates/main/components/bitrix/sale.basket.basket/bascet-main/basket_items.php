<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
use Bitrix\Sale\DiscountCouponsManager;
use \Bitrix\Main\Config\Option;
                require($_SERVER["DOCUMENT_ROOT"].'/parser/functions.php');
                require($_SERVER["DOCUMENT_ROOT"].'/parser/config.php');
                // var_dump($arResult["ID"]);
                $id_iblock   = 8;
$min_price = Option::get('imyie.orderminprice', 'min_price', 0);
if (!empty($arResult["ERROR_MESSAGE"]))
    ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;
$bPriceType    = false;

if ($normalCount > 0):
    ?>
    <style media="screen">
      .range-table {
        display: table;
      }
      .quant, .price_ru, .price_usd {
        display: inline-block;
        border: 1px solid black;
      }

      .range-table {
        display: none;
      }
      #allSum_FORMATED_usd {
        display: inline-block;
        font-size: 1.225em;
        color: #000;
      }
      .final_price_usd, .allSum_usd, #allSum_FORMATED_usd, .order_usd, .master_usd {
        font-weight: bold;
      }
    </style>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH.'/components/bitrix/sale.basket.basket/bascet-main/my_script.js');?>

    <div id="basket_items_list">
        <div class="wrap_table_cart">
            <div id="simplecheckout-warning-block-sum">
                <? $min_sum = (float)(preg_replace("/[^,.0-9]/", '', $arResult["allSum_FORMATED"]));
                if ($min_sum < 15000):?>
                <div class="simplecheckout-warning-block"><p>����������� ����� ������ ���������� <?=$min_price?> ��� ��� (6500 ������)!</p></div>
                <? endif;?>
            </div>
            <table id="basket_items" class="table_cart">
                <thead>
                <tr>

                    <?
                    foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):
                        $arHeader["name"] = (isset($arHeader["name"]) ? (string)$arHeader["name"] : '');
                        if ($arHeader["name"] == '')
                            $arHeader["name"] = GetMessage("SALE_".$arHeader["id"]);
                        $arHeaders[] = $arHeader["id"];

                        // remember which values should be shown not in the separate columns, but inside other columns
                        if (in_array($arHeader["id"], array("TYPE")))
                        {
                            $bPriceType = true;
                            continue;
                        }
                        elseif ($arHeader["id"] == "PROPS")
                        {
                            $bPropsColumn = true;
                            continue;
                        }
                        elseif ($arHeader["id"] == "DELAY")
                        {
                            $bDelayColumn = true;
                            continue;
                        }
                        elseif ($arHeader["id"] == "DELETE")
                        {
                            $bDeleteColumn = true;
                            continue;
                        }
                        elseif ($arHeader["id"] == "WEIGHT")
                        {
                            $bWeightColumn = true;
                        }?>

                        <?endforeach; ?>
                    <th>����</th>
                    <th>�������</th>
                    <th>������������</th>
					          <th>����������� � ������</th>
                    <th>���-�� (��.)</th>
                    <th>���������</th>
                    <th>�����</th>
                    <th></th>
                </tr>
                </thead>

                <tbody>
                <?
                $vendors = array();
                $ids = array();
                foreach ($arResult["ITEMS"]["AnDelCanBuy"] as $k=>$item){
                    $vendors[] .= $item["PROPERTY_VENDOR_ID_VALUE"];
                    $ids[] .= $arResult["GRID"]["ROWS"][$k]["ID"];

                    $test = CIBlockElement::GetProperty(
                      8, $item["PRODUCT_ID"], array(), array("CODE" => "VARIATIONS")
                    );
                    while ($arP = $test->GetNext())
                    {
                      $pp[$item["PRODUCT_ID"]] = \Bitrix\Main\Web\Json::decode(html_entity_decode($arP["VALUE"]));
                    }

                    $test2 = CIBlockElement::GetProperty(
                      8, $item["PRODUCT_ID"], array(), array("CODE" => "SERIAL")
                    );
                    while ($arP = $test2->GetNext())
                    {
                      $pp2[$item["PRODUCT_ID"]] = \Bitrix\Main\Web\Json::decode(html_entity_decode($arP["VALUE"]));
                    }


                }
                $vendors = array_unique($vendors);
                $vendors = array_values($vendors);
                ?>


                <?foreach($vendors as $d=>$vendor):?>

                <?foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):

					             $error_no_product = false;

                       if($vendor == $arItem["PROPERTY_VENDOR_ID_VALUE"]):

                       if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
//echo "<pre>";
//print_r($pp);

$color = $arItem["PROPS_ALL"]["COLOR"]["VALUE"];
$size = $arItem["PROPS_ALL"]["SIZE"]["VALUE"];
//echo "<b>" . $color . "</b><br>";
$option1 = [];
$option2 = [];
foreach($pp[$arItem["PRODUCT_ID"]] as $index => $it)
{
  //print_r($it);

  foreach ($it as $it2){

    if($it2["VALUE"] == $color){
      $option1[$arItem["PRODUCT_ID"]]["pid"] = $it2["Pid"];
      $option1[$arItem["PRODUCT_ID"]]["vid"] = $it2["Vid"];


    }
  }

  foreach ($it as $it3) {
    if ($it3["VALUE"] == $size) {

      $option2[$arItem["PRODUCT_ID"]]["pid"] = $it3["Pid"];
      $option2[$arItem["PRODUCT_ID"]]["vid"] = $it3["Vid"];
    }
  }


}
//print_r($pp2);
//print_r($option2);

foreach($pp2 as $i => $item)
{
  foreach ($item as $option) {
    //echo $option["OtapiConfiguredItem"]["configurators"][0]["Vid"]."<br>";
    if ($option["OtapiConfiguredItem"]["configurators"][0]["Pid"][0] == $option1[$i]["pid"]) {
      if ($option["OtapiConfiguredItem"]["configurators"][0]["Vid"] == $option1[$i]["vid"]) {
        if ($option["OtapiConfiguredItem"]["configurators"][1]["Pid"][0] == $option2[$i]["pid"]) {
          if ($option["OtapiConfiguredItem"]["configurators"][1]["Vid"] == $option2[$i]["vid"]) {
            $configured_price = $option["OtapiConfiguredItem"]["prices"];
          }
        }
      }
    }
  }
}

if ($configured_price[0]["master_price"]) {
    $mp[$arItem["ID"]]["rub"] = (float)$configured_price[0]["master_price"];
    $mp[$arItem["ID"]]["usd"] = (float)$configured_price[0]["master_price_usd"];
    $master_price = (float)$configured_price[0]["master_price"];
    $master_price_usd = (float)$configured_price[0]["master_price_usd"];
} else {
    $mp[$arItem["ID"]] = $configured_price;
}
//print_r($configured_price);



//echo "</pre>";
                            $res = CIBlockElement::GetByID( $arItem["PRODUCT_ID"] );
                                    if($ar_res = $res->GetNext()) $id_section = $ar_res['IBLOCK_SECTION_ID'];
                                $sql_section = CIBlockSection::GetList(Array(), Array('IBLOCK_ID'=>8, 'ACTIVE'=>'Y', 'ID'=>$id_section), false, Array('UF_ID_CATEGORY'));
                                if($result_section = $sql_section->GetNext())
                                {
                                $externalCategoryID = $result_section["UF_ID_CATEGORY"];
                                }


                            endif;
                    if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):?>

                        <!--<tr class="tb_seller_name"><td colspan="8"><span>��������: </span><a href="/vendor/index.php?manufacturer=<?=$vendor?>" target="_blank"><?=$vendor?></a></td></tr>-->
					<?if ($error_no_product):?>
						<tr id="<?=$arItem["ID"]?>" class="tb_row_content no-product" vendor="<?=$vendor?>">
							<div>����� <b><?=$arItem["PROPERTY_ARTICLE_VALUE"];?></b> � ��������� ����� �����������</div>
					<?else:?>
                        <tr id="<?=$arItem["ID"]?>" class="tb_row_content" vendor="<?=$vendor?>">
					<?endif;?>

                            <?
                            foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):

                                if (in_array($arHeader["id"], array("PROPS", "DELAY", "DELETE", "TYPE"))) // some values are not shown in the columns in this template
                                    continue;

                                if ($arHeader["name"] == '')
                                    $arHeader["name"] = GetMessage("SALE_".$arHeader["id"]);

                                if ($arHeader["id"] == "NAME"):
                                    ?>
                                    <td class="table_block1 table_block">
                                        <div class="tb_wrap_img">
                                            <?
                                            if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
                                                $url = $arItem["PREVIEW_PICTURE_SRC"];
                                            elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
                                                $url = $arItem["DETAIL_PICTURE_SRC"];
                                            elseif (strlen($arItem["PROPERTY_MAIN_PHOTO_VALUE"]) > 0):
                                                    $url = $arItem["PROPERTY_MAIN_PHOTO_VALUE"];
                                            else:
                                                $url = $templateFolder."/images/no_photo.png";
                                            endif;
                                            ?>

                                            <?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
                                                <img src="<?=$url?>" alt="<?=$arItem["NAME"]?>">
                                                <?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
                                        </div>
                                        <?
                                        if (!empty($arItem["BRAND"])):
                                            ?>
                                            <div class="bx_ordercart_brand">
                                                <img alt="" src="<?=$arItem["BRAND"]?>" />
                                            </div>
                                            <?
                                        endif;
                                        ?>
                                    </td>
                                    <td class="table_block2 table_block"><p><?= strlen($arItem["PROPERTY_ARTICLE_VALUE"]) ? $arItem["PROPERTY_ARTICLE_VALUE"] : ""; ?></p></td>
                                    <td class="table_block3 table_block">

                                        <p>
                                            <?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?endif;?>
                                                <?=$arItem["NAME"]?>
                                                <?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0):?></a><?endif;?>
                                        </p>
                                        <div class="bx_ordercart_itemart">
                                            <?
                                            if ($bPropsColumn):
                                                foreach ($arItem["PROPS"] as $val):

                                                    if (is_array($arItem["SKU_DATA"]))
                                                    {
                                                        $bSkip = false;
                                                        foreach ($arItem["SKU_DATA"] as $propId => $arProp)
                                                        {
                                                            if ($arProp["CODE"] == $val["CODE"])
                                                            {
                                                                $bSkip = true;
                                                                break;
                                                            }
                                                        }
                                                        if ($bSkip)
                                                            continue;
                                                    }

                                                    echo $val["NAME"].":&nbsp;<span>".$val["VALUE"]."<span><br/>";
                                                endforeach;
                                            endif;
                                            ?>
                                        </div>
                                        <?
                                        if (is_array($arItem["SKU_DATA"]) && !empty($arItem["SKU_DATA"])):
                                            foreach ($arItem["SKU_DATA"] as $propId => $arProp):

                                                // if property contains images or values
                                                $isImgProperty = false;
                                                if (!empty($arProp["VALUES"]) && is_array($arProp["VALUES"]))
                                                {
                                                    foreach ($arProp["VALUES"] as $id => $arVal)
                                                    {
                                                        if (!empty($arVal["PICT"]) && is_array($arVal["PICT"])
                                                            && !empty($arVal["PICT"]['SRC']))
                                                        {
                                                            $isImgProperty = true;
                                                            break;
                                                        }
                                                    }
                                                }
                                                $countValues = count($arProp["VALUES"]);
                                                $full = ($countValues > 5) ? "full" : "";

                                                if ($isImgProperty): // iblock element relation property
                                                    ?>
                                                    <div class="bx_item_detail_scu_small_noadaptive <?=$full?>">

													<span class="bx_item_section_name_gray">
														<?=$arProp["NAME"]?>:
													</span>

                                                        <div class="bx_scu_scroller_container">

                                                            <div class="bx_scu">
                                                                <ul id="prop_<?=$arProp["CODE"]?>_<?=$arItem["ID"]?>"
                                                                    style="width: 200%; margin-left:0;"
                                                                    class="sku_prop_list"
                                                                >
                                                                    <?
                                                                    foreach ($arProp["VALUES"] as $valueId => $arSkuValue):

                                                                        $selected = "";
                                                                        foreach ($arItem["PROPS"] as $arItemProp):
                                                                            if ($arItemProp["CODE"] == $arItem["SKU_DATA"][$propId]["CODE"])
                                                                            {
                                                                                if ($arItemProp["VALUE"] == $arSkuValue["NAME"] || $arItemProp["VALUE"] == $arSkuValue["XML_ID"])
                                                                                    $selected = " bx_active";
                                                                            }
                                                                        endforeach;
                                                                        ?>
                                                                        <li style="width:10%;"
                                                                            class="sku_prop<?=$selected?>"
                                                                            data-value-id="<?=$arSkuValue["XML_ID"]?>"
                                                                            data-element="<?=$arItem["ID"]?>"
                                                                            data-property="<?=$arProp["CODE"]?>"
                                                                        >
                                                                            <a href="javascript:void(0)" class="cnt"><span class="cnt_item" style="background-image:url(<?=$arSkuValue["PICT"]["SRC"];?>)"></span></a>
                                                                        </li>
                                                                        <?
                                                                    endforeach;
                                                                    ?>
                                                                </ul>
                                                            </div>

                                                            <div class="bx_slide_left" onclick="leftScroll('<?=$arProp["CODE"]?>', <?=$arItem["ID"]?>, <?=$countValues?>);"></div>
                                                            <div class="bx_slide_right" onclick="rightScroll('<?=$arProp["CODE"]?>', <?=$arItem["ID"]?>, <?=$countValues?>);"></div>
                                                        </div>

                                                    </div>
                                                    <?
                                                else:
                                                    ?>
                                                    <div class="bx_item_detail_size_small_noadaptive <?=$full?>">

													<span class="bx_item_section_name_gray">
														<?=$arProp["NAME"]?>:
													</span>

                                                        <div class="bx_size_scroller_container">
                                                            <div class="bx_size">
                                                                <ul id="prop_<?=$arProp["CODE"]?>_<?=$arItem["ID"]?>"
                                                                    style="width: 200%; margin-left:0;"
                                                                    class="sku_prop_list"
                                                                >
                                                                    <?
                                                                    foreach ($arProp["VALUES"] as $valueId => $arSkuValue):

                                                                        $selected = "";
                                                                        foreach ($arItem["PROPS"] as $arItemProp):
                                                                            if ($arItemProp["CODE"] == $arItem["SKU_DATA"][$propId]["CODE"])
                                                                            {
                                                                                if ($arItemProp["VALUE"] == $arSkuValue["NAME"])
                                                                                    $selected = " bx_active";
                                                                            }
                                                                        endforeach;
                                                                        ?>
                                                                        <li style="width:10%;"
                                                                            class="sku_prop<?=$selected?>"
                                                                            data-value-id="<?=($arProp['TYPE'] == 'S' && $arProp['USER_TYPE'] == 'directory' ? $arSkuValue['XML_ID'] : $arSkuValue['NAME']); ?>"
                                                                            data-element="<?=$arItem["ID"]?>"
                                                                            data-property="<?=$arProp["CODE"]?>"
                                                                        >
                                                                            <a href="javascript:void(0)" class="cnt"><?=$arSkuValue["NAME"]?></a>
                                                                        </li>
                                                                        <?
                                                                    endforeach;
                                                                    ?>
                                                                </ul>
                                                            </div>
                                                            <div class="bx_slide_left" onclick="leftScroll('<?=$arProp["CODE"]?>', <?=$arItem["ID"]?>, <?=$countValues?>);"></div>
                                                            <div class="bx_slide_right" onclick="rightScroll('<?=$arProp["CODE"]?>', <?=$arItem["ID"]?>, <?=$countValues?>);"></div>
                                                        </div>

                                                    </div>
                                                    <?
                                                endif;
                                            endforeach;
                                        endif;
                                        ?>
                                    </td>
                                    <td class="table_block4 table_block">
										<p>
                                            <? if(isset($arItem['PROPS_ALL']['COMMENT'])){
                                                echo $arItem['PROPS_ALL']['COMMENT']['VALUE']."<br>";
                                            }

                                            if($arItem['PROPS_ALL']['COLOR']['VALUE'] != ""){
                                                echo "����: ".$arItem['PROPS_ALL']['COLOR']['VALUE']."<br>";
                                            }

                                            if($arItem['PROPS_ALL']['SIZE']['VALUE'] != ""){
                                                echo "������: ".$arItem['PROPS_ALL']['SIZE']['VALUE']."<br>";
                                            }


                                            /*$arFilter1 = array("IBLOCK_ID"=>11, 'ACTIVE'=>"Y", "PROPERTY_PRODUCT_ID"=>$arItem["ID"]);
                    $rsItems1 = CIBlockElement::GetList(array("ID"=>"DESC"), $arFilter1, false, false, $arSelect1);
                    while($ob1 = $rsItems1->GetNextElement())
                    {
                        $arFields1 = $ob1->GetFields();
                        $arProps1 = $ob1 ->GetProperties();
                        ?>���:<?=$arFields1["NAME"]?><br/>
                        ����:<?=$arFields1["DATE_CREATE"]?><br/>
                        �����������:<?=$arFields1["PREVIEW_TEXT"]?><br/>
                        <?
                    }*/?>
										</p>
									</td>
									<td class="table_block5 table_block">
                                        <p class="basket_quantity_control">
                                            <?
                                            $ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
                                            $max = isset($arItem["AVAILABLE_QUANTITY"]) ? "max=\"".$arItem["AVAILABLE_QUANTITY"]."\"" : "";
                                            $useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
                                            $useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");


                                            if (!isset($arItem["MEASURE_RATIO"]))
                                            {
                                                $arItem["MEASURE_RATIO"] = 1;
                                            }

                                            if (
                                                floatval($arItem["MEASURE_RATIO"]) != 0
                                            ):
                                            ?>

											<a href="javascript:void(0);" class="tb5_plus" onclick=" setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'up', <?=$useFloatQuantityJS?>, <?=$arItem["PROPS"][4]["VALUE"]?>); recalculate();">+</a>
                                            <input
                                                    type="text"
                                                    size="3"
                                                    id="QUANTITY_INPUT_<?=$arItem["ID"]?>"
                                                    class="tb5_num input__basket_form--block"
                                                    name="QUANTITY_INPUT_<?=$arItem["ID"]?>"
                                                    data-min="<?=$arItem["PROPS"][4]["VALUE"];//=$arItem["PROPS"][4]["VALUE"]?>"
                                                    size="2"
                                                    maxlength="18"
                                                    min="0"
                                                    <?=$max?>
                                                    step="<?=$ratio?>"
                                                    value="<?=$arItem["QUANTITY"]?>"
                                                    onchange="updateQuantity('QUANTITY_INPUT_<?=$arItem["ID"]?>', '<?=$arItem["ID"]?>', <?=$ratio?>, <?=$useFloatQuantityJS?>, <?=$arItem["PROPS"][4]["VALUE"]?>); recalculate();"/>
											<a href="javascript:void(0);" class="tb5_minus" onclick=" setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'down', <?=$useFloatQuantityJS?>, <?=$arItem["PROPS"][4]["VALUE"]?>); recalculate();">-</a>



                                            <?
                                            endif;
                                            ?>

                                            <input type="hidden" id="QUANTITY_<?=$arItem['ID']?>" name="QUANTITY_<?=$arItem['ID']?>" value="<?=$arItem["QUANTITY"]?>" />
                                        </p>
                                    </td>
<!--                                    PRICE-->

                                    <td class="table_block6 table_block">

                                        <div class="r-prices">
                                          <?//print_r($mp)?>
                                          <?if(!$mp[$arItem["ID"]]["rub"]):?>
                                          <div class="range-table">

                                            <?foreach ($mp[$arItem["ID"]] as $range):?>
                                              <div class="ranges">
                                                <div class="quant">
                                                  <?=$range['quant'];?>
                                                </div>
                                                <div class="price_ru">
                                                  <?=$range['price'];?>
                                                </div>
                                                <div class="price_usd">
                                                  <?=$range['price_usd'];?>
                                                </div>
                                              </div>
                                            <?endforeach;?>
                                            </div>
                                          <?else:?>
                                            <div class="master_price">

                                                <div class="master_rub">
                                                  <?//=$mp[$arItem["ID"]]["rub"]?>
                                                </div>
                                                <div class="master1_usd">
                                                  $<span class="master_usd"><?=$mp[$arItem["ID"]]["usd"]?></span>
                                                </div>

                                            </div>

                                          <?endif;?>
                                        </div>
                                        <div class="final_price_usd">

                                        </div>


                                        <p id="current_price_<?=$arItem['ID']?>">
                                            <?=$arItem["PRICE"]?>�.
                                        </p>


                                    </td>
                                    <td class="table_block7 table_block">
                                      <div class="allSum_usd">

                                      </div>
                                      <p id="sum_<?=$arItem["ID"]?>"><?=$arItem["SUM"]?></p>
                                    </td>

                                    <?
                                endif;
                            endforeach;

                            if ($bDelayColumn || $bDeleteColumn):
                                ?>
                                <td class="table_block8 table_block">
                                    <?
                                    if ($bDeleteColumn):
                                        ?>
                                        <p class="tb_del">
                                            <a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>">&times;</a><br />                                        </p>
                                        <?
                                    endif;
                                    if ($bDelayColumn):
                                        ?>
                                        <!-- <a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delay"])?>"><?=GetMessage("SALE_DELAY")?></a> -->
                                        <?
                                    endif;
                                    ?>
                                </td>
                                <?
                            endif;
                            ?>
                        </tr>
                        <?
                    endif;
                endif;
                endforeach;?>

            <?endforeach;?>

                </tbody>
            </table>
        </div>
        <input type="hidden" id="column_headers" value="<?=CUtil::JSEscape(implode($arHeaders, ","))?>" />
        <input type="hidden" id="offers_props" value="<?=CUtil::JSEscape(implode($arParams["OFFERS_PROPS"], ","))?>" />
        <input type="hidden" id="action_var" value="<?=CUtil::JSEscape($arParams["ACTION_VARIABLE"])?>" />
        <input type="hidden" id="quantity_float" value="<?=$arParams["QUANTITY_FLOAT"]?>" />
        <input type="hidden" id="count_discount_4_all_quantity" value="<?=($arParams["COUNT_DISCOUNT_4_ALL_QUANTITY"] == "Y") ? "Y" : "N"?>" />
        <input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
        <input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
        <input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />
        <input type="hidden" id="auto_calculation" value="<?=($arParams["AUTO_CALCULATION"] == "N") ? "N" : "Y"?>" />
            <table class="table_cart table_cart--sum">
            <?if ($bWeightColumn && floatval($arResult['allWeight']) > 0):?>
                <tr>
                    <td class="custom_t1"><?=GetMessage("SALE_TOTAL_WEIGHT")?></td>
                    <td class="custom_t2" id="allWeight_FORMATED"><?=$arResult["allWeight_FORMATED"]?>
                    </td>
                </tr>
            <?endif;?>
            <?if ($arParams["PRICE_VAT_SHOW_VALUE"] == "Y"):?>
                <tr>
                    <td><?echo GetMessage('SALE_VAT_EXCLUDED')?></td>
                    <td id="allSum_wVAT_FORMATED"><?=$arResult["allSum_wVAT_FORMATED"]?></td>
                </tr>
                <?if (floatval($arResult["DISCOUNT_PRICE_ALL"]) > 0):?>
                    <tr>
                        <td class="custom_t1"></td>
                        <td class="custom_t2" style="text-decoration:line-through; color:#828282;" id="PRICE_WITHOUT_DISCOUNT">
                            <?=$arResult["PRICE_WITHOUT_DISCOUNT"]?>
                        </td>
                    </tr>
                <?endif;?>
                <?
                if (floatval($arResult['allVATSum']) > 0):
                    ?>
                    <tr>
                        <td><?echo GetMessage('SALE_VAT')?></td>
                        <td id="allVATSum_FORMATED"><?=$arResult["allVATSum_FORMATED"]?></td>
                    </tr>
                    <?
                endif;
                ?>
            <?endif;?>
            <tr class="tb_result">
                <td colspan="8">
                    <p><span><?=GetMessage("SALE_TOTAL")?></span></p>
                    <div id="allSum_FORMATED_usd"></div>
                    <div class="tb_result__sum" id="allSum_FORMATED">

                      (<?=str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"])?>)
                    </div>
                </td>
            </tr>
            </table>
       <div class="bx_ordercart_order_pay">

           <div class="bx_ordercart_order_pay_left" id="coupons_block">
              <?
                if ($arParams["HIDE_COUPON"] != "Y")
                {
                    ?>
                   <div class="bx_ordercart_coupon">
                   <span><?=GetMessage("STB_COUPON_PROMT")?></span><input type="text" id="coupon" name="COUPON" value="" onchange="enterCoupon();">&nbsp;<a class="bx_bt_button bx_big" href="javascript:void(0)" onclick="enterCoupon();" title="<?=GetMessage('SALE_COUPON_APPLY_TITLE'); ?>"><?=GetMessage('SALE_COUPON_APPLY'); ?></a>
                    </div><?
                    if (!empty($arResult['COUPON_LIST']))
                    {
                        foreach ($arResult['COUPON_LIST'] as $oneCoupon)
                        {
                            $couponClass = 'disabled';
                            switch ($oneCoupon['STATUS'])
                            {
                                case DiscountCouponsManager::STATUS_NOT_FOUND:
                                case DiscountCouponsManager::STATUS_FREEZE:
                                    $couponClass = 'bad';
                                    break;
                                case DiscountCouponsManager::STATUS_APPLYED:
                                    $couponClass = 'good';
                                    break;
                            }
                            ?><div class="bx_ordercart_coupon"><input disabled readonly type="text" name="OLD_COUPON[]" value="<?=htmlspecialcharsbx($oneCoupon['COUPON']);?>" class="<? echo $couponClass; ?>"><span class="<? echo $couponClass; ?>" data-coupon="<? echo htmlspecialcharsbx($oneCoupon['COUPON']); ?>"></span><div class="bx_ordercart_coupon_notes"><?
                                if (isset($oneCoupon['CHECK_CODE_TEXT']))
                                {
                                    echo (is_array($oneCoupon['CHECK_CODE_TEXT']) ? implode('<br>', $oneCoupon['CHECK_CODE_TEXT']) : $oneCoupon['CHECK_CODE_TEXT']);
                                }
                                ?></div></div><?
                        }
                        unset($couponClass, $oneCoupon);
                    }
                }
                else
               {
                    ?>&nbsp;<?
               }
               ?>
           </div>
           <div style="clear:both;"></div>
           <div class="bx_ordercart_order_pay_center">

                <?if ($arParams["USE_PREPAYMENT"] == "Y" && strlen($arResult["PREPAY_BUTTON"]) > 0):?>
                   <?=$arResult["PREPAY_BUTTON"]?>
                   <span><?=GetMessage("SALE_OR")?></span>
              <?endif;?>
              <?
               if ($arParams["AUTO_CALCULATION"] != "Y")
               {
                   ?>
                   <a href="javascript:void(0)" onclick="updateBasket();" class="checkout refresh"><?GetMessage("SALE_REFRESH")?></a>
                  <?
               }
               ?>
               <a href="javascript:void(0)" onclick="checkOut();" class="checkout"><?GetMessage("SALE_ORDER")?></a>
           </div>
        </div>
    </div>
    <?
else:
    ?>
    <div id="basket_items_list">
        <table>
            <tbody>
            <tr>
                <td style="text-align:center">
                    <div class=""><?=GetMessage("SALE_NO_ITEMS");?></div>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <?
endif;
?>

<script>
    BX.ready(function(){
        vendors();
    });
</script>
