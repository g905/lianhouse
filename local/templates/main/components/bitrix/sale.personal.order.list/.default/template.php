 <?
 global $USER;
$id_user = $USER->GetID(); // ID ������������
$arFilter = Array("USER_ID" => $id_user);
require($_SERVER["DOCUMENT_ROOT"].'/parser/functions.php');
require($_SERVER["DOCUMENT_ROOT"].'/parser/config.php');
$id_iblock   = 8;
$sql = CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $arFilter);
while ($result = $sql->Fetch()){
	if($result["CANCELED"]=="Y"){$userordescancel++;}elseif($result["STATUS_ID"]=="F"){$userordesfinish++;}elseif($result["STATUS_ID"]=="Y"){$userordesshipped++;}else{$userordesprocess++;}
	$userordestotal++;
}
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

//��������, ����������� �� ������������ ������ �����������
//$moderators = new Array();
$property_enums = CIBlockPropertyEnum::GetList(Array("SORT"=>"ASC"), Array("IBLOCK_ID"=>5, "PROPERTY_ID" => "23"));
while ($element = $property_enums->GetNext()) {
	$moderators[] = (int)($element["VALUE"]);
}
if ( CSite::InGroup( $moderators ) ){ $isAdmin = true;}
//���������� ��������� � faq
if (!$isAdmin){
	$faqcount = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 5, "NAME"=>$USER->GetFullName()), array(), false, array('ID', 'NAME'));
}
else {
	$faqcount = CIBlockElement::GetList(array(), array('IBLOCK_ID' => 5), array(), false, array('ID', 'NAME'));
}

use Bitrix\Main,
Bitrix\Main\Localization\Loc,
Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/style.css");
$this->addExternalCss("/bitrix/css/main/bootstrap.css");
CJSCore::Init(array('clipboard', 'fx'));

Loc::loadMessages(__FILE__);

if (!empty($arResult['ERRORS']['FATAL']))
{
	foreach($arResult['ERRORS']['FATAL'] as $error)
	{
		ShowError($error);
	}
	$component = $this->__component;
	if ($arParams['AUTH_FORM_IN_TEMPLATE'] && isset($arResult['ERRORS']['FATAL'][$component::E_NOT_AUTHORIZED]))
	{
		$APPLICATION->AuthForm('', false, false, 'N', false);
	}

}
else
{
	if (!empty($arResult['ERRORS']['NONFATAL']))
	{
		foreach($arResult['ERRORS']['NONFATAL'] as $error)
		{
			ShowError($error);
		}
	}
	if (!count($arResult['ORDERS']))
	{
		if ($_REQUEST["filter_history"] == 'Y')
		{
			if ($_REQUEST["show_canceled"] == 'Y')
			{
				?>
				<h3><?= Loc::getMessage('SPOL_TPL_EMPTY_CANCELED_ORDER')?></h3>
				<?
			}
			else
			{
				?>
				<h3><?= Loc::getMessage('SPOL_TPL_EMPTY_HISTORY_ORDER_LIST')?></h3>
				<?
			}
		}
		else
		{
			?>
			<h3><?= Loc::getMessage('SPOL_TPL_EMPTY_ORDER_LIST')?></h3>
			<?
		}
	}

	?>
	<div class="pa_line_block">
		<a href="/personal/order/" class="pal_block">
			<div class="pal_inner_block pib1">
				<p class="pal_name"><?= Loc::getMessage('SPOL_CHECK_PIB1_NAME')?></p>
				<p class="pal_text"><?= Loc::getMessage('SPOL_CHECK_PIB1_TEXT')?></p>
				<span class="pal_namber"><?=$userordesshipped + $userordesprocess + $userordescancel ?></span>
			</div>
		</a>
		<a href="/personal/order/index.php?filter_history=Y" class="pal_block">
			<div class="pal_inner_block pib2">
				<p class="pal_name"><?= Loc::getMessage('SPOL_CHECK_PIB2_NAME')?></p>
				<p class="pal_text"><?= Loc::getMessage('SPOL_CHECK_PIB2_TEXT')?></p>
				<!--span class="pal_namber">15925���</span-->
			</div>
		</a>
		<?
		?>
		<a href="/personal/faq/" class="pal_block">
			<div class="pal_inner_block pib3">
				<p class="pal_name"><?= Loc::getMessage('SPOL_CHECK_PIB3_NAME')?></p>
				<p class="pal_text"><?= Loc::getMessage('SPOL_CHECK_PIB3_TEXT')?></p>
				<span class="pal_namber"><?=$faqcount?></span>
			</div>
		</a>
		<a href="/personal/questions/" class="pal_block">
			<div class="pal_inner_block pib4">
				<p class="pal_name"><?= Loc::getMessage('SPOL_CHECK_PIB4_NAME')?></p>
				<p class="pal_text"><?= Loc::getMessage('SPOL_CHECK_PIB4_TEXT')?></p>
			</div>
		</a>
	</div>
	<!--================================================================-->
	<section class="delivery_s1 pers_area">
		<div class="del_s1_left">
			<div class="pa_navigation">
				<a href="/personal/profile/"><?= Loc::getMessage('PA_NAVIGATION_PROFILE')?></a>
				<a href="/personal/passwd/"><?= Loc::getMessage('PA_NAVIGATION_PASSWORD')?></a>
				<a href="/personal/address/"><?= Loc::getMessage('PA_NAVIGATION_ADDRESS')?></a>
				<a href="<?=$APPLICATION->GetCurPageParam('logout=yes', Array('login'))?>"><?= Loc::getMessage('PA_NAVIGATION_LOGOUT')?></a>
			</div>
			<?/* ������ �����������, ���������� ��� �������������
			$APPLICATION->IncludeComponent(
				"bitrix:system.auth.form",
				"login_order",
				array(
					"PROFILE_URL" => "/personal/profile/",
					"SHOW_ERRORS" => "N",
					"COMPONENT_TEMPLATE" => "login_order",
					"REGISTER_URL" => "",
					"FORGOT_PASSWORD_URL" => ""
				),
				false
			);*/?>

		</div><!--del_s1_left-->
		<div class="margin_right_sidebar">
			<div class="pa_my_orders">
				<div class="pamq_top">
					<?$clearFromLink = array("filter_history","filter_status","show_all", "show_canceled", "delivery");
					$nothing = !isset($_REQUEST["filter_history"]) && !isset($_REQUEST["show_all"]);
					$delivery = isset($_REQUEST["delivery"]);
					?>
					<a class="pamq_a1" href="/personal/order/"><?= Loc::getMessage('SPOL_CHECK_PAMQ_A1')?> <span>(<?=$userordesprocess++?>)</span></a>
					<a class="pamq_a2" href="<?=$APPLICATION->GetCurPageParam("delivery=Y", $clearFromLink, false)?>"><?= Loc::getMessage('SPOL_CHECK_PAMQ_A2')?> <span>(<?=$userordesshipped?>)</span></a>
					<a class="pamq_a3" href="<?=$APPLICATION->GetCurPageParam("filter_history=Y", $clearFromLink, true)?>"><?= Loc::getMessage('SPOL_CHECK_PAMQ_A3')?> <span>(<?=$userordesfinish++?>)</span></a>
					<a class="pamq_a4" href="<?=$APPLICATION->GetCurPageParam("filter_history=Y&show_canceled=Y", $clearFromLink, false)?>"><?= Loc::getMessage('SPOL_CHECK_PAMQ_A4')?> <span>(<?=$userordescancel?>)</span></a>
					<a class="pamq_a5 pamq_active" href="<?=$APPLICATION->GetCurPageParam("filter_history=Y&show_canceled=Y&show_all=Y", $clearFromLink, false)?>"><?= Loc::getMessage('SPOL_CHECK_PAMQ_A5')?> <span>(<?=$userordestotal?>)</span></a>
				</div>
				<div class="chat_order_wrap">

					<!--////////////////////////// chat_block ////////////////////////////////////////////////////-->
					<?
					foreach ($arResult['ORDERS'] as $key => $order)
					{
						$it = [];

						$basket = \Bitrix\Sale\Order::load($order['ORDER']["ID"])->getBasket();

						foreach ($basket as $basketItem) {
							$a=$basketItem->getPropertyCollection();
							$bid = $basketItem->GetField('ID');

							foreach($a as $b){
								//echo $b->getField("NAME").": ".$b->getField("VALUE")."<br>";
								if(($b->getField("NAME") === "Product XML_ID")||($b->getField("NAME") === "�����������")){
									continue;
								} else {
									$it[$bid][$b->getField("NAME")] = $b->getField("VALUE");
									//$it[$bid]["ID"] = $bid;
								}

							}
						}

						//echo "<pre>"; print_r($it); echo "</pre>";

						$userID = $order['ORDER']["RESPONSIBLE_ID"];
						$rsUser = CUser::GetByID($userID);
						$arUser = $rsUser->Fetch();
						$userResponsiible = $arUser["NAME"]." ".$arUser["LAST_NAME"];
						$orderHeaderStatus = $order['ORDER']['STATUS_ID'];
						$additional_information = '';
						$orderID = $order['ORDER']["ID"];
						$arOrder = CSaleOrder::GetByID($orderID);

							// dd($order["ORDER"]["STATUS_ID"]);
						$order_props = CSaleOrderPropsValue::GetOrderProps($orderID);
						/*����������� ��������� ������*/

						while ($arProps = $order_props->Fetch()){

							if ($arProps['ORDER_PROPS_ID']==7){
								$delivery_address = $arProps['VALUE'];
							}

			              if($arProps['ORDER_PROPS_ID']==9){
			                $order_weight = $arProps['VALUE'];
			              }


						}

						?>
						<div class="chat_block"<?

						if ((!strpos($APPLICATION->GetCurUri(),'show_all=Y')) and ($delivery) and ($orderHeaderStatus == "N")) echo("style = display:none;");
						elseif ((!strpos($APPLICATION->GetCurUri(),'show_all=Y')) and (!$delivery) and ($orderHeaderStatus == "Y")) echo("style = display:none;");
						?> ><?//echo($orderHeaderStatus)?>
						<div class="pamot_name_block" style="background: url(/local/templates/main/img/icons/<?echo($orderHeaderStatus)?>.png) no-repeat;">
							<p class="pamot_name"><?= Loc::getMessage('SPOL_CHECK_PAMOT_NAME')?> <span><?=$order['ORDER']['ACCOUNT_NUMBER']?></span></p>
							<span class="pamot_expectation">
								<?=$arResult['INFO']['STATUS'][$orderHeaderStatus]['NAME']?></span>
								<div>
									<span class="pamot_themename"> <?= Loc::getMessage('SPOL_CHECK_PAMOT_THEMENAME')?>: <? if ($userResponsiible<>' '){echo $userResponsiible;} else {echo Loc::getMessage('USERRESPONSIIBLE_NONE');}
									?></span>
									<span class="pamot_smallname"><?=Loc::getMessage('SPOL_TPL_DELIVERY_SERVICE')?>:
										<?=$arResult['INFO']['DELIVERY'][$order['SHIPMENT'][0]["DELIVERY_ID"]]['NAME']." - ".$delivery_address?></span>

									</div>
									<div class="clear"></div>
								</div>
								<div class="cb_hide_block">

									<div class="order_line"></div>
									<div class="pamo_bottom">

										<div class="pamo_cost">
											<p class="pamo_cost_p pamo_cost_p1"><?= Loc::getMessage('PAMO_COST_P PAMO_COST_P1')?>: <span><?=$order['ORDER']['FORMATED_PRICE']?></span></p>
											<p class="pamo_cost_p pamo_cost_p2"><?= Loc::getMessage('PAMO_COST_P PAMO_COST_P2')?>: <span><?=count($order['BASKET_ITEMS']);?></span></p>
											<p class="pamo_cost_p pamo_cost_p3"><?= Loc::getMessage('PAMO_COST_P PAMO_COST_P3')?>:<span><?=$order['SHIPMENT'][0]["FORMATED_DELIVERY_PRICE"];?></span></p>
                    <p class="pamo_cost_p pamo_cost_p4"><?= Loc::getMessage('PAMO_COST_P PAMO_COST_P4')?>:
                    <?if($order_weight != ''):?>
                      <span><?=$order_weight;?>��.</span>
                    <?else:?>
                      <span>��������������</span>
                    <?endif;?>
                    </p>

										<!--p class="pamo_cost_p pamo_cost_p4"><?= Loc::getMessage('PAMO_COST_P PAMO_COST_P4')?>: <span>2,2��</span></p-->
									</div>
								</div>

								<!--=======-->
								<div class="wrap_table_order">
									<table class="table_order">
										<thead>
											<tr>
												<th><?= Loc::getMessage('WRAP_TABLE_ORDER_P1')?></th>
												<th><?= Loc::getMessage('WRAP_TABLE_ORDER_P2')?></th>
												<th><?= Loc::getMessage('WRAP_TABLE_ORDER_P3')?></th>
												<th><?= Loc::getMessage('WRAP_TABLE_ORDER_P4')?> ($)</th>
												<th><?= Loc::getMessage('WRAP_TABLE_ORDER_P5')?> (��.)</th>
												<!--th><?= Loc::getMessage('WRAP_TABLE_ORDER_P6')?> ($)</th-->
												<th><?= Loc::getMessage('WRAP_TABLE_ORDER_P7')?> ($)</th>
											</tr>
										</thead>
										<tbody>
											<!---->

											<?

											foreach ($order["BASKET_ITEMS"] as $key => $order_item)
											{
//������� ���� ���� ����� �� ��������, �� �������, �� �������

												$arFilter = Array("IBLOCK_ID"=>8, "ID"=>$order_item["PRODUCT_ID"]);
												$res = CIBlockElement::GetList(Array(), $arFilter);
												if ($ob = $res->GetNextElement()){;
    $arFields = $ob->GetFields(); // ���� ��������
    $picture = (CFile::GetPath($arFields["PREVIEW_PICTURE"]));
    $arProps = $ob->GetProperties(); // �������� ��������
    $article = $arProps["ARTICLE"]["VALUE"];



	//echo "<pre>"; print_r($arProps); echo "</pre>";
}
	//var_dump($arProps);
?>
<tr class="tb_row_order">
	<td class="table_order_block1 table_order_block">
		<div class="tb_order_wrap_img">
			<img src="<?=$arProps["MAIN_PHOTO"]["VALUE"]?>" alt="">
		</div>
	</td>
	<td class="table_order_block2 table_order_block"><p><?=$article?></p></td>
	<td class="table_order_block3 table_order_block"><p><?=$order_item["NAME"]?></p>
		<div style="border-top: 1px solid #ddd; background: #eee; color: #999">
		<?foreach($it[$order_item["ID"]] as $k=>$p):?>
			<?if($p!=""):?>
				<p><span style="font-weight: bold"><?=$k?></span>: <span><?=$p?></span></p>
			<?endif;?>
		<?endforeach;?>
		</div>
	</td>
	<td class="table_order_block4 table_order_block"><p>
															<?//������� ���� ���� ����� �� ��������, �� �������, �� �������
															if (($order['ORDER']['CANCELED'] == "N") and ($order["ORDER"]["STATUS_ID"] == "N")){
															// var_dump($order['ORDER']['CANCELED']);
																//���������� ���� ����� �������
																$res = CIBlockElement::GetByID($order_item["PRODUCT_ID"]);
																if($ar_res = $res->GetNext()) $id_section = $ar_res['IBLOCK_SECTION_ID'];
																$sql_section = CIBlockSection::GetList(Array(), Array('IBLOCK_ID'=>8, 'ACTIVE'=>'Y', 'ID'=>$id_section), false, Array('UF_ID_CATEGORY'));
																if($result_section = $sql_section->GetNext())
																{
																	$externalCategoryID = $result_section["UF_ID_CATEGORY"];
																}
																// var_dump($externalCategoryID);
																$extproductPrice = 'http://otapi.net/OtapiWebService2.asmx/GetItemPrice?instanceKey=' . CFG_SERVICE_INSTANCEKEY
																. '&language=' . CFG_REQUEST_LANGUAGE
																. '&categoryId=' . $externalCategoryID
																. '&quantity=' . 1
																. '&itemId=' . $arProps["ARTICLE"]["VALUE"]
																. '&promotionId=&configurationId=';
																// $extproductItemsPrice = pars($extproductPrice)->Result->ConvertedPriceWithoutSign;
																$extproductItemsPrice = (float)$extproductItemsPrice;
																//var_dump($extproductItemsPrice);
																if(((float)$order_item["PRICE"] != $extproductItemsPrice))
																{
																		//�������� ����
																	$arPrices = Array(
																		"PRODUCT_ID" => $order_item["PRODUCT_ID"],
																		"CATALOG_GROUP_ID" => 3,
																		"PRICE" => $extproductItemsPrice,
																		"CURRENCY" => "RUB",
																								// "QUANTITY_FROM" => 1,
																								// "QUANTITY_TO" => ''
																	);

																	$resprice = CPrice::GetList(
																		array(),
																		array(
																			"PRODUCT_ID" => $order_item["PRODUCT_ID"],
																			"CATALOG_GROUP_ID" => 3
																		)
																	);

																	if ($arr = $resprice->Fetch())
																	{
																						// var_dump($arr);
																		CPrice::Update($arr["ID"], $arPrices);
																	}
																	else
																	{
																		CPrice::Add($arPrices);
																	}

																}
															}?>
															<?=CurrencyFormat($order_item["PRICE"], $order_item["CURRENCY"])?>
														</p></td>
														<td class="table_order_block5 table_order_block"><p><?=$order_item["QUANTITY"]?></p></td>
														<!--td class="table_order_block6 table_order_block"><p>???</p></td-->
														<td class="table_order_block7 table_order_block"><p><?
														$item_summ = $order_item["QUANTITY"] * $order_item["PRICE"];
														echo CurrencyFormat($item_summ, $order_item["CURRENCY"]);
														?></p></td>
													</tr>
													<?}?>
													<!---->


													<tr class="tb_order_result">
														<td colspan="7">
															<div>
																<p><span><?= Loc::getMessage('WRAP_TABLE_ORDER_RESULT')?>: </span>
																	<? echo(CurrencyFormat((($order['SHIPMENT'][0] ["PRICE_DELIVERY"])+($order['ORDER']["PRICE"])), $order_item["CURRENCY"]));?>

																</p>

															</div>
														</td>
													</tr>
												</tbody>
											</table>
										</div>
										<!--=======-->

											<? /*

										*/?>

									</div>
									<div class="cb_hide_show">
										<span class="cb_line"></span>
										<?echo Loc::getMessage("SPOL_CHECK_PAMOT_HIDE_SHOW")?><i class="fa fa-angle-down" aria-hidden="true"></i>
									</div>
								</div>
								<?}?>
								<!--chat_block-->
								<!--////////////////////////// /chat_block ////////////////////////////////////////////////////-->
							</div>



				<? /*pink80 ?>
				<div class="row col-md-12 col-sm-12">
					<?
					$nothing = !isset($_REQUEST["filter_history"]) && !isset($_REQUEST["show_all"]);
					$clearFromLink = array("filter_history","filter_status","show_all", "show_canceled");

					if ($nothing || $_REQUEST["filter_history"] == 'N')
					{
						?>
						<a class="sale-order-history-link" href="<?=$APPLICATION->GetCurPageParam("filter_history=Y", $clearFromLink, false)?>">
							<?echo Loc::getMessage("SPOL_TPL_VIEW_ORDERS_HISTORY")?>
						</a>
						<?
					}
					if ($_REQUEST["filter_history"] == 'Y')
					{
						?>
						<a class="sale-order-history-link" href="<?=$APPLICATION->GetCurPageParam("", $clearFromLink, false)?>">
							<?echo Loc::getMessage("SPOL_TPL_CUR_ORDERS")?>
						</a>
						<?
						if ($_REQUEST["show_canceled"] == 'Y')
						{
							?>
							<a class="sale-order-history-link" href="<?=$APPLICATION->GetCurPageParam("filter_history=Y", $clearFromLink, false)?>">
								<?echo Loc::getMessage("SPOL_TPL_VIEW_ORDERS_HISTORY")?>
							</a>
							<?
						}
						else
						{
							?>
							<a class="sale-order-history-link" href="<?=$APPLICATION->GetCurPageParam("filter_history=Y&show_canceled=Y", $clearFromLink, false)?>">
								<?echo Loc::getMessage("SPOL_TPL_VIEW_ORDERS_CANCELED")?>
							</a>
							<?
						}
					}
					?>
				</div>

				<?
				if (!count($arResult['ORDERS']))
				{
					?>

					<div class="row col-md-12 col-sm-12">
						<a href="<?=htmlspecialcharsbx($arParams['PATH_TO_CATALOG'])?>" class="sale-order-history-link">
							<?=Loc::getMessage('SPOL_TPL_LINK_TO_CATALOG')?>
						</a>
					</div>

					<?
				}

				if ($_REQUEST["filter_history"] !== 'Y')
				{
					$paymentChangeData = array();
					$orderHeaderStatus = null;

					foreach ($arResult['ORDERS'] as $key => $order)
					{
						if ($orderHeaderStatus !== $order['ORDER']['STATUS_ID'] && $arResult['SORT_TYPE'] == 'STATUS')
						{
							$orderHeaderStatus = $order['ORDER']['STATUS_ID'];

							?>

							<h1 class="sale-order-title">
								<?= Loc::getMessage('SPOL_TPL_ORDER_IN_STATUSES') ?> &laquo;<?=htmlspecialcharsbx($arResult['INFO']['STATUS'][$orderHeaderStatus]['NAME'])?>&raquo;
							</h1>

							<?
						}
						?>

						<div class="col-md-12 col-sm-12 sale-order-list-container">
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12 sale-order-list-title-container">
									<h2 class="sale-order-list-title">
										<?=Loc::getMessage('SPOL_TPL_ORDER')?>
										<?=Loc::getMessage('SPOL_TPL_NUMBER_SIGN').$order['ORDER']['ACCOUNT_NUMBER']?>
										<?=Loc::getMessage('SPOL_TPL_FROM_DATE')?>
										<?=$order['ORDER']['DATE_INSERT']->format($arParams['ACTIVE_DATE_FORMAT'])?>,
										<?=count($order['BASKET_ITEMS']);?>
										<?
										$count = count($order['BASKET_ITEMS']) % 10;
										if ($count == '1')
										{
											echo Loc::getMessage('SPOL_TPL_GOOD');
										}
										elseif ($count >= '2' && $count <= '4')
										{
											echo Loc::getMessage('SPOL_TPL_TWO_GOODS');
										}
										else
										{
											echo Loc::getMessage('SPOL_TPL_GOODS');
										}
										?>
										<?=Loc::getMessage('SPOL_TPL_SUMOF')?>
										<?=$order['ORDER']['FORMATED_PRICE']?>
									</h2>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 sale-order-list-inner-container">
									<span class="sale-order-list-inner-title-line">
										<span class="sale-order-list-inner-title-line-item"><?=Loc::getMessage('SPOL_TPL_PAYMENT')?></span>
										<span class="sale-order-list-inner-title-line-border"></span>
									</span>
									<?
									$showDelimeter = false;
									foreach ($order['PAYMENT'] as $payment)
									{
										if ($order['ORDER']['LOCK_CHANGE_PAYSYSTEM'] !== 'Y')
										{
											$paymentChangeData[$payment['ACCOUNT_NUMBER']] = array(
												"order" => htmlspecialcharsbx($order['ORDER']['ACCOUNT_NUMBER']),
												"payment" => htmlspecialcharsbx($payment['ACCOUNT_NUMBER']),
												"allow_inner" => $arParams['ALLOW_INNER'],
												"only_inner_full" => $arParams['ONLY_INNER_FULL']
											);
										}
										?>
										<div class="row sale-order-list-inner-row">
											<?
											if ($showDelimeter)
											{
												?>
												<div class="sale-order-list-top-border"></div>
												<?
											}
											else
											{
												$showDelimeter = true;
											}
											?>

											<div class="sale-order-list-inner-row-body">
												<div class="col-md-9 col-sm-8 col-xs-12 sale-order-list-payment">
													<div class="sale-order-list-payment-title">
														<?
														$paymentSubTitle = Loc::getMessage('SPOL_TPL_BILL')." ".Loc::getMessage('SPOL_TPL_NUMBER_SIGN').htmlspecialcharsbx($payment['ACCOUNT_NUMBER']);
														if(isset($payment['DATE_BILL']))
														{
															$paymentSubTitle .= " ".Loc::getMessage('SPOL_TPL_FROM_DATE')." ".$payment['DATE_BILL']->format($arParams['ACTIVE_DATE_FORMAT']);
														}
														$paymentSubTitle .=",";
														echo $paymentSubTitle;
														?>
														<span class="sale-order-list-payment-title-element"><?=$payment['PAY_SYSTEM_NAME']?></span>
														<?
														if ($payment['PAID'] === 'Y')
														{
															?>
															<span class="sale-order-list-status-success"><?=Loc::getMessage('SPOL_TPL_PAID')?></span>
															<?
														}
														elseif ($order['ORDER']['IS_ALLOW_PAY'] == 'N')
														{
															?>
															<span class="sale-order-list-status-restricted"><?=Loc::getMessage('SPOL_TPL_RESTRICTED_PAID')?></span>
															<?
														}
														else
														{
															?>
															<span class="sale-order-list-status-alert"><?=Loc::getMessage('SPOL_TPL_NOTPAID')?></span>
															<?
														}
														?>
													</div>
													<div class="sale-order-list-payment-price">
														<span class="sale-order-list-payment-element"><?=Loc::getMessage('SPOL_TPL_SUM_TO_PAID')?>:</span>

														<span class="sale-order-list-payment-number"><?=$payment['FORMATED_SUM']?></span>
													</div>
													<?
													if (!empty($payment['CHECK_DATA']))
													{
														$listCheckLinks = "";
														foreach ($payment['CHECK_DATA'] as $checkInfo)
														{
															$title = Loc::getMessage('SPOL_CHECK_NUM', array('#CHECK_NUMBER#' => $checkInfo['ID']))." - ". htmlspecialcharsbx($checkInfo['TYPE_NAME']);
															if (strlen($checkInfo['LINK']))
															{
																$link = $checkInfo['LINK'];
																$listCheckLinks .= "<div><a href='$link' target='_blank'>$title</a></div>";
															}
														}
														if (strlen($listCheckLinks) > 0)
														{
															?>
															<div class="sale-order-list-payment-check">
																<div class="sale-order-list-payment-check-left"><?= Loc::getMessage('SPOL_CHECK_TITLE')?>:</div>
																<div class="sale-order-list-payment-check-left">
																	<?=$listCheckLinks?>
																</div>
															</div>
															<?
														}
													}
													if ($payment['PAID'] !== 'Y' && $order['ORDER']['LOCK_CHANGE_PAYSYSTEM'] !== 'Y')
													{
														?>
														<a href="#" class="sale-order-list-change-payment" id="<?= htmlspecialcharsbx($payment['ACCOUNT_NUMBER']) ?>">
															<?= Loc::getMessage('SPOL_TPL_CHANGE_PAY_TYPE') ?>
														</a>
														<?
													}
													if ($order['ORDER']['IS_ALLOW_PAY'] == 'N' && $payment['PAID'] !== 'Y')
													{
														?>
														<div class="sale-order-list-status-restricted-message-block">
															<span class="sale-order-list-status-restricted-message"><?=Loc::getMessage('SOPL_TPL_RESTRICTED_PAID_MESSAGE')?></span>
														</div>
														<?
													}
													?>

												</div>
												<?
												if ($payment['PAID'] === 'N' && $payment['IS_CASH'] !== 'Y')
												{
													if ($order['ORDER']['IS_ALLOW_PAY'] == 'N')
													{
														?>
														<div class="col-md-3 col-sm-4 col-xs-12 sale-order-list-button-container">
															<a class="sale-order-list-button inactive-button">
																<?=Loc::getMessage('SPOL_TPL_PAY')?>
															</a>
														</div>
														<?
													}
													elseif ($payment['NEW_WINDOW'] === 'Y')
													{
														?>
														<div class="col-md-3 col-sm-4 col-xs-12 sale-order-list-button-container">
															<a class="sale-order-list-button" target="_blank" href="<?=htmlspecialcharsbx($payment['PSA_ACTION_FILE'])?>">
																<?=Loc::getMessage('SPOL_TPL_PAY')?>
															</a>
														</div>
														<?
													}
													else
													{
														?>
														<div class="col-md-3 col-sm-4 col-xs-12 sale-order-list-button-container">
															<a class="sale-order-list-button ajax_reload" href="<?=htmlspecialcharsbx($payment['PSA_ACTION_FILE'])?>">
																<?=Loc::getMessage('SPOL_TPL_PAY')?>
															</a>
														</div>
														<?
													}
												}
												?>

											</div>
											<div class="col-lg-9 col-md-9 col-sm-10 col-xs-12 sale-order-list-inner-row-template">
												<a class="sale-order-list-cancel-payment">
													<i class="fa fa-long-arrow-left"></i> <?=Loc::getMessage('SPOL_CANCEL_PAYMENT')?>
												</a>
											</div>
										</div>
										<?
									}
									if (!empty($order['SHIPMENT']))
									{
										?>
										<div class="sale-order-list-inner-title-line">
											<span class="sale-order-list-inner-title-line-item"><?=Loc::getMessage('SPOL_TPL_DELIVERY')?></span>
											<span class="sale-order-list-inner-title-line-border"></span>
										</div>
										<?
									}
									$showDelimeter = false;
									foreach ($order['SHIPMENT'] as $shipment)
									{
										if (empty($shipment))
										{
											continue;
										}
										?>
										<div class="row sale-order-list-inner-row">
											<?
												if ($showDelimeter)
												{
													?>
													<div class="sale-order-list-top-border"></div>
													<?
												}
												else
												{
													$showDelimeter = true;
												}
											?>
											<div class="col-md-9 col-sm-8 col-xs-12 sale-order-list-shipment">
												<div class="sale-order-list-shipment-title">
												<span class="sale-order-list-shipment-element">
													<?=Loc::getMessage('SPOL_TPL_LOAD')?>
													<?
													$shipmentSubTitle = Loc::getMessage('SPOL_TPL_NUMBER_SIGN').htmlspecialcharsbx($shipment['ACCOUNT_NUMBER']);
													if ($shipment['DATE_DEDUCTED'])
													{
														$shipmentSubTitle .= " ".Loc::getMessage('SPOL_TPL_FROM_DATE')." ".$shipment['DATE_DEDUCTED']->format($arParams['ACTIVE_DATE_FORMAT']);
													}

													if ($shipment['FORMATED_DELIVERY_PRICE'])
													{
														$shipmentSubTitle .= ", ".Loc::getMessage('SPOL_TPL_DELIVERY_COST')." ".$shipment['FORMATED_DELIVERY_PRICE'];
													}
													echo $shipmentSubTitle;
													?>
												</span>
													<?
													if ($shipment['DEDUCTED'] == 'Y')
													{
														?>
														<span class="sale-order-list-status-success"><?=Loc::getMessage('SPOL_TPL_LOADED');?></span>
														<?
													}
													else
													{
														?>
														<span class="sale-order-list-status-alert"><?=Loc::getMessage('SPOL_TPL_NOTLOADED');?></span>
														<?
													}
													?>
												</div>

												<div class="sale-order-list-shipment-status">
													<span class="sale-order-list-shipment-status-item"><?=Loc::getMessage('SPOL_ORDER_SHIPMENT_STATUS');?>:</span>
													<span class="sale-order-list-shipment-status-block"><?=htmlspecialcharsbx($shipment['DELIVERY_STATUS_NAME'])?></span>
												</div>

												<?
												if (!empty($shipment['DELIVERY_ID']))
												{
													?>
													<div class="sale-order-list-shipment-item">
														<?=Loc::getMessage('SPOL_TPL_DELIVERY_SERVICE')?>:
														<?=$arResult['INFO']['DELIVERY'][$shipment['DELIVERY_ID']]['NAME']?>
													</div>
													<?
												}

												if (!empty($shipment['TRACKING_NUMBER']))
												{
													?>
													<div class="sale-order-list-shipment-item">
														<span class="sale-order-list-shipment-id-name"><?=Loc::getMessage('SPOL_TPL_POSTID')?>:</span>
														<span class="sale-order-list-shipment-id"><?=htmlspecialcharsbx($shipment['TRACKING_NUMBER'])?></span>
														<span class="sale-order-list-shipment-id-icon"></span>
													</div>
													<?
												}
												?>
											</div>
											<?
											if (strlen($shipment['TRACKING_URL']) > 0)
											{
												?>
												<div class="col-md-2 col-md-offset-1 col-sm-12 sale-order-list-shipment-button-container">
													<a class="sale-order-list-shipment-button" target="_blank" href="<?=$shipment['TRACKING_URL']?>">
														<?=Loc::getMessage('SPOL_TPL_CHECK_POSTID')?>
													</a>
												</div>
												<?
											}
											?>
										</div>
										<?
									}
									?>
									<div class="row sale-order-list-inner-row">
										<div class="sale-order-list-top-border"></div>
										<div class="col-md-8  col-sm-12 sale-order-list-about-container">
											<a class="sale-order-list-about-link" href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_DETAIL"])?>"><?=Loc::getMessage('SPOL_TPL_MORE_ON_ORDER')?></a>
										</div>
										<div class="col-md-2 col-sm-12 sale-order-list-repeat-container">
											<a class="sale-order-list-repeat-link" href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_COPY"])?>"><?=Loc::getMessage('SPOL_TPL_REPEAT_ORDER')?></a>
										</div>
										<div class="col-md-2 col-sm-12 sale-order-list-cancel-container">
											<a class="sale-order-list-cancel-link" href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_CANCEL"])?>"><?=Loc::getMessage('SPOL_TPL_CANCEL_ORDER')?></a>
										</div>
									</div>
								</div>
							</div>
						</div>

					}
				}
				else
				{
					$orderHeaderStatus = null;

					if ($_REQUEST["show_canceled"] === 'Y' && count($arResult['ORDERS']))
					{
						?>
						<h1 class="sale-order-title">
							Cancel<?= Loc::getMessage('SPOL_TPL_ORDERS_CANCELED_HEADER') ?>
						</h1>
						<?
					}

					foreach ($arResult['ORDERS'] as $key => $order)
					{
						if ($orderHeaderStatus !== $order['ORDER']['STATUS_ID'] && $_REQUEST["show_canceled"] !== 'Y')
						{
							$orderHeaderStatus = $order['ORDER']['STATUS_ID'];
							?>
							<h1 class="sale-order-title">
								OK<?= Loc::getMessage('SPOL_TPL_ORDER_IN_STATUSES') ?> &laquo;<?=htmlspecialcharsbx($arResult['INFO']['STATUS'][$orderHeaderStatus]['NAME'])?>&raquo;
							</h1>
							<?
						}
						?>
						<div class="col-md-12 col-sm-12 sale-order-list-container">
							<div class="row">
								<div class="col-md-12 col-sm-12 sale-order-list-accomplished-title-container">
									<div class="row">
										<div class="col-md-8 col-sm-12 sale-order-list-accomplished-title-container">
											<h2 class="sale-order-list-accomplished-title">
												<?= Loc::getMessage('SPOL_TPL_ORDER') ?>
												<?= Loc::getMessage('SPOL_TPL_NUMBER_SIGN') ?>
												<?= htmlspecialcharsbx($order['ORDER']['ACCOUNT_NUMBER'])?>
												<?= Loc::getMessage('SPOL_TPL_FROM_DATE') ?>
												<?= $order['ORDER']['DATE_INSERT'] ?>,
												<?= count($order['BASKET_ITEMS']); ?>
												<?
												$count = substr(count($order['BASKET_ITEMS']), -1);
												if ($count == '1')
												{
													echo Loc::getMessage('SPOL_TPL_GOOD');
												}
												elseif ($count >= '2' || $count <= '4')
												{
													echo Loc::getMessage('SPOL_TPL_TWO_GOODS');
												}
												else
												{
													echo Loc::getMessage('SPOL_TPL_GOODS');
												}
												?>
												<?= Loc::getMessage('SPOL_TPL_SUMOF') ?>
												<?= $order['ORDER']['FORMATED_PRICE'] ?>
											</h2>
										</div>
										<div class="col-md-4 col-sm-12 sale-order-list-accomplished-date-container">
											<?
											if ($_REQUEST["show_canceled"] !== 'Y')
											{
												?>
												<span class="sale-order-list-accomplished-date">
													<?= Loc::getMessage('SPOL_TPL_ORDER_FINISHED')?>
												</span>
												<?
											}
											else
											{
												?>
												<span class="sale-order-list-accomplished-date canceled-order">
													<?= Loc::getMessage('SPOL_TPL_ORDER_CANCELED')?>
												</span>
												<?
											}
											?>
											<span class="sale-order-list-accomplished-date-number"><?= $order['ORDER']['DATE_STATUS_FORMATED'] ?></span>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 sale-order-list-inner-accomplished">
									<div class="row sale-order-list-inner-row">
										<div class="col-md-3 col-sm-12 sale-order-list-about-accomplished">
											<a class="sale-order-list-about-link" href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_DETAIL"])?>">
												<?=Loc::getMessage('SPOL_TPL_MORE_ON_ORDER')?>
											</a>
										</div>
										<div class="col-md-3 col-md-offset-6 col-sm-12 sale-order-list-repeat-accomplished">
											<a class="sale-order-list-repeat-link sale-order-link-accomplished" href="<?=htmlspecialcharsbx($order["ORDER"]["URL_TO_COPY"])?>">
												<?=Loc::getMessage('SPOL_TPL_REPEAT_ORDER')?>
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?
					}
				}/*pink80*/?>
				<?
				?>
			</div>
		</div><!--margin_right_sidebar-->
	</section>
	<div class="clearfix"></div>
	<?

	echo $arResult["NAV_STRING"];

	if ($_REQUEST["filter_history"] !== 'Y')
	{
		$javascriptParams = array(
			"url" => CUtil::JSEscape($this->__component->GetPath().'/ajax.php'),
			"templateFolder" => CUtil::JSEscape($templateFolder),
			"paymentList" => $paymentChangeData
		);
		$javascriptParams = CUtil::PhpToJSObject($javascriptParams);
		?>
		<script>
			BX.Sale.PersonalOrderComponent.PersonalOrderList.init(<?=$javascriptParams?>);
		</script>
		<?
	}
}
?>
