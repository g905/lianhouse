<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
	use Bitrix\Main,
		Bitrix\Main\Localization\Loc,
		Bitrix\Main\Page\Asset;
Asset::getInstance()->addJs("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/script.js");
Asset::getInstance()->addCss("/bitrix/components/bitrix/sale.order.payment.change/templates/.default/style.css");
$this->addExternalCss("/bitrix/css/main/bootstrap.css");
CJSCore::Init(array('clipboard', 'fx'));

Loc::loadMessages(__FILE__);

/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
global $USER;
$id_user = $USER->GetID(); // ID пользователя
$countfaq = 0;
if (empty($id_user))
	{
		$APPLICATION->AuthForm('', false, false, 'N', false);
	}

else
{
	//количество заказов пользователя
$arFilter = Array("USER_ID" => $id_user);
$sql = CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $arFilter);
while ($result = $sql->Fetch()){
	if($result["CANCELED"]=="Y"){$userordescancel++;}elseif($result["STATUS_ID"]=="F"){$userordesfinish++;}elseif($result["STATUS_ID"]=="Y"){$userordesshipped++;}else{$userordesprocess++;}
	$userordestotal++;
	}
//echo "<pre>";
//var_dump ($arResult['ITEMS']);
//echo "</pre>";

foreach ($arResult['ITEMS'] as $key=>$val):
if ($val["CREATED_BY"] == $id_user){$countfaq++;}
endforeach;
?>

	<div class="pa_line_block">
						<a href="/personal/order/" class="pal_block">
							<div class="pal_inner_block pib1">
								<p class="pal_name"><?= Loc::getMessage('SPOL_CHECK_PIB1_NAME')?></p>
								<p class="pal_text"><?= Loc::getMessage('SPOL_CHECK_PIB1_TEXT')?></p>
								<span class="pal_namber"><?=$userordesshipped + $userordesprocess + $userordescancel?></span>
							</div>
						</a>
						<a href="/personal/order/index.php?filter_history=Y" class="pal_block">
							<div class="pal_inner_block pib2">
								<p class="pal_name"><?= Loc::getMessage('SPOL_CHECK_PIB2_NAME')?></p>
								<p class="pal_text"><?= Loc::getMessage('SPOL_CHECK_PIB2_TEXT')?></p>
							</div>
						</a>
						<a href="/personal/faq/" class="pal_block">
							<div class="pal_inner_block pib3">
								<p class="pal_name"><?= Loc::getMessage('SPOL_CHECK_PIB3_NAME')?></p>
								<p class="pal_text"><?= Loc::getMessage('SPOL_CHECK_PIB3_TEXT')?></p>
								<span class="pal_namber"><?=$countfaq?></span>
							</div>
						</a>
						<a href="/#" class="pal_block">
							<div class="pal_inner_block pib4">
								<p class="pal_name"><?= Loc::getMessage('SPOL_CHECK_PIB4_NAME')?></p>
								<p class="pal_text"><?= Loc::getMessage('SPOL_CHECK_PIB4_TEXT')?></p>
							</div>
						</a>
					</div>
<!--================================================================-->
<section class="delivery_s1 pers_area">
		<div class="del_s1_left">
			<div class="pa_navigation">
				<a href="/personal/profile/"><?= Loc::getMessage('PA_NAVIGATION_PROFILE')?></a>
				<a href="/#"><?= Loc::getMessage('PA_NAVIGATION_PASSWORD')?></a>
				<a href="/#"><?= Loc::getMessage('PA_NAVIGATION_ADDRESS')?></a>
				<a href=<?=$APPLICATION->GetCurPageParam("logout=yes", Array("login"))?>"><?= Loc::getMessage('PA_NAVIGATION_LOGOUT')?></a>
			</div>
			<?/* Модуль авторизации, подключить при необходимости
			$APPLICATION->IncludeComponent(
				"bitrix:system.auth.form", 
				"login_order", 
				array(
					"PROFILE_URL" => "/personal/profile/",
					"SHOW_ERRORS" => "N",
					"COMPONENT_TEMPLATE" => "login_order",
					"REGISTER_URL" => "",
					"FORGOT_PASSWORD_URL" => ""
				),
				false
			);*/?>

		</div>



	<div class="margin_right_sidebar">
							
							<div class="pa_my_questions">
								
								<div class="pamq_top">
									<a class="pamq_a1" href="/#">������� ������ ������  <span>(1)</span></a>
									<a class="pamq_a2" href="/#">������� ������ ���������  <span>(2)</span></a>
									<a class="pamq_a3" href="/#">������ �����  <span>(2)</span></a>
									<a class="pamq_a4 pamq_active" href="/#">��� <span>(4)</span></a>
								</div>
								
								<div class="pamq_bottom">




									<div class="chat_block">
										<div class="cb_name_block">
											<p class="cb_name">������ � <span>1</span></p>
											<span class="cb_expectation">������� ������ ������</span>
											<span class="cb_themename">����: �������� ������</span>
											<span class="cb_smallname">��� ��������: �������� �������</span>
										</div>
										<div class="cb_hide_block">
											<div class="cbhb_top">
												<span class="cb_line2"></span>
												<div class="cbnb_name_block">
													<p class="cbhb_name">��� �� ������� � <span>1</span></p>
													<span class="cbnb_status">������ �����</span>
												</div>
												<span class="cbnb_show_answer">�������� ��� ������ (4)</span>
												<div class="clear"></div>
											</div>
											<div class="cbnb_bottom">
												<div>
													<!---->
													<div class="cbnb_message">
														<div class="cbnbm_img">
															<img src="img/message.jpg" alt="">	
														</div>
														<div class="cbnbm_block_text">
															<p class="cbnbm_name">����� �������<span class="cbnbm_date"><i class="fa fa-clock-o" aria-hidden="true"></i> �����, 17:48</span></p>
															<div class="cbnbm_text">
																������ ����, ������� �� ��������� � ������ ���������, �� ����������� ����������� � ����� �������� � ����� ��������� �����. ��� ������ ��� ����� �� ��� ����� ���������, ��� ���� ����� ������� � ����� �������. � ������ �� ������ ����������� ������ ������ � ������� Ҍ�� ������, ��� ������ ���������� ����������.
															</div>
														</div>
													</div>
													<!---->
													<div class="cbnb_message">
														<div class="cbnbm_img">
															<img src="img/catalog3.jpg" alt="">	
														</div>
														<div class="cbnbm_block_text">
															<p class="cbnbm_name">����� �������<span class="cbnbm_date"><i class="fa fa-clock-o" aria-hidden="true"></i> �����, 17:48</span></p>
															<div class="cbnbm_text">
																������ ����, ������� �� ��������� � ������ ���������, �� ����������� ����������� � ����� �������� � ����� ��������� �����. ��� ������ ��� ����� �� ��� ����� ���������, ��� ���� ����� ������� � ����� �������. � ������ �� ������ ����������� ������ ������ � ������� Ҍ�� ������, ��� ������ ���������� ����������.
															</div>
														</div>
													</div>
													<!---->
												</div>


												<form class="wrap_question" action="" method="" enctype="multipart/form-data">
													<div class="bq_elem">
														<textarea name=""></textarea>
														<div class="file_question">
															<input class="file_question" name="" type="file">
														</div>
													</div>
													<button class="button2" type="submit">������ ������</button>
													<div class="clear"></div>				
												</form>



											</div>
										</div>
										<div class="cb_hide_show">
											<span class="cb_line"></span>
											���������� ���<i class="fa fa-angle-down" aria-hidden="true"></i>
										</div>					
									</div><!--chat_block-->



<!--==================================================================================-->

									<div class="chat_block">
										<div class="cb_name_block">
											<p class="cb_name">������ � <span>1</span></p>
											<span class="cb_expectation">������� ������ ������</span>
											<span class="cb_themename">����: �������� ������</span>
											<span class="cb_smallname">��� ��������: �������� �������</span>
										</div>
										<div class="cb_hide_block">
											<div class="cbhb_top">
												<span class="cb_line2"></span>
												<div class="cbnb_name_block">
													<p class="cbhb_name">��� �� ������� � <span>1</span></p>
													<span class="cbnb_status">������ �����</span>
												</div>
												<span class="cbnb_show_answer">�������� ��� ������ (4)</span>
												<div class="clear"></div>
											</div>
											<div class="cbnb_bottom">
												<div>
													<!---->
													<div class="cbnb_message">
														<div class="cbnbm_img">
															<img src="img/message.jpg" alt="">	
														</div>
														<div class="cbnbm_block_text">
															<p class="cbnbm_name">����� �������<span class="cbnbm_date"><i class="fa fa-clock-o" aria-hidden="true"></i> �����, 17:48</span></p>
															<div class="cbnbm_text">
																������ ����, ������� �� ��������� � ������ ���������, �� ����������� ����������� � ����� �������� � ����� ��������� �����. ��� ������ ��� ����� �� ��� ����� ���������, ��� ���� ����� ������� � ����� �������. � ������ �� ������ ����������� ������ ������ � ������� Ҍ�� ������, ��� ������ ���������� ����������.
															</div>
														</div>
													</div>
													<!---->
													<div class="cbnb_message">
														<div class="cbnbm_img">
															<img src="img/catalog3.jpg" alt="">	
														</div>
														<div class="cbnbm_block_text">
															<p class="cbnbm_name">����� �������<span class="cbnbm_date"><i class="fa fa-clock-o" aria-hidden="true"></i> �����, 17:48</span></p>
															<div class="cbnbm_text">
																������ ����, ������� �� ��������� � ������ ���������, �� ����������� ����������� � ����� �������� � ����� ��������� �����. ��� ������ ��� ����� �� ��� ����� ���������, ��� ���� ����� ������� � ����� �������. � ������ �� ������ ����������� ������ ������ � ������� Ҍ�� ������, ��� ������ ���������� ����������.
															</div>
														</div>
													</div>
													<!---->
												</div>
												<form class="wrap_question" action="" method="" enctype="multipart/form-data">
													<div class="bq_elem">
														<textarea name=""></textarea>
														<div class="file_question">
															<input class="file_question" name="" type="file">
														</div>
													</div>
													<button class="button2" type="submit">������ ������</button>
													<div class="clear"></div>				
												</form>
											</div>
										</div>
										<div class="cb_hide_show">
											<span class="cb_line"></span>
											���������� ���<i class="fa fa-angle-down" aria-hidden="true"></i>
										</div>					
									</div><!--chat_block-->



								</div><!--pamq_bottom-->
								<div class="wrap_pagenation">
									<div class="pagenation">
										<a class="pag_arrow" href="/#"><i class="fa fa-chevron-left" aria-hidden="true"></i></a>
										<div class="pages">
											<a class="active_pagen" href="/#">1</a>
											<a href="/#">2</a>
											<a href="/#">3</a>
											<a href="/#">4</a>
											<a class="pag_dots" href="/#">...</a>
											<a href="/#">8</a>
										</div>
										<a class="pag_arrow" href="/#"><i class="fa fa-chevron-right" aria-hidden="true"></i></a>
									</div>
								</div>
						

							</div>

						
<?//elements list?>
<a name="top"></a>
<?/*foreach ($arResult['ITEMS'] as $key=>$val):?>
<?
if ($val["CREATED_BY"] == $id_user){
?>



	<li class="point-faq"><a href="#<?=$val["ID"]?>"><?=$val['NAME']?></a><br/></li>
	<?}?>
<?endforeach;*/?>
<br/>
<?foreach ($arResult['ITEMS'] as $key=>$val):?>
<?if ($val["CREATED_BY"] == $id_user){?>
<?

/*
$APPLICATION->IncludeComponent("api:qa.list", "faq", Array(
	"COMPONENT_TEMPLATE" => ".default",
		"INCLUDE_CSS" => "Y",	// ���������� ���������� �����
		"ADMIN_EMAIL" => "pink80@udm.ru",	// E-mail ��������������
		"ACTIVE" => "Y",	// ������������
		"MESS_ACTIVE" => "���� ��������� ������� ��������� � ����� ������������ ����� �������� �����������",	// ����� ����������� � ���������
		"ALLOW" => "ALL",	// ��������� ��������
		"MESS_ALLOW_USER" => "�������� ����� ������ �������������� ������������",	// ����� ����������� ��� "��������������"
		"MESS_ALLOW_EDITOR" => "�������� ����� ������ ��������� �����",	// ����� ����������� ��� "���������"
		"PAGE_TITLE" => "",	// ��������� ��������
		"PAGE_URL" => $arResult["DETAIL_PAGE_URL"],	// HTTP-����� ��������
		"HASH" => "",	// #����� ��������� ������
		"DATE_FORMAT" => "d.m.Y",	// ������ ����
		"THEME" => "flat",	// ����
		"COLOR" => "orange1",	// ����
		"IBLOCK_ID" => "7",	// ID ���������
		"ELEMENT_ID" => "21",	// ID ��������
		"XML_ID" => "",	// ������� ���
		"CODE" => "",	// ���������� ���
		"FORM_QUESTION_MESS_TITLE" => "������ ������",	// ��������� �����
		"FORM_QUESTION_MESS_SUBMIT" => "���������",	// ����� ������
		"FORM_QUESTION_MESS_SUBMIT_AJAX" => "������������",	// ����� ���� ������
		"FORM_QUESTION_MESS_REPLY" => "�������� ������ �� �����",	// ����� �������� �� ������
		"FORM_QUESTION_MESS_NAME" => "���� ��� *",	// ���� "���� ���"
		"FORM_QUESTION_MESS_EMAIL" => "��. ����� *",	// ���� "��. �����"
		"FORM_QUESTION_MESS_TEXT" => "������ *",	// ���� "������"
		"FORM_ANSWER_MESS_SUBMIT_AJAX" => "������������",	// ����� ���� ������
		"FORM_ANSWER_MESS_TITLE" => "�������� �����",	// ��������� �����
		"FORM_ANSWER_MESS_SUBMIT" => "���������",	// ����� ������
		"FORM_ANSWER_MESS_REPLY" => "�������� ������ �� �����",	// ����� �������� �� ������
		"FORM_ANSWER_MESS_NAME" => "���� ��� *",	// ���� "���� ���"
		"FORM_ANSWER_MESS_EMAIL" => "��. ����� *",	// ���� "��. �����"
		"FORM_ANSWER_MESS_TEXT" => "����� *",	// ���� "�����"
		"LIST_QUESTION_MESS_LINK" => "������ �� ������/����� �#ID#",	// ����� ������ �� ������/����� � ����
		"LIST_QUESTION_MESS_CONFIRM_DELETE" => "������� ������ �#ID#?<br>��� ������ � ����������� � ���� ������ ����� �������",	// ����� �������� ������ � ����
		"LIST_QUESTION_MESS_CONFIRM_ERASE" => "������� ������� ������ �#ID#?",	// ����� �������� ������ � ����
		"LIST_QUESTION_MESS_TEXT_ERASE" => "��������� ������",	// ����� ������� ������
		"LIST_QUESTION_MESS_EXPERT" => "�������",	// ����� ������ "�������"
		"LIST_QUESTION_MESS_BUTTON_ANSWER" => "��������",	// ����� ������ "��������"
		"LIST_QUESTION_MESS_BUTTON_EDIT" => "��������",	// ����� ������ "��������"
		"LIST_QUESTION_MESS_BUTTON_SAVE" => "���������",	// ����� ������ "���������"
		"LIST_QUESTION_MESS_BUTTON_CANCEL" => "������",	// ����� ������ "������"
		"LIST_QUESTION_MESS_BUTTON_ERASE" => "�������",	// ����� ������ "�������"
		"LIST_QUESTION_MESS_BUTTON_DELETE" => "�������",	// ����� ������ "�������"
		"USE_PRIVACY" => "Y",	// �������� ���������� �� ��������� ������������ ������
		"MESS_PRIVACY" => "� �������� �� ��������� ������������ ������",	// ���������������� ����������
		"MESS_PRIVACY_LINK" => "",	// ������ �� ����������
		"MESS_PRIVACY_CONFIRM" => "��� ����������� �� ������ ������� ���������� �� ��������� ������������ ������",	// ��������� � ������������� ������� ����������
	),
	false
);
*/
	$this->AddEditAction($val['ID'],$val['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($val['ID'],$val['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BSFE_ELEMENT_DELETE_CONFIRM')));
?>
<a name="<?=$val["ID"]?>"></a>
<table cellpadding="0" cellspacing="0" class="data-table" width="100%"  id="<?=$this->GetEditAreaId($val['ID']);?>">
	<tr>
		<th>
		<?=$val['NAME']?>
		</th>
	</tr>
	<tr>
		<td>
		<?=$val['PREVIEW_TEXT']?>
		<?=$val['DETAIL_TEXT']?>
		<br/>
		<div style="float: left"><a href="#top"><?=GetMessage("SUPPORT_FAQ_GO_UP")?></a></div>
		<?if ($arParams["SHOW_RATING"] == "Y"):?>
			<div class="faq-rating" style="float: right">
			<?
			$GLOBALS["APPLICATION"]->IncludeComponent(
				"bitrix:rating.vote", $arParams["RATING_TYPE"],
				Array(
					"ENTITY_TYPE_ID" => "IBLOCK_ELEMENT",
					"ENTITY_ID" => $val['ID'],
					"OWNER_ID" => $val['CREATED_BY'],
					"USER_VOTE" => $arResult['RATING'][$val['ID']]["USER_VOTE"],
					"USER_HAS_VOTED" => $arResult['RATING'][$val['ID']]["USER_HAS_VOTED"],
					"TOTAL_VOTES" => $arResult['RATING'][$val['ID']]["TOTAL_VOTES"],
					"TOTAL_POSITIVE_VOTES" => $arResult['RATING'][$val['ID']]["TOTAL_POSITIVE_VOTES"],
					"TOTAL_NEGATIVE_VOTES" => $arResult['RATING'][$val['ID']]["TOTAL_NEGATIVE_VOTES"],
					"TOTAL_VALUE" => $arResult['RATING'][$val['ID']]["TOTAL_VALUE"],
					"PATH_TO_USER_PROFILE" => $arParams["PATH_TO_USER"],
				),
				$component,
				array("HIDE_ICONS" => "Y")
			);?>
			</div>
		<?endif;?>		
		</td>
	</tr>
</table>
<br/>
<?}?>
<?endforeach;?>
</div><!--margin_right_sidebar-->
</section>
<?}?>