<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);?>
<div class="search-form">
<form method="get" action="<?=$arResult["FORM_ACTION"]?>">
	<div class="hs3_b2_select">
		<div class="select_dropdown"></div>
		<select name="categories" id="select_categories">
			<option value="all">��� ���������</option>
			<?foreach(getCategories(1) as $categories): ?>
				<option value="<?=$categories['UF_ID_CATEGORY']; ?>"><?=$categories['NAME']; ?></option>
			<?endforeach; ?>
		</select>
		</div>
		<div class="hs3_b2_group">
			<input type="text" name="q" value="" maxlength="50" placeholder="����� 47 000 �������">
			<button class="btn_search" type="submit"></button>
		</div>
	</div>
</form>

