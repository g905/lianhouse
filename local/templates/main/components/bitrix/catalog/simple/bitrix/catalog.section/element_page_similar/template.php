<?
use Bitrix\Main\Localization\Loc;

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

$this->setFrameMode(true);?>

<div class="cdb1_similar_products">
    <p class="cdb1_sp_name"><? echo Loc::getMessage('MOISEO_SIMILAR_HEADER'); ?></p>
    <div id="cdb1_similar_carousel" class="cdb1_similar_carousel owl-carousel" >
        <!---->
        <?php foreach ($arResult["ITEMS"] as $arElement): ?>
            <?
                $this->AddEditAction($arElement['ID'], $arElement['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arElement['ID'], $arElement['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BCS_ELEMENT_DELETE_CONFIRM')));
            ?>
            <div class="cb_block tabs_catalog">
                <div class="cbb_inner cbb_left">
                    <a href="<?=$arElement['DETAIL_PAGE_URL']; ?>">
                        <img src="<?= $arElement['DETAIL_PICTURE']['SRC']; ?>" alt="<?= $arElement['NAME']; ?>">
                    </a>
                    <p class="cbb_name"><?= $arElement['NAME']; ?></p>
                </div>
                <div class="cbb_inner cbb_right">
                    <div class="cbb_wrap">
                        <p class="cbb_name"><?= $arElement['NAME']; ?></p>
                        <div class="cbb_popular">
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                            <i class="fa fa-star" aria-hidden="true"></i>
                        </div>
                    </div>
                    <div class="cbb_bottom_block">
                        <?if($arElement["ITEM_PRICES"][0]["RATIO_DISCOUNT"] > 0):?>
                            <s><?=$arElement["ITEM_PRICES"][0]["PRINT_BASE_PRICE"];?></s> <p class="cbb_pricce"><?=$arElement["ITEM_PRICES"][0]["PRINT_PRICE"];?></p>
                        <?else:?>
                            <p class="cbb_pricce"><?=$arElement["ITEM_PRICES"][0]["PRINT_BASE_PRICE"];?></p>
                        <?endif;?>
                        <div class="cbb_icons">
                            <a href="/#"><i class="icon1 fa fa-heart" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
   