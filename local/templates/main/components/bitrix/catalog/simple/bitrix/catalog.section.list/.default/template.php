<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
$this->setFrameMode(true);
?>
<div class="cm_hide">
    <span>������ ����������</span>
</div>
<div class="dr_wrap_content">
    <div class="cat_message">
        <p>
           ���������� ����������, ��� ���� � ���� ��������� ������� ������� ��� ����� ��������. ��������� �������� ������� �� ������ � ���� �����, ������ ��������� �������� ����������������� ���������� ������ � ������ �������� ����� ������ ����������. ���� ��� �� ������� ����� ��������� ������ �� ������ ���������� �� ������ � ����� ������!
        </p>
        <p>
            ���� � ��� ��������� �������� � ������� ������, ���������� � ��� � <a href="#">������ ���������</a> ����� ������ ������� �� ����������� ������� ����� ����� ����� �� ����� �������� ����!
        </p>
    </div>
</div>
<div class="section__title"><?= $arResult["SECTION"]['NAME']; ?></div>
<p>��������� ������������:</p>
<div class="b-tags-list__list">
<?
foreach($arResult["SECTIONS"] as $arSection):

    //echo $CURRENT_DEPTH . '-' . $arSection['RELATIVE_DEPTH_LEVEL'];
	$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_EDIT"));
	$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], CIBlock::GetArrayByID($arSection["IBLOCK_ID"], "SECTION_DELETE"), array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM')));
	if($arSection["RELATIVE_DEPTH_LEVEL"] == 1):
		//echo "\n",str_repeat("\t", $arSection["DEPTH_LEVEL"]-$TOP_DEPTH),"<ul>";
//	elseif($CURRENT_DEPTH == $arSection["DEPTH_LEVEL"])
//		echo "</li>";
//	else
//	{
//		while($CURRENT_DEPTH > $arSection["DEPTH_LEVEL"])
//		{
//			echo "</li>";
//			echo "\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH),"</ul>","\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH-1);
//			$CURRENT_DEPTH--;
//		}
//		echo "\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH),"</li>";
//	}

//	echo "\n",str_repeat("\t", $arSection["DEPTH_LEVEL"]-$TOP_DEPTH);
//	?>
        <a href="<?=$arSection["SECTION_PAGE_URL"]?>" id="<?=$this->GetEditAreaId($arSection['ID']);?>">
            <?=$arSection["NAME"]?>
        </a>

	<?
	//$CURRENT_DEPTH = $arSection["DEPTH_LEVEL"];
    endif;
endforeach;
//while($CURRENT_DEPTH > $TOP_DEPTH)
//{
//	echo "</li>";
//	echo "\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH),"</ul>","\n",str_repeat("\t", $CURRENT_DEPTH-$TOP_DEPTH-1);
//	$CURRENT_DEPTH--;
//}
?>

</div>
  <?if(count($arResult["SECTIONS"])==0):?>
  <div class="b-tags-list__list">
<?if(CModule::IncludeModule("iblock")):?>
    <?
    $by = "NAME";
    $order = "ASC";
    $arFilter = Array('IBLOCK_ID'=>'8', 'SECTION_ID'=>$arResult["SECTION"]["IBLOCK_SECTION_ID"]);
    $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true);
    while($ar_result = $db_list->GetNext()):?>
      <?if($arResult["SECTION"]["ID"]==$ar_result['ID']) continue;?>
      <a href="<?=$ar_result["SECTION_PAGE_URL"]?>" id="<?=$this->GetEditAreaId($ar_result['ID']);?>">
          <?=$ar_result["NAME"]?>
      </a>
    <?endwhile;?>
    <?endif;?>
</div>
<?endif;?>
