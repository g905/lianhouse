<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<ul id="main_slider">
    <? foreach ($arResult["ITEMS"] as $arItem): ?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
        <li id="<?= $this->GetEditAreaId($arItem['ID']); ?>">
            <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>" title="<?= $arItem["PREVIEW_PICTURE"]["ALT"] ?>"/>
            <div class="main_slide_content">
                <p class="ms_name"><?= $arItem["PREVIEW_TEXT"] ?></p>
                <? if ($arItem["PROPERTIES"]["TITLE"]["VALUE"]["TEXT"]): ?>
                    <p class="ms_smallname"><?= $arItem["PROPERTIES"]["TITLE"]["~VALUE"]["TEXT"] ?></p>
                <?endif;?>
                <? if ($arItem["PROPERTIES"]["PRICE"]["VALUE"]): ?>
                <p class="ms_price">
                    <? if ("Y" == $arItem["PROPERTIES"]["PRICE_IS_INTERVAL"]["VALUE"]): ?>��<?endif;?>
                    <span><?= $arItem["PROPERTIES"]["PRICE"]["VALUE"] ?> ���.</span>
                    <? if ("Y" == $arItem["PROPERTIES"]["PRICE_IS_MEASURE"]["VALUE"]): ?>��<?endif;?>
                </p>
                <? endif; ?>
                <? if ($arItem["PROPERTIES"]["LINK"]["VALUE"]): ?>
                    <a class="ms_order" href="<?= $arItem["PROPERTIES"]["LINK"]["VALUE"] ?>">��������</a>
                <? endif; ?>
            </div>
        </li>
    <? endforeach; ?>
</ul>