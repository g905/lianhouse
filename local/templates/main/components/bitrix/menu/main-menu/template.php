<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?if (!empty($arResult)):?>
<nav>
	<ul>
<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
	<?if($arItem["SELECTED"]):?>
		<li>
			<a href="<?=$arItem["LINK"]?>" class="selected">
				<span class="hs1_nav_icon"></span>
				<span class="hs1_nav_text">
					<?=$arItem["TEXT"]?></span>
			</a>
		</li>
	<?else:?>
		<li>
			<a href="<?=$arItem["LINK"]?>">
				<span class="hs1_nav_icon"></span>
				<span class="hs1_nav_text">
				<?=$arItem["TEXT"]?></span>
			</a>
		</li>
	<?endif?>
	
<?endforeach?>

	</ul>
</nav>
<?endif?>