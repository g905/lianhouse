<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

$arViewModeList = $arResult['VIEW_MODE_LIST'];

$arViewStyles = array(
	'LIST' => array(
		'CONT' => 'bx_sitemap',
		'TITLE' => 'bx_sitemap_title',
		'LIST' => 'smb_ul',
	),
	'LINE' => array(
		'CONT' => 'bx_catalog_line',
		'TITLE' => 'bx_catalog_line_category_title',
		'LIST' => 'bx_catalog_line_ul',
		'EMPTY_IMG' => $this->GetFolder().'/images/line-empty.png'
	),
	'TEXT' => array(
		'CONT' => 'bx_catalog_text',
		'TITLE' => 'bx_catalog_text_category_title',
		'LIST' => 'bx_catalog_text_ul'
	),
	'TILE' => array(
		'CONT' => 'bx_catalog_tile',
		'TITLE' => 'bx_catalog_tile_category_title',
		'LIST' => 'bx_catalog_tile_ul',
		'EMPTY_IMG' => $this->GetFolder().'/images/tile-empty.png'
	)
);
$arCurView = $arViewStyles[$arParams['VIEW_MODE']];

$strSectionEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_EDIT");
$strSectionDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "SECTION_DELETE");
$arSectionDeleteParams = array("CONFIRM" => GetMessage('CT_BCSL_ELEMENT_DELETE_CONFIRM'));

?><?
if ('Y' == $arParams['SHOW_PARENT_NAME'] && 0 < $arResult['SECTION']['ID'])
{
	$this->AddEditAction($arResult['SECTION']['ID'], $arResult['SECTION']['EDIT_LINK'], $strSectionEdit);
	$this->AddDeleteAction($arResult['SECTION']['ID'], $arResult['SECTION']['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

	?><h1
		class="<? echo $arCurView['TITLE']; ?>"
		id="<? echo $this->GetEditAreaId($arResult['SECTION']['ID']); ?>"
	><a href="<? echo $arResult['SECTION']['SECTION_PAGE_URL']; ?>"><?
		echo (
			isset($arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]) && $arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"] != ""
			? $arResult['SECTION']["IPROPERTY_VALUES"]["SECTION_PAGE_TITLE"]
			: $arResult['SECTION']['NAME']
		);
	?></a></h1><?
}
if (0 < $arResult["SECTIONS_COUNT"])
{
?>
<div id="sidebarwrapper"></div>
<ul class="<? echo $arCurView['LIST']; ?>">
<?
	switch ($arParams['VIEW_MODE'])
	{
		case 'LINE':
			foreach ($arResult['SECTIONS'] as &$arSection)
			{
				$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
				$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

				if (false === $arSection['PICTURE'])
					$arSection['PICTURE'] = array(
						'SRC' => $arCurView['EMPTY_IMG'],
						'ALT' => (
							'' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
							? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
							: $arSection["NAME"]
						),
						'TITLE' => (
							'' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
							? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
							: $arSection["NAME"]
						)
					);
				?><li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
				<a
					href="<? echo $arSection['SECTION_PAGE_URL']; ?>"
					class="bx_catalog_line_img"
					style="background-image: url('<? echo $arSection['PICTURE']['SRC']; ?>');"
					title="<? echo $arSection['PICTURE']['TITLE']; ?>"
				></a>
				<h2 class="bx_catalog_line_title"><a href="<? echo $arSection['SECTION_PAGE_URL']; ?>"><? echo $arSection['NAME']; ?></a><?
				if ($arParams["COUNT_ELEMENTS"])
				{
					?> <span>(<? echo $arSection['ELEMENT_CNT']; ?>)</span><?
				}
				?></h2><?
				if ('' != $arSection['DESCRIPTION'])
				{
					?><p class="bx_catalog_line_description"><? echo $arSection['DESCRIPTION']; ?></p><?
				}
				?><div style="clear: both;"></div>
				</li><?
			}
			unset($arSection);
			break;
		case 'TEXT':
			foreach ($arResult['SECTIONS'] as &$arSection)
			{
				$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
				$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

				?><li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>"><h2 class="bx_catalog_text_title"><a href="<? echo $arSection['SECTION_PAGE_URL']; ?>"><? echo $arSection['NAME']; ?></a><?
				if ($arParams["COUNT_ELEMENTS"])
				{
					?> <span>(<? echo $arSection['ELEMENT_CNT']; ?>)</span><?
				}
				?></h2></li><?
			}
			unset($arSection);
			break;
		case 'TILE':
			foreach ($arResult['SECTIONS'] as &$arSection)
			{
				$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
				$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

				if (false === $arSection['PICTURE'])
					$arSection['PICTURE'] = array(
						'SRC' => $arCurView['EMPTY_IMG'],
						'ALT' => (
							'' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
							? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_ALT"]
							: $arSection["NAME"]
						),
						'TITLE' => (
							'' != $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
							? $arSection["IPROPERTY_VALUES"]["SECTION_PICTURE_FILE_TITLE"]
							: $arSection["NAME"]
						)
					);
				?><li id="<? echo $this->GetEditAreaId($arSection['ID']); ?>">
				<a
					href="<? echo $arSection['SECTION_PAGE_URL']; ?>"
					class="bx_catalog_tile_img"
					style="background-image:url('<? echo $arSection['PICTURE']['SRC']; ?>');"
					title="<? echo $arSection['PICTURE']['TITLE']; ?>"
				> </a><?
				if ('Y' != $arParams['HIDE_SECTION_NAME'])
				{
					?><h2 class="bx_catalog_tile_title"><a href="<? echo $arSection['SECTION_PAGE_URL']; ?>"><? echo $arSection['NAME']; ?></a><?
					if ($arParams["COUNT_ELEMENTS"])
					{
						?> <span>(<? echo $arSection['ELEMENT_CNT']; ?>)</span><?
					}
				?></h2><?
				}
				?></li><?
			}
			unset($arSection);
			break;
		case 'LIST':
			$intCurrentDepth = 1;
			$boolFirst = true;
			$i = 1;
			foreach ($arResult['SECTIONS'] as &$arSection)
			{
				// var_dump ($arSection["RELATIVE_DEPTH_LEVEL"]);
				$this->AddEditAction($arSection['ID'], $arSection['EDIT_LINK'], $strSectionEdit);
				$this->AddDeleteAction($arSection['ID'], $arSection['DELETE_LINK'], $strSectionDelete, $arSectionDeleteParams);

				if ($intCurrentDepth < $arSection['RELATIVE_DEPTH_LEVEL'])
				{
					if (0 < $intCurrentDepth)
					    $ulCssClass = "smb_popup_menu smb_popup_menu_" . ($arSection["RELATIVE_DEPTH_LEVEL"] - 1);
					    if (3 < $arSection['RELATIVE_DEPTH_LEVEL']) {
                            $ulCssClass .= $arSection['RELATIVE_DEPTH_LEVEL'] - 1;
                        }
						echo "\n",str_repeat("\t", $arSection['RELATIVE_DEPTH_LEVEL']);
					    echo '<ul class="'.$ulCssClass.'">';
				}
				elseif ($intCurrentDepth == $arSection['RELATIVE_DEPTH_LEVEL'])
				{
					if (!$boolFirst)
						echo '</li>';
				}
				else
				{
					while ($intCurrentDepth > $arSection['RELATIVE_DEPTH_LEVEL'])
					{
						echo '</li>',"\n",str_repeat("\t", $intCurrentDepth),'</ul>',"\n",str_repeat("\t", $intCurrentDepth-1);
						$intCurrentDepth--;
					}
					echo str_repeat("\t", $intCurrentDepth-1),'</li>';
				}

				echo (!$boolFirst ? "\n" : ''),str_repeat("\t", $arSection['RELATIVE_DEPTH_LEVEL']);
				//$liCssClass = (1 == $arSection["RELATIVE_DEPTH_LEVEL"])
                //    ? "smb_li".$arSection["RELATIVE_DEPTH_LEVEL"]
                //    : ((2 == $arSection["RELATIVE_DEPTH_LEVEL"]) ? "smbpm_li" : "");
                $liCssClass = "smb_li".$arSection["RELATIVE_DEPTH_LEVEL"];
				$aClass = (2 == $arSection["RELATIVE_DEPTH_LEVEL"]) ? "smbpm_a" : "";
				?>

                    <li class="<?= $liCssClass?>" id="<?=$this->GetEditAreaId($arSection['ID']);?>">
                    <a class="<?= $aClass?>" href="<? echo $arSection["SECTION_PAGE_URL"]; ?>" onclick = "BX.showWait()">
                        <?if (1 == $arSection["RELATIVE_DEPTH_LEVEL"] && $arSection["UF_CSS_CLASS"]){?>
                            <i class="icon1 fa <?= $arSection["UF_CSS_CLASS"]?>" aria-hidden="true"></i>
                        <?}?>
                        <? echo $arSection["NAME"];?>
                        <?if ($arSection["HAS_CHILDREN"] != '') {?>
                            <i class="icon2 fa fa-angle-right" aria-hidden="true"></i>
                        <?}?>
                        <? if ($arParams["COUNT_ELEMENTS"]) {?> <span>(<? echo $arSection["ELEMENT_CNT"]; ?>)</span><?}
				    ?></a>
        <?	$i++;
				$intCurrentDepth = $arSection['RELATIVE_DEPTH_LEVEL'];
				$boolFirst = false;
			}
			unset($arSection);
			while ($intCurrentDepth > 1)
			{
				echo '</li>',"\n",str_repeat("\t", $intCurrentDepth),'</ul>',"\n",str_repeat("\t", $intCurrentDepth-1);
				$intCurrentDepth--;
			}
			if ($intCurrentDepth > 0)
			{
				echo '</li>',"\n";
			}
			break;
	}
?>
<!--<a href="/index.php?route=product/catalog">
					<i class="icon1 fa fa-bars" aria-hidden="true"></i>��� ������<i class="icon2 fa fa-angle-right" aria-hidden="true"></i>
				</a>-->
<li class="active">
	<a href="/catalog/">

		<div id="show_cats" style="width: 100%; text-align: center; margin-left: -10px; margin-top: 10px"><i class="icon1 fa fa-bars" aria-hidden="true"></i>&nbsp;��� ���������</div></a></li>
</ul>
<?
	echo ('LINE' != $arParams['VIEW_MODE'] ? '<div style="clear: both;"></div>' : '');
}
?>

<script>

	// console.log($("#sidebarwrapper").position().top);
	// var topmenu = $("#sidebarwrapper").position().top;
	// $(".smb_li1").each(function(){
	// 	if ($(this).position().top > topmenu+380){
	// 		$(this).css("display","none");
	// 	}
	// });
	// $("#sidebarwrapper").parent().scroll(function(){
	// 	console.log("scroll");
	// });
	// $(".sidebar_menu_body").hover(function(){
	// 	var topmenu = $("#sidebarwrapper").position().top;
  //         function addHandler(object, event, handler) {
  //         if (object.addEventListener) {
  //           object.addEventListener(event, handler, false);
  //         }
  //         else if (object.attachEvent) {
  //           object.attachEvent('on' + event, handler);
  //         }
  //         }
  //         addHandler(window, 'DOMMouseScroll', wheel);
  //         addHandler(window, 'mousewheel', wheel);
  //         addHandler(document, 'mousewheel', wheel);
  //         function wheel(event) {
  //         var delta;
  //         event = event || window.event;
  //         if (event.wheelDelta) {
  //           delta = event.wheelDelta / 120;
  //           if (window.opera) delta = -delta;
  //         }
  //         else if (event.detail) {
  //           delta = -event.detail / 3;
  //         }
  //         if (event.preventDefault) event.preventDefault();
  //         event.returnValue = false;
  //         // console.log(delta);
  //         if(delta == 1){
	// 					$("#sidebarwrapper").offset(function(i, coord){
	// 						var  newCoord = {};
	// 						newCoord.top = coord.top + 10;
	// 						return newCoord;
	// 						});
	// 						var topmenu = $("#sidebarwrapper").position().top;
	// 						console.log(topmenu);
	// 						// $(".smb_li1").each(function(){
	// 						// 	if ($(this).position().top > topmenu+420){
	// 						// 		$(this).css("display","none");
	// 						// 	}else{
	// 						// 		$(this).css("display","block");
	// 						// 	}
	// 						// });
  //           // $('.catalog_b .navigation .carousel').jcarousel('scroll', '-=1');
  //         }
  //         else{

	// 					$("#sidebarwrapper").offset(function(i, coord){
	// 						var  newCoord = {};
	// 						newCoord.top = coord.top - 10;
	// 						return newCoord;
	// 						});
	// 						var topmenu = $("#sidebarwrapper").position().top;
	// 						console.log(topmenu);
	// 						// $(".smb_li1").each(function(){
	// 						// 	if ($(this).position().top > topmenu+420){
	// 						// 		$(this).css("display","none");
	// 						// 	}else{
	// 						// 		$(this).css("display","block");
	// 						// 	}
	// 						// });
  //         }
	// 				}
	// 		}, function()
	// 		{
	// 			console.log("sdsd")
	// 			window.removeEventListener('mousewheel', this, false);
	// 			window.removeEventListener('DOMMouseScroll', this, false);
	// 			document.removeEventListener('mousewheel', this, false);
	// 			$(window).off();
	// 			$(window).off();
	// 			$(document).off();
	// 		}
	// 		);

</script>
