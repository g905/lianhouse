<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="ru"> <!--<![endif]-->
<!--TEST2 FINDME-->
<head>
<script>(function(w, c){(w[c]=w[c]||[]).push(function(){new zTracker({"id":"705636a735c7e72a8f9b34c80ec59605646","metrics":{"metrika":"46475754","ga":"UA-130968973-2"}});});})(window, "zTrackerCallbacks");</script>
<script async id="zd_ct_phone_script" src="https://my.zadarma.com/js/ct_phone.min.js"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-130968973-2"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-130968973-2');
</script>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-5HCT6HZ');</script>
<!-- End Google Tag Manager -->


	<?$APPLICATION->ShowHead()?>
	<title><?$APPLICATION->ShowTitle(false)?></title>
	<meta name="description" content="">

	<link rel="shortcut icon" href="<?= SITE_TEMPLATE_PATH?>/img/favicon/favicon.ico" type="image/x-icon">
	<link rel="apple-touch-icon" href="<?= SITE_TEMPLATE_PATH?>/img/favicon/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?= SITE_TEMPLATE_PATH?>/img/favicon/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?= SITE_TEMPLATE_PATH?>/img/favicon/apple-touch-icon-114x114.png">

	<!--link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH?>/css/main.css"-->
	<link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH?>/css/libs.min.css">

	<link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH?>/css/custom.css?v=1">

	<? CJSCore::Init(array("jquery")); ?>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">


</head>



<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5HCT6HZ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
	<div id="panel"><? $APPLICATION->ShowPanel(); ?></div>
	<!--========================== header ================================-->
    <?if (SITE_ENV == 'PROD'):?>
        <div class="loader">
            <div class="loader_inner"></div>
        </div>
    <?endif;?>

	<!-- popup forms -->
	<!--registration-->
	<div class="obertka_nsf">
		<div class="form_wrap_nsf" id="form_wrap_nsf">
			<form onsubmit="datalayer.push({'event':'registration_submit'})" method="post" action="" class="product_add_nsf">
				<div class="exit_wrap_nsf"><p class="exit_nsf">&times;</p></div>
				<p class="name_nsf">�����������</p>
				<!--<input type="hidden" name="inspection">-->
				<input class="inp_nsf" name="firstName" type="text" placeholder="���"> <br>
				<input class="inp_nsf" name="lastName" type="text" placeholder="�������">
				<input class="inp_nsf" name="phone" type="text" placeholder="����� ��������">
				<input class="inp_nsf" name="email" type="text" placeholder="E-mail">
				<input class="inp_nsf" name="password" type="password" placeholder="������">
				<input class="inp_nsf" name="confirumPassword" type="password" placeholder="������������� ������">
				<div class="bottom_block_nsf">
					<button class="sub_nsf registerBtn" type="submit">������������������</button>
					<span>��� <a href="/#" class="openLogin">������� � ������������ �������</a></span>

				</div>
				
				<p class="text_nsf">
					������� ������ ��������������������, �  ��������������� �������� ��
					���������  ���������� �� ���������� � ���������  ������, �������� ��
					��������� ���������- ��� ������ � ������������ � <a href="/#">��������� �������</a>
				</p>
				<p class="answer" id="answerRegister"></p>
			</form>
		</div>
	</div>
	<!--Login-->
	<div class="obertka_nsf3">
		<div class="form_wrap_nsf" id="form_wrap_nsf">
			<form method="post" action="" class="product_add_nsf loginForm">
				<div class="exit_wrap_nsf"><p class="exit_nsf">&times;</p></div>
				<p class="name_nsf">����</p>
				<input class="inp_nsf" name="login" type="text" placeholder="E-mail">
				<input class="inp_nsf" name="password" type="password" placeholder="������">
				<div class="bottom_block_nsf">
					<button class="sub_nsf login_btn" type="submit">�����</button>
					<span>��� <a href="/#" class="openRegister">�����������������</a></span>
					<div class="fgpwd" style="display: inline-block; font-size: 12px; float: right; margin-top: 10px"><a href="<?=$_SERVER["SITE_DIR"]?>/auth/forgot-password/pass_restore.php?forgot_password=yes">������������ ������</a></div>
				</div>
				<p class="answer" id="answerLogin"></p>
			</form>
		</div>
	</div>
	<!--Ask a Question-->
	<div class="obertka_nsf2">
		<div class="form_wrap_nsf" id="form_wrap_nsf">
			<form method="post" action="" class="product_add_nsf">
				<div class="exit_wrap_nsf"><p class="exit_nsf">&times;</p></div>
				<p class="name_nsf">������ ������</p>
				<!--<input type="hidden" name="inspection">-->
				<input class="inp_nsf" name="mail" type="text" placeholder="��� e-mail">
				<textarea name="message" placeholder="���� ���������"></textarea>
				<div class="bottom_block_nsf">
					<button class="sub_nsf support-button" type="submit">��������� ������</button>
				</div>
			</form>
		</div>
	</div>
	<div class="obertka_nsf4">
		<div class="form_wrap_nsf" id="form_wrap_nsf">
			<div class="product_add_nsf">
				<p style="text-align: center;">���������� �� �����������. ������ �� ������ ����� ��� ����� ������� � �������</p>
			</div>
		</div>
	</div>
	<!-- /popup forms -->
	<div class="hide-popup"></div>

	<div class="out_wrapper">
		<div class="content">
			<header>
				<section class="header_s1">
					<div class="workspace">
						<?/*
						<div class="hs1_block hs1_b1">
							<span class="hs1_navigator"></span>
							<span class="hs1_city">���������</span>
						</div>
						*/?>
						<div class="hs1_block hs1_b2">
							<?$APPLICATION->IncludeComponent(
								"bitrix:menu",
								"main-menu",
								Array(
									"ALLOW_MULTI_SELECT" => "N",
									"CHILD_MENU_TYPE" => "left",
									"DELAY" => "N",
									"MAX_LEVEL" => "1",
									"MENU_CACHE_GET_VARS" => array(""),
									"MENU_CACHE_TIME" => "3600",
									"MENU_CACHE_TYPE" => "A",
									"MENU_CACHE_USE_GROUPS" => "Y",
									"ROOT_MENU_TYPE" => "top",
									"USE_EXT" => "N"
								)
							);?>
						</div>

						<div class="hs1_block hs1_b3">
							<? global $USER;
								if ($USER->IsAuthorized()) {
							?>
								<a href="/personal/profile/"><span class="hs1_b_man"></span><?=$USER->GetParam("EMAIL");?></a>
								<span>/</span>
								<a href="?logout=yes">�����</a>
							<?
								} else {
							?>
								<a href="/#" class="login"><span class="hs1_b_man"></span>����</a>
								<span>/</span>
								<a class="registration">�����������</a>
							<?
								}
							?>
						</div>

					</div>
				</section>


				<!--================= burger menu ==========================-->
				<div id="burger">
				  <div class="one"></div>
				  <div class="two"></div>
				  <div class="three"></div>
				</div>
				<div id="burger_menu">
					<?/*
					<div class="hs1_block hs1_b1">
						<span class="hs1_navigator"></span>
						<span class="hs1_city">���������</span>
					</div>
					*/?>
				 	<nav>
						<ul>
							<li><a href="/#"><span class="hs1_nav_icon"></span><span class="hs1_nav_text">� ��������</span></a></li>
							<li><a href="/#"><span class="hs1_nav_icon"></span><span class="hs1_nav_text">��������</span></a></li>
							<li><a href="/#"><span class="hs1_nav_icon"></span><span class="hs1_nav_text">������</span></a></li>
							<li><a href="/#"><span class="hs1_nav_icon"></span><span class="hs1_nav_text">��������</span></a></li>
							<li><a href="/#"><span class="hs1_nav_icon"></span><span class="hs1_nav_text">FAQ</span></a></li>
						</ul>
					</nav>

					<div class="hs1_block hs1_b3">
						<?if($USER->IsAuthorized()):?>
						<a href="/personal/profile/"><span class="hs1_b_man"></span><?=$USER->GetParam("EMAIL");?></a>
						<span>/</span>
						<a href="?logout=yes">�����</a>
						<?else:?>
							<a href="/#" class="login"><span class="hs1_b_man"></span>����</a>
							<span>/</span>
							<a class="registration">�����������</a>
						<?endif;?>

					</div>

					<div class="hs2_b2_b2">
						<a href="tel:88001234567">8 800 1234567</a>
						<span>������ �� ������ ����������</span>
					</div>

				</div>
				<!--================= /burger menu ==========================-->


				<section class="header_s2">
					<div class="workspace">
						<div class="hs2_block hs2_b1">
							<a href="<?= SITE_DIR?>"><img src="<?= SITE_TEMPLATE_PATH?>/img/logo.png" alt="�������"></a>
						</div>

						<div class="hs2_block hs2_b2">
							<?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
							   "AREA_FILE_SHOW" => "file",
							   "PATH" => SITE_TEMPLATE_PATH."/banner.php",
							   "EDIT_TEMPLATE" => ""
							   ),
							   false
							);?>
							<!--<div class="hs2_b2_b1">
								<div class="hs2_b2_item">
									�����������<br>
									����� <span>15000 ���.</span>
								</div>
								<div class="hs2_b2_item">
									������ ��<br>
									������ �� <span>200%</span>
								</div>
							</div>
							<div class="hs2_b2_b2">
								<a href="tel:88001234567">8 800 1234567</a>
								<span>������ �� ������ ����������</span>
							</div>-->
						</div>

						<div  id="basket-container" class="hs2_block hs2_b3">
							<?$APPLICATION->IncludeComponent(
                                "electron:basket.small.bazarow",
                                "ajax",
                                Array(
                                    "COMPONENT_TEMPLATE" => "ajax",
                                    "PATH_TO_BASKET" => "/personal/cart",
                                    "PATH_TO_ORDER" => "/personal/cart",
                                    "SHOW_DELAY" => "N",
                                    "SHOW_NOTAVAIL" => "Y",
                                    "SHOW_SUBSCRIBE" => "Y"
                                )
                            );?>
						</div>
						<?/*
						<!--location-->
						<div class="location_popup">
							<p>��� ����� ���������?</p>
							<a class="lp_blue" href="/#">��������</a>
							<a class="lp_red" href="/#">��� �����</a>
						</div>
						*/?>
					</div>
				</section>

				<section class="header_new_menu">
					<div class="workspace">
						<?$APPLICATION->IncludeComponent(
							"bitrix:menu",
							"main-second-menu",
							Array(
								"ALLOW_MULTI_SELECT" => "N",
								"CHILD_MENU_TYPE" => "",
								"DELAY" => "N",
								"MAX_LEVEL" => "4",
								"MENU_CACHE_GET_VARS" => array(""),
								"MENU_CACHE_TIME" => "3600",
								"MENU_CACHE_TYPE" => "A",
								"MENU_CACHE_USE_GROUPS" => "Y",
								"ROOT_MENU_TYPE" => "topsecond",
								"USE_EXT" => "Y"
							)
						);?>
					</div>
				</section>

				<section class="header_s3">
					<div class="workspace">

						<div class="hs3_block hs3_b1">
							<div class="sidebar_menu">
									<div class="sidebar_m_top">
										<div class="sidebar_lines">
											<span class="sidebar_line"></span>
											<span class="sidebar_line"></span>
											<span class="sidebar_line"></span>
										</div>
										<p class="sidebar_name">������� �������</p>
									</div>
									<div class="sidebar_menu_body">
									<?$APPLICATION->IncludeComponent(
	"bitrix:catalog.section.list",
	"sidebar-catalog",
	array(
		"ADD_SECTIONS_CHAIN" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"COUNT_ELEMENTS" => "N",
		"IBLOCK_ID" => "8",
		"IBLOCK_TYPE" => "sections_elements",
		"SECTION_CODE" => "",
		"SECTION_FIELDS" => array(
			0 => "NAME",
			1 => "",
		),
		"SECTION_ID" => "",
		"SECTION_URL" => "/catalog/#SECTION_CODE_PATH#/",
		"SECTION_USER_FIELDS" => array(
			0 => "",
			1 => "UF_CSS_CLASS",
			2 => "",
		),
		"SHOW_PARENT_NAME" => "Y",
		"TOP_DEPTH" => "5",
		"VIEW_MODE" => "LIST",
		"COMPONENT_TEMPLATE" => "sidebar-catalog"
	),
	false
);?>
									</div>
							</div>
						</div>
						<div class="hs3_block hs3_b2">
							<?$APPLICATION->IncludeComponent(
	"bitrix:search.form",
	"search-form",
	array(
		"PAGE" => "#SITE_DIR#search/index.php",
		"USE_SUGGEST" => "N",
		"COMPONENT_TEMPLATE" => "search-form"
	),
	false
);?>
						</div>
						<div class="hs3_block hs3_b3 hs_b3_tar">
							<a href="<?= SITE_SERVER_NAME?>/personal/order?filter_history=Y&show_canceled=Y&show_all=Y" class="hs3_b3_status">������<br>������</a>
							<a href="/izbrannoe/" class="hs3_b3_like"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
						</div>
					</div>
				</section>
			</header> 



			<!--========================== /header ================================-->
            <? if ($APPLICATION->GetCurPage(false) !== '/' && strripos($APPLICATION->GetCurPage(false),'personal') === false): ?>
            <section class="catalog">
                <div class="workspace" style="overflow: hidden;">
                    <div class="cs_s1_left"></div>


                        <?$APPLICATION->IncludeComponent(
                            "bitrix:breadcrumb",
                            "main",
                            Array(
                                "PATH" => "",
                                "SITE_ID" => "s1",
                                "START_FROM" => "0"
                            )
                        );?>


                </div>
            </section>
            <? endif; ?>
