<?php
include_once($_SERVER['DOCUMENT_ROOT']."/bitrix/modules/wsrubi.smtp/classes/general/wsrubismtp.php");
require_once __DIR__ . "/functions.php";
require_once __DIR__ . "/defines.php";

\Bitrix\Main\EventManager::getInstance()->addEventHandler('sale', 'onSaleAdminOrderInfoBlockShow', 'onSaleAdminOrderInfoBlockShow');

function onSaleAdminOrderInfoBlockShow(\Bitrix\Main\Event $event)
{
     $order = $event->getParameter("ORDER");
     $prop_weight_ID = 9;

     $propertyCollection = $order->getPropertyCollection();//->getArray();
     $weight = $propertyCollection->getItemByOrderPropertyId($prop_weight_ID)->getValue();
     if(!$weight) $weight = "�� ����������";

    return new \Bitrix\Main\EventResult(
            \Bitrix\Main\EventResult::SUCCESS,
            array(
                array('TITLE' => '����� ���:',
                'VALUE' => $weight, 'ID' => 'param1'),
                ),
            'sale'
            );
}
