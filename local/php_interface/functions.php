<?php

function dd($d, $die = true) {
    echo '<pre>';
    print_r($d);
    echo '</pre>';

    if ($die) die();
}

function getFooterBanner() {
    $banner = array();
    CModule::IncludeModule("iblock");
    $dbRes = CIBlockElement::GetList(
        array(
            "SORT" => "ASC"
        ),
        array(
            "ACTIVE" => "Y",
            "ACTIVE_DATE" => "Y",
            "CHECK_PERMISSIONS" => "Y",
            "MIN_PERMISSIONS" => "R",
            "IBLOCK_ID" => IB_BANNER,
            "PROPERTY_IS_POPULAR_VALUE" => "Y",
        ),
        false,
        array(
            "nTopCount" => 1
        ),
        array(
            "*"
        )
    );
    if ($arRes = $dbRes->GetNextElement()) {
        $banner = $arRes->GetFields();
    }

    return $banner;
}

function getPopularProducts() {
    $arPopularsProducts = array();
    CModule::IncludeModule("iblock");
    $dbRes = CIBlockElement::GetList(
        array(
            "SORT" => "ASC"
        ),
        array(
            "ACTIVE" => "Y",
            "ACTIVE_DATE" => "Y",
            "CHECK_PERMISSIONS" => "Y",
            "MIN_PERMISSIONS" => "R",
            "IBLOCK_ID" => IB_CATALOG,
            "PROPERTY_IS_POPULAR_VALUE" => "Y",
        ),
        false,
        false,
        array(
            "*",
            "CATALOG_GROUP_".CLIENT_PRICE_ID
        )
    );
    while ($arRes = $dbRes->GetNextElement()) {
        $arProduct = $arRes->GetFields();
        $arProduct["PROPERTIES"] = $arRes->GetProperties();

        $arPopularsProducts[$arProduct["ID"]] = $arProduct;
    }

    return $arPopularsProducts;
}

function getTextCurrency($currency) {
    $result = RUB_SYMBOL;
    switch ($currency) {
        case "RUB":
            break;
    }

    return $result;
}

function getRequestCode() {
    $parseUrl = parse_url($_SERVER["REQUEST_URI"]);
    $arRequest = explode("/", $parseUrl["path"]);
    if ((count($arRequest) <= 3) && ("" === end($arRequest)))
        return "";

    return ("" === end($arRequest)) ? $arRequest[count($arRequest) - 2] : $arRequest[count($arRequest) - 1];
}

function getDaylyDiscount() {
    return CCatalogDiscount::GetList(array("VALUE" => "DESC"), array("ACTIVE" => "Y"), array("nTopCount" => 1), "*")->Fetch();
}

function getSimilarProducts($ids) {
    $arResult = array();

    $dbELs = CIBlockElement::GetList(
        array("SORT" => "ASC", "ID" => "DESC"),
        array("ID" => $ids),
        false,
        false,
        array("ID", "NAME", "PREVIEW_PICTURE", "CATALOG_GROUP_1", "PROPERTY_ARTICLE")
    );

    while ($arEL = $dbELs->GetNextElement()) {
        $f = $arEL->GetFields();
        $arResult[$f["ID"]] = $f;
        $arResult[$f["ID"]]["PROPERTIES"] = $arEL->GetProperties();
    }

    return $arResult;
}

function CustomCurrencyFormat($currency) {
    $c = RUB_SYMBOL;
    switch ($currency) {
        case "RUB":
            $c = RUB_SYMBOL;
            break;
    }

    return $c;

}

function getCatalogSortTypes() {
    return array(
        array(
            "CODE" => "POPULAR",
            "INTERNAL_CODE" => "shows",
            "NAME" => "�� ������������",
            "ORDER" => "DESC"
        ),
        array(
            "CODE" => "DATE",
            "INTERNAL_CODE" => "created",
            "NAME" => "�� ���� ����������",
            "ORDER" => "DESC"
        ),
        array(
            "CODE" => "PRICE",
            "INTERNAL_CODE" => "catalog_PRICE_".BASE_PRICE_ID,
            "NAME" => "�� ���� (�� �����������)",
            "ORDER" => "ASC"
        ),
        array(
            "CODE" => "PRICE",
            "INTERNAL_CODE" => "catalog_PRICE_".BASE_PRICE_ID,
            "NAME" => "�� ���� (�� ��������)",
            "ORDER" => "DESC"
        ),
    );
}

function defineCatalogSort() {
    $parseUri = parse_url($_SERVER["REQUEST_URI"]);
    $arQuery = array();
    $sortTypes = getCatalogSortTypes();
    $sort = array(
        "FIELD" => "shows",
        "ORDER" => "DESC",
    );
    if (!array_key_exists("query", $parseUri)) {
        return $sort;
    }

    parse_str($parseUri["query"], $arQuery);

    if (!array_key_exists("sort", $arQuery)) {
        return $sort;
    }

    foreach ($sortTypes as $type) {
        if (strtolower($type["CODE"]) == strtolower($arQuery["sort"])) {
            $sort["FIELD"] = $type["INTERNAL_CODE"];
            break;
        }
    }

    if (array_key_exists("order", $arQuery) && in_array(strtolower($arQuery["order"]), array("asc", "desc"))) {
        $sort["ORDER"] = strtoupper($arQuery["order"]);
    }

    return $sort;

}

function setUrlQuery($get_query) {
    $url .= ( isset( $_SERVER['QUERY_STRING'] ) ) ? "&$get_query" : "?$get_query";
    
    return $url;
}

//�������� ��������� �� ������� �����������
function getCategories($depthLevel) 
{

    $arFilter = Array('IBLOCK_ID' => 8, 'DEPTH_LEVEL' => $depthLevel);
    $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true, $arSelect = array("UF_*"));

    while($ar_result = $db_list->GetNext())
    {
        $count++;

        $result[] = $ar_result;

    }

    return $result;
}