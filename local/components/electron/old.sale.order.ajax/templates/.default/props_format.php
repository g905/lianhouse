<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?



if (!function_exists("showFilePropertyField"))
{
	function showFilePropertyField($name, $property_fields, $values, $max_file_size_show=50000)
	{
		$res = "";

		if (!is_array($values) || empty($values))
			$values = array(
				"n0" => 0,
			);

		if ($property_fields["MULTIPLE"] == "N")
		{
			$res = "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[0]\" id=\"".$name."[0]\"></label>";
		}
		else
		{
			$res = '
			<script type="text/javascript">
				function addControl(item)
				{
					var current_name = item.id.split("[")[0],
						current_id = item.id.split("[")[1].replace("[", "").replace("]", ""),
						next_id = parseInt(current_id) + 1;

					var newInput = document.createElement("input");
					newInput.type = "file";
					newInput.name = current_name + "[" + next_id + "]";
					newInput.id = current_name + "[" + next_id + "]";
					newInput.onchange = function() { addControl(this); };

					var br = document.createElement("br");
					var br2 = document.createElement("br");

					BX(item.id).parentNode.appendChild(br);
					BX(item.id).parentNode.appendChild(br2);
					BX(item.id).parentNode.appendChild(newInput);
				}
			</script>
			';

			$res .= "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[0]\" id=\"".$name."[0]\"></label>";
			$res .= "<br/><br/>";
			$res .= "<label for=\"\"><input type=\"file\" size=\"".$max_file_size_show."\" value=\"".$property_fields["VALUE"]."\" name=\"".$name."[1]\" id=\"".$name."[1]\" onChange=\"javascript:addControl(this);\"></label>";
		}

		return $res;
	}
}

if (!function_exists("PrintPropsForm"))
{
	function PrintPropsForm($arSource = array(), $locationTemplate = ".default")
	{
		if (!empty($arSource))
		{
			?>
				<div>
					<?
					foreach ($arSource as $arProperties)
					{
                        $rsUser = CUser::GetByID($_SESSION['SESS_AUTH']['USER_ID']);
                        $arUser = $rsUser->Fetch();
                        ?>
						<div data-property-id-row="<?=intval(intval($arProperties["ID"]))?>" class="<?=$arProperties["CODE"]?>">

						<?
						if ($arProperties["TYPE"] == "CHECKBOX")
						{
							?>
							<input type="hidden" name="<?=$arProperties["FIELD_NAME"]?>" value="">

							<div class="bx_block r1x3 pt8">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r1x3 pt8">
								<input type="checkbox" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" value="Y"<?if ($arProperties["CHECKED"]=="Y") echo " checked";?>>

								<?
								if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
								?>
								<div class="bx_description">
									<?=$arProperties["DESCRIPTION"]?>
								</div>
								<?
								endif;
								?>
							</div>

							<div style="clear: both;"></div>
							<?
						}
						elseif ($arProperties["TYPE"] == "TEXT")
						{
							?>
							<div class="wcf_body_line">
                                <p class="wcfbl_name">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span>*</span>
								<?endif;?>
                                </p>
														<?
														 //var_dump($arUser);
														?>
                            <input type="text" class="wcfb1_input" size="<?=$arProperties["SIZE1"]?>" value="<?=$arProperties["VALUE"]?>" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["DESCRIPTION"]?>" placeholder="<?=$arProperties["NAME"]?>"/>
                            </div>
                            <?
						}
						elseif ($arProperties["TYPE"] == "SELECT")
						{
							?>
							<br/>
							<div class="bx_block r1x3 pt8">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r3x1">
								<select name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" size="<?=$arProperties["SIZE1"]?>">
									<?
									foreach($arProperties["VARIANTS"] as $arVariants):
									?>
										<option value="<?=$arVariants["VALUE"]?>"<?if ($arVariants["SELECTED"] == "Y") echo " selected";?>><?=$arVariants["NAME"]?></option>
									<?
									endforeach;
									?>
								</select>

								<?
								if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
								?>
								<div class="bx_description">
									<?=$arProperties["DESCRIPTION"]?>
								</div>
								<?
								endif;
								?>
							</div>
							<div style="clear: both;"></div>
							<?
						}
						elseif ($arProperties["TYPE"] == "MULTISELECT")
						{
							?>
							<br/>
							<div class="bx_block r1x3 pt8">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r3x1">
								<select multiple name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" size="<?=$arProperties["SIZE1"]?>">
									<?
									foreach($arProperties["VARIANTS"] as $arVariants):
									?>
										<option value="<?=$arVariants["VALUE"]?>"<?if ($arVariants["SELECTED"] == "Y") echo " selected";?>><?=$arVariants["NAME"]?></option>
									<?
									endforeach;
									?>
								</select>

								<?
								if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
								?>
								<div class="bx_description">
									<?=$arProperties["DESCRIPTION"]?>
								</div>
								<?
								endif;
								?>
							</div>
							<div style="clear: both;"></div>
							<?
						}
						elseif ($arProperties["TYPE"] == "TEXTAREA")
						{
							$rows = ($arProperties["SIZE2"] > 10) ? 4 : $arProperties["SIZE2"];
							?>
							<div class="wcf_block wcf_block2 <?=$arProperties["CODE"]?>">
                                <div class="wcf_block_inner">
                                    <p class="wcf_top">
                                    <?=$arProperties["NAME"]?>
                                    <?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
                                        <span>*</span>
                                    <?endif;?>
                                    </p>

                                        <? if($arProperties["CODE"] == 'UF_ADDRESS_DELIVERY'):?>
																							<?
																							//var_dump($arUser["UF_ADDRESS_DELIVERY"]);?>
																							<div class="wcf_adress">
																								<style>
																									.wcfb1_input {

																									}
																									.wcfb1_input:not(.chbx){
																										width: 100%!important;
																									}

																									.empty_field {
																										background: red!important;
																									}
																								</style>

																							<?
																							if ($arUser["UF_ADDRESS_DELIVERY"]){
																								$deliveryaddreses = (\Bitrix\Main\Web\Json::decode($arUser["UF_ADDRESS_DELIVERY"]));
																								$i=0;

																								//echo "<pre>"; print_r($deliveryaddreses); echo "</pre>";
																								//print_r($deliveryaddreses);
																								$j=0;
																								foreach($deliveryaddreses as $deliveryaddres){

																								if($deliveryaddres["main_adress"]=="yes"){?>

									<div name="test" id="del_addr" action="" method="">
										<?if($deliveryaddres['LAST_NAME']&&$deliveryaddres['NAME']&&$deliveryaddres['MIDDLE_NAME']):?>
											<?=$deliveryaddres['LAST_NAME']?>&nbsp;<?=$deliveryaddres['NAME']?>&nbsp;<?=$deliveryaddres['MIDDLE_NAME']?></p>
										<?endif;?>
										<?if(trim($arUser["PERSONAL_MOBILE"])):?>
											<p class="wcfa_tel"><?=$arUser["PERSONAL_MOBILE"]?></p>
										<?endif;?>
									<div class="wcf_body_line">
										<input class="wcfb1_input" name="NAME" value="<?=$deliveryaddres['NAME']?>" type="hidden">
									</div>
									<div class="wcf_body_line">
										<input class="wcfb1_input" name="LAST_NAME" value="<?=$deliveryaddres['LAST_NAME']?>" type="hidden">
									</div>
									<div class="wcf_body_line">
										<input class="wcfb1_input" name="MIDDLE_NAME" value="<?=$deliveryaddres['MIDDLE_NAME']?>" type="hidden">
									</div>
									<div class="wcf_body_line">
										<input class="wcfb1_input" name="COUNTRY" value="<?=$deliveryaddres['COUNTRY']?>" type="text" placeholder="������">
									</div>
									<div class="wcf_body_line">
										<input class="wcfb1_input" name="AREA" value="<?=$deliveryaddres['AREA']?>" type="text" placeholder="�������">
									</div>
									<div class="wcf_body_line">
										<input class="wcfb1_input" name="CITY" value="<?=$deliveryaddres['CITY']?>" type="text" placeholder="�����">
									</div>
									<div class="wcf_body_line">
										<input class="wcfb1_input" name="POSTCODE" value="<?=$deliveryaddres['POSTCODE']?>" type="text" placeholder="������">
									</div>
									<div class="wcf_body_line" style="margin-bottom: 10px">
										<input class="wcfb1_input" name="ADDRESS" value="<?=$deliveryaddres['ADDRESS']?>" type="text" placeholder="�����">
									</div>

									<input id="main_adress" class="wcfb1_input" name="main_adress" type="hidden" value="yes">
									<input type="hidden" name="USERID" value="<?=$arUser['ID']?>">
									<input type="hidden" id="CHADDRID" name="CHADDRID" class="CHADDRID" value="<?=$j?>">
									<div class="par_submit" style="display: inline-block">
										<button class="button2" type="submit">���������</button>
									</div>
									<div style="display: inline-block; width: 50%; text-align: right"><a class="changeaddr" href="/personal/address/">������</a></div>
								</div>
								<?
								$dellcurraddr = $deliveryaddres["LAST_NAME"]." ".$deliveryaddres["NAME"]." ".$deliveryaddres["MIDDLE_NAME"]." ".$deliveryaddres["ADDRESS"]." ".$deliveryaddres["COUNTRY"]." ".$deliveryaddres["AREA"]." ".$deliveryaddres["CITY"]." ".$deliveryaddres["POSTCODE"]." ".$arUser["PERSONAL_MOBILE"];
																									}
																									else{
																										?>
																										<!-- <div class="wcfa_wrap_a">
																											<a class="changeaddr blue_but" href="/personal/address/"><?= getMessage('PERSONAL__ADDRESSES_CHANGE')?></a>
																											<!-- <a class="deladdr red_but" href="javascript:void(null);"><?= getMessage('PERSONAL__ADDRESSES_DEL')?></a> -->
																											<div class="clear"></div>
																										<!-- </div> -->
																										<?
																									}
																									$j++;
																								}
																							} else { ?>

																								<div name="test" id="del_addr" action="" method="">
																									<p class="wcfa_name">

																										<?if($deliveryaddres['LAST_NAME']&&$deliveryaddres['NAME']&&$deliveryaddres['MIDDLE_NAME']):?>
																											<?=$deliveryaddres['LAST_NAME']?>&nbsp;<?=$deliveryaddres['NAME']?>&nbsp;<?=$deliveryaddres['MIDDLE_NAME']?></p>
																										<?endif;?>
																										<?if(trim($arUser["PERSONAL_MOBILE"])):?>
																											<p class="wcfa_tel"><?=$arUser["PERSONAL_MOBILE"]?></p>
																										<?endif;?>
																									<p class="wcfa_location" style="display: none"><?=$deliveryaddres["ADDRESS"]." ".$deliveryaddres["COUNTRY"]." ".$deliveryaddres["AREA"]." ".$deliveryaddres["CITY"]." ".$deliveryaddres["POSTCODE"]?>
																																								</p>
																								<div class="wcf_body_line">
																									<input class="wcfb1_input" name="NAME" value="<?=$deliveryaddres['NAME']?>" type="hidden">
																								</div>
																								<div class="wcf_body_line">
																									<input class="wcfb1_input" name="LAST_NAME" value="<?=$deliveryaddres['LAST_NAME']?>" type="hidden">
																								</div>
																								<div class="wcf_body_line">
																									<input class="wcfb1_input" name="MIDDLE_NAME" value="<?=$deliveryaddres['MIDDLE_NAME']?>" type="hidden">
																								</div>
																								<div class="wcf_body_line">
																									<input class="wcfb1_input" name="COUNTRY" value="<?=$deliveryaddres['COUNTRY']?>" type="text" placeholder="������">
																								</div>
																								<div class="wcf_body_line">
																									<input class="wcfb1_input" name="AREA" value="<?=$deliveryaddres['AREA']?>" type="text" placeholder="�������">
																								</div>
																								<div class="wcf_body_line">
																									<input class="wcfb1_input" name="CITY" value="<?=$deliveryaddres['CITY']?>" type="text" placeholder="�����">
																								</div>
																								<div class="wcf_body_line">
																									<input class="wcfb1_input" name="POSTCODE" value="<?=$deliveryaddres['POSTCODE']?>" type="text" placeholder="������">
																								</div>
																								<div class="wcf_body_line" style="margin-bottom: 10px">
																									<input class="wcfb1_input" name="ADDRESS" value="<?=$deliveryaddres['ADDRESS']?>" type="text" placeholder="�����">
																								</div>

																								<input id="main_adress" class="wcfb1_input" name="main_adress" type="hidden" value="yes">
																								<input type="hidden" name="USERID" value="<?=$arUser['ID']?>">
																								<input type="hidden" id="CHADDRID" name="CHADDRID" class="CHADDRID" value="<?=$j?>">
																								<div class="par_submit" style="display: inline-block">
																									<button class="button2" type="submit">���������</button>
																								</div>
																								<div style="display: inline-block; width: 50%; text-align: right"><a class="changeaddr" href="/personal/address/">������</a></div>
																							</div>

																					<?
																					$dellcurraddr = $deliveryaddres["LAST_NAME"]." ".$deliveryaddres["NAME"]." ".$deliveryaddres["MIDDLE_NAME"]." ".$deliveryaddres["ADDRESS"]." ".$deliveryaddres["COUNTRY"]." ".$deliveryaddres["AREA"]." ".$deliveryaddres["CITY"]." ".$deliveryaddres["POSTCODE"]." ".$arUser["PERSONAL_MOBILE"];

																						}
																							?>
																								</div>
																							<??>
																							<textarea style="display:none" rows="<?=$rows?>" cols="<?=$arProperties["SIZE1"]?>" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" placeholder="<?=$arProperties["DESCRIPTION"]?>"><?=$dellcurraddr?></textarea>
                                        <?else:?>
                                            <textarea rows="<?=$rows?>" cols="<?=$arProperties["SIZE1"]?>" name="<?=$arProperties["FIELD_NAME"]?>" id="<?=$arProperties["FIELD_NAME"]?>" placeholder="<?=$arProperties["DESCRIPTION"]?>"></textarea>
                                        <?endif;?>

                                </div>
                            </div>
							<?
						}
						elseif ($arProperties["TYPE"] == "LOCATION")
						{
							?>
							<div class="bx_block r1x3 pt8">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r3x1">

								<?
								$value = 0;
								if (is_array($arProperties["VARIANTS"]) && count($arProperties["VARIANTS"]) > 0)
								{
									foreach ($arProperties["VARIANTS"] as $arVariant)
									{
										if ($arVariant["SELECTED"] == "Y")
										{
											$value = $arVariant["ID"];
											break;
										}
									}
								}

								// here we can get '' or 'popup'
								// map them, if needed
								if(CSaleLocation::isLocationProMigrated())
								{
									$locationTemplateP = $locationTemplate == 'popup' ? 'search' : 'steps';
									$locationTemplateP = $_REQUEST['PERMANENT_MODE_STEPS'] == 1 ? 'steps' : $locationTemplateP; // force to "steps"
								}
								?>

								<?if($locationTemplateP == 'steps'):?>
									<input type="hidden" id="LOCATION_ALT_PROP_DISPLAY_MANUAL[<?=intval($arProperties["ID"])?>]" name="LOCATION_ALT_PROP_DISPLAY_MANUAL[<?=intval($arProperties["ID"])?>]" value="<?=($_REQUEST['LOCATION_ALT_PROP_DISPLAY_MANUAL'][intval($arProperties["ID"])] ? '1' : '0')?>" />
								<?endif?>

								<?CSaleLocation::proxySaleAjaxLocationsComponent(array(
									"AJAX_CALL" => "N",
									"COUNTRY_INPUT_NAME" => "COUNTRY",
									"REGION_INPUT_NAME" => "REGION",
									"CITY_INPUT_NAME" => $arProperties["FIELD_NAME"],
									"CITY_OUT_LOCATION" => "Y",
									"LOCATION_VALUE" => $value,
									"ORDER_PROPS_ID" => $arProperties["ID"],
									"ONCITYCHANGE" => ($arProperties["IS_LOCATION"] == "Y" || $arProperties["IS_LOCATION4TAX"] == "Y") ? "submitForm()" : "",
									"SIZE1" => $arProperties["SIZE1"],
								),
								array(
									"ID" => $value,
									"CODE" => "",
									"SHOW_DEFAULT_LOCATIONS" => "Y",

									// function called on each location change caused by user or by program
									// it may be replaced with global component dispatch mechanism coming soon
									"JS_CALLBACK" => "submitFormProxy",

									// function window.BX.locationsDeferred['X'] will be created and lately called on each form re-draw.
									// it may be removed when sale.order.ajax will use real ajax form posting with BX.ProcessHTML() and other stuff instead of just simple iframe transfer
									"JS_CONTROL_DEFERRED_INIT" => intval($arProperties["ID"]),

									// an instance of this control will be placed to window.BX.locationSelectors['X'] and lately will be available from everywhere
									// it may be replaced with global component dispatch mechanism coming soon
									"JS_CONTROL_GLOBAL_ID" => intval($arProperties["ID"]),

									"DISABLE_KEYBOARD_INPUT" => "Y",
									"PRECACHE_LAST_LEVEL" => "Y",
									"PRESELECT_TREE_TRUNK" => "Y",
									"SUPPRESS_ERRORS" => "Y"
								),
								$locationTemplateP,
								true,
								'location-block-wrapper'
								)?>

								<?
								if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
								?>
								<div class="bx_description">
									<?=$arProperties["DESCRIPTION"]?>
								</div>
								<?
								endif;
								?>

							</div>
							<div style="clear: both;"></div>
							<?
						}
						elseif ($arProperties["TYPE"] == "RADIO")
						{
							?>
							<div class="bx_block r1x3 pt8">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r3x1">
								<?
								if (is_array($arProperties["VARIANTS"]))
								{
									foreach($arProperties["VARIANTS"] as $arVariants):
									?>
										<input
											type="radio"
											name="<?=$arProperties["FIELD_NAME"]?>"
											id="<?=$arProperties["FIELD_NAME"]?>_<?=$arVariants["VALUE"]?>"
											value="<?=$arVariants["VALUE"]?>" <?if($arVariants["CHECKED"] == "Y") echo " checked";?> />

										<label for="<?=$arProperties["FIELD_NAME"]?>_<?=$arVariants["VALUE"]?>"><?=$arVariants["NAME"]?></label></br>
									<?
									endforeach;
								}
								?>

								<?
								if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
								?>
								<div class="bx_description">
									<?=$arProperties["DESCRIPTION"]?>
								</div>
								<?
								endif;
								?>
							</div>
							<div style="clear: both;"></div>
							<?
						}
						elseif ($arProperties["TYPE"] == "FILE")
						{
							?>
							<br/>
							<div class="bx_block r1x3 pt8">
								<?=$arProperties["NAME"]?>
								<?if ($arProperties["REQUIED_FORMATED"]=="Y"):?>
									<span class="bx_sof_req">*</span>
								<?endif;?>
							</div>

							<div class="bx_block r3x1">
								<?=showFilePropertyField("ORDER_PROP_".$arProperties["ID"], $arProperties, $arProperties["VALUE"], $arProperties["SIZE1"])?>

								<?
								if (strlen(trim($arProperties["DESCRIPTION"])) > 0):
								?>
								<div class="bx_description">
									<?=$arProperties["DESCRIPTION"]?>
								</div>
								<?
								endif;
								?>
							</div>

							<div style="clear: both;"></div><br/>
							<?
						}
						?>
						</div>

						<?if(CSaleLocation::isLocationProEnabled()):?>

							<?
							$propertyAttributes = array(
								'type' => $arProperties["TYPE"],
								'valueSource' => $arProperties['SOURCE'] == 'DEFAULT' ? 'default' : 'form' // value taken from property DEFAULT_VALUE or it`s a user-typed value?
							);

							if(intval($arProperties['IS_ALTERNATE_LOCATION_FOR']))
								$propertyAttributes['isAltLocationFor'] = intval($arProperties['IS_ALTERNATE_LOCATION_FOR']);

							if(intval($arProperties['CAN_HAVE_ALTERNATE_LOCATION']))
								$propertyAttributes['altLocationPropId'] = intval($arProperties['CAN_HAVE_ALTERNATE_LOCATION']);

							if($arProperties['IS_ZIP'] == 'Y')
								$propertyAttributes['isZip'] = true;
							?>

							<script>

								<?// add property info to have client-side control on it?>
								(window.top.BX || BX).saleOrderAjax.addPropertyDesc(<?=CUtil::PhpToJSObject(array(
									'id' => intval($arProperties["ID"]),
									'attributes' => $propertyAttributes
								))?>);

							</script>
						<?endif?>

						<?
					}
					?>
				</div>
			<?
		}
	}
}
?>

<script>
	$(function(){
		console.log($("#del_addr").length);
	});

	function call(elem) {
		var e=elem.find('select, input, textarea').serialize();
		//alert(e);
     	  var msg   = e;//(elem).serialize();
            $.ajax({
              type: 'POST',
              url: '/personal/address/ipost.php',
              data: msg,
              success: function(data) {
								 location.reload();
								console.log(data)
              },
              error:  function(xhr, str){
    	    alert('�������� ������: ' + xhr.responseCode);

           }
				});
      }

	$('.wcfb1_input').click(function(){
			$(this).removeClass('empty_field');
		});

	$('.button2').click(function(event){

		event.preventDefault();
		/*var a=$('.wcf_adress').find('.wcfb1_input');
		a.each(function(){
			console.log($(this).attr('name'));
		});*/

			var checkedradio = ($("input:radio:checked").length);
			var checkedcheckbox = ($("input:checkbox:checked").length);
			var chk1 = 0;
			$('.wcf_adress').find('.wcfb1_input').each(function(){
				if($(this).val() == ''){
			$(this).addClass('empty_field');
					} else {
			$(this).removeClass('empty_field');
			chk1 ++;
					}

			if(($(this).is(':radio'))&(checkedradio != 0)){
			$(this).parent().removeClass('empty_field');
					}
			if(($(this).is(':checkbox'))&(checkedcheckbox != 0)){
			$(this).parent().removeClass('empty_field');
					}
			});

		if ((chk1 <= 9) & (chk1 >= 6) & (checkedradio != 0)){
			call($('#del_addr'));
			}
	})

</script>
