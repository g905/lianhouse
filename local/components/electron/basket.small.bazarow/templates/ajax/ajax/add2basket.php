<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog")) {

   if (isset($_POST['id']) && isset($_POST['QUANTITY'])) {
      $PRODUCT_ID = intval($_POST['id']);
      $QUANTITY = intval($_POST['QUANTITY']);
      $PRICE = (float)($_POST['price']);
      if(isset($_POST['comment']))
      {
        $COMMENT = mb_convert_encoding($_POST['comment'], "Windows-1251");
      }
      if(isset($_POST['color_model'])) $color = mb_convert_encoding($_POST['color_model'], "Windows-1251");
      if(isset($_POST['size_model'])) $size = mb_convert_encoding($_POST['size_model'], "Windows-1251");
      if(isset($_POST['min_q'])) $min_q = $_POST['min_q'];

      /*Add2BasketByProductID($PRODUCT_ID, $QUANTITY, array(
            array("NAME"=>"������", "CODE"=>"SIZE", "VALUE"=>$size),
            array("NAME"=>"����", "CODE"=>"COLOR", "VALUE"=>$color),
            array("NAME" => "�����������", "CODE" => "COMMENT", "VALUE"=>$COMMENT),
            array("NAME" => "����", "CODE" => "PRICE", "VALUE"=>$PRICE),
            array("CUSTOM_PRICE"=>"Y")),false );*/
        Add2BasketByProductID($PRODUCT_ID, $QUANTITY, array(
          'PRICE'=>$PRICE,
          'CUSTOM_PRICE'=>"Y"
        ), array(
          array("NAME"=>"����", "CODE"=>"PRICE", "VALUE"=>$PRICE),
          array("NAME"=>"������", "CODE"=>"SIZE", "VALUE"=>$size),
          array("NAME"=>"����", "CODE"=>"COLOR", "VALUE"=>$color),
          array("NAME" => "�����������", "CODE" => "COMMENT", "VALUE"=>$COMMENT),
          array("NAME" => "����������� ����������", "CODE" => "MIN_QUANTITY", "VALUE" => $min_q)));

//echo "sadf";
            //print_r($_POST);
   }
   else { echo "��� ����������";  }
 }
else { echo "�� ���������� ������"; }

$APPLICATION->IncludeComponent(
    "electron:basket.small.bazarow",
    "ajax",
    Array(
        "COMPONENT_TEMPLATE" => "ajax",
        "PATH_TO_BASKET" => "/personal/cart",
        "PATH_TO_ORDER" => "/personal/cart",
        "SHOW_DELAY" => "N",
        "SHOW_NOTAVAIL" => "Y",
        "SHOW_SUBSCRIBE" => "Y"
    )
);

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");
?>
