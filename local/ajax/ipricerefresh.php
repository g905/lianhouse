<?
if($_SERVER['REQUEST_METHOD'] == 'POST') {
  require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
  IncludeTemplateLangFile(__FILE__);
  CModule::IncludeModule("iblock");
  CModule::IncludeModule("catalog");
  require($_SERVER["DOCUMENT_ROOT"].'/parser/functions.php');
  require($_SERVER["DOCUMENT_ROOT"].'/parser/config.php');

  $externalCategoryID = $_POST["categoryID"];

  $extproductItems = 'http://otapi.net/OtapiWebService2.asmx/GetItemFullInfo?instanceKey=' . CFG_SERVICE_INSTANCEKEY
  . '&language=' . CFG_REQUEST_LANGUAGE
  . '&categoryId=' . $_POST["categoryID"]
  . '&itemId=' . $_POST["extprodID"];
    //file_put_contents("log.txt", $extproductItems);
  $extproductItemsInfo = pars($extproductItems)->OtapiItemFullInfo;

  if($ranges = $extproductItemsInfo->QuantityRanges){
    $arRanges = array();
    foreach($ranges->Range as $k=>$range){
      //$nm = "RANGE".$k+1;
      $r = array(
        "quantity" => (int)$range->MinQuantity,
        "price" => (real)$range->Price->ConvertedPriceWithoutSign,
        "price_usd" => (real)$range->Price->ConvertedPriceList->DisplayedMoneys->Money[2]
      );

      $rr = $range->Price->ConvertedPriceList->DisplayedMoneys->Money[2];

      array_push($arRanges, $r);
    }
  } else {
  	$masterPrice = $extproductItemsInfo->Price->ConvertedPriceWithoutSign;
    $masterPriceUsd = $extproductItemsInfo->Price->ConvertedPriceList->DisplayedMoneys->Money[2];
  }

  file_put_contents($_SERVER["DOCUMENT_ROOT"]."/qlog.txt", print_r($arRanges, true), FILE_APPEND);

  $PRICE_TYPE_ID = 3;
  $PRICE_TYPE_ID_USD = 2;
  $PRODUCT_ID = $_POST["id"];

  if(count($arRanges)>0){
  foreach($arRanges as $k=>$range_item){
    if($k+1==count($arRanges)){
      $arFields[] = Array(
        "PRODUCT_ID" => $PRODUCT_ID,
        "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
        "PRICE" => $range_item["price"],
        "CURRENCY" => "RUB",
        "QUANTITY_FROM" => $range_item["quantity"],
        "QUANTITY_TO" => "INF"
      );
      $arFieldsUsd[] = Array(
        "PRODUCT_ID" => $PRODUCT_ID,
        "CATALOG_GROUP_ID" => $PRICE_TYPE_ID_USD,
        "PRICE" => $range_item["price_usd"],
        "CURRENCY" => "USD",
        "QUANTITY_FROM" => $range_item["quantity"],
        "QUANTITY_TO" => "INF"
      );
    } else {
      $arFields[] = Array(
        "PRODUCT_ID" => $PRODUCT_ID,
        "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
        "PRICE" => $range_item["price"],
        "CURRENCY" => "RUB",
        "QUANTITY_FROM" => $range_item["quantity"],
        "QUANTITY_TO" => $arRanges[$k+1]["quantity"]-1,
      );
      $arFieldsUsd[] = Array(
        "PRODUCT_ID" => $PRODUCT_ID,
        "CATALOG_GROUP_ID" => $PRICE_TYPE_ID_USD,
        "PRICE" => $range_item["price_usd"],
        "CURRENCY" => "USD",
        "QUANTITY_FROM" => $range_item["quantity"],
        "QUANTITY_TO" => $arRanges[$k+1]["quantity"]-1,
      );
    }
  }
  } else {
  $arFields[] = Array(
    "PRODUCT_ID" => $PRODUCT_ID,
    "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
    "PRICE" => $masterPrice,
    "CURRENCY" => "RUB",

  );
  $arFieldsUsd[] = Array(
    "PRODUCT_ID" => $PRODUCT_ID,
    "CATALOG_GROUP_ID" => $PRICE_TYPE_ID_USD,
    "PRICE" => $masterPriceUsd,
    "CURRENCY" => "USD",

  );
  }

  $res = CPrice::GetList(
    array(),
    array(
      "PRODUCT_ID" => $PRODUCT_ID,
      "CATALOG_GROUP_ID" => $PRICE_TYPE_ID
    )
  );

  if ($arr = $res->Fetch())
  {
      foreach($arFields as $k=>$arField){
        CPrice::Update($arr["ID"], $arField);
      }

  } else
  {
      foreach($arFields as $arField){
        CPrice::Add($arField);
      }
  }

  $res_usd = CPrice::GetList(
    array(),
    array(
      "PRODUCT_ID" => $PRODUCT_ID,
      "CATALOG_GROUP_ID" => $PRICE_TYPE_ID_USD
    )
  );

  if ($arr_usd = $res_usd->Fetch())
  {
      foreach($arFieldsUsd as $k=>$arField){
        CPrice::Update($arr_usd["ID"], $arField);
      }

  } else
  {
      foreach($arFieldsUsd as $arField){
        CPrice::Add($arField);
      }
  }
}
?>
