<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

global $USER;
if (!is_object($USER)) $USER = new CUser;
$arAuthResult = $USER->Login($_POST["login"], $_POST["password"], "Y");
if (is_array($arAuthResult)) {
	echo json_encode(false);
} else {
	echo json_encode($arAuthResult);
}
