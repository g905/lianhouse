<?php
// var_dump ($_POST);

//$postid = "DOPFILE".($_POST["POSTID"]);
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
    IncludeTemplateLangFile(__FILE__);
    CModule::IncludeModule("iblock");
    CModule::IncludeModule("catalog");
    require($_SERVER["DOCUMENT_ROOT"].'/parser/functions.php');
    require($_SERVER["DOCUMENT_ROOT"].'/parser/config.php');
    $externalCategoryID = $_POST["categoryID"];

    $extproductItems = 'http://otapi.net/OtapiWebService2.asmx/GetItemFullInfo?instanceKey=' . CFG_SERVICE_INSTANCEKEY
. '&language=' . CFG_REQUEST_LANGUAGE
. '&categoryId=' . $externalCategoryID
. '&itemId=' . $_POST["extprodID"];
    $extproductItemsInfo = pars($extproductItems, $_SERVER)->OtapiItemFullInfo;

    $confItems = array();
    foreach ($extproductItemsInfo->ConfiguredItems->OtapiConfiguredItem as $k=>$confItem) {
        $d[$k] = array(
      "configurators" => array(),//$confItem->Configurators,
      "prices"=> array(),
      "id"=>(string)$confItem->Id
    );
        foreach ($confItem->Configurators->ValuedConfigurator as $co) {
            $c = array(
        "Pid" => $co->attributes()["Pid"],
        //"Vid" => htmlentities(mb_convert_encoding($co->attributes()["Vid"], 'UTF-8', 'auto')),
        //"Vid" => iconv("Big5", "CP1251", $co->attributes()["Vid"]),
        "Vid" => htmlentities($co->attributes()["Vid"]),
        //"Vid" => htmlentities($co->attributes()["Vid"], ENT_QUOTES, 'CP1251'),

        //"Col" => $extproductItemsInfo->Attributes->ItemAttribute[$k]
      );
            foreach ($extproductItemsInfo->Attributes->ItemAttribute as $atr) {
                if ((string)$atr->attributes()["Pid"]==(string)$co->attributes()["Pid"]) {
                    if ($atr->ImageUrl) {
                        $c["pic"] = 'true';
                    } else {
                        $c["pic"] = 'false';
                    }
                    $c["value"] = iconv("UTF-8", "CP1251", (string)$atr->Value);
                }
            }
            //if($extproductItemsInfo->Attributes->ItemAttribute[$k])
            array_push($d[$k]["configurators"], $c);
        }
        if ($confItem->QuantityRanges):
      foreach ($confItem->QuantityRanges->Range as $range) {
          $a = array(
          "quant"=>(int)$range->MinQuantity,
          "price"=>(float)$range->Price->ConvertedPriceWithoutSign,
          "price_usd"=>(float)$range->Price->ConvertedPriceList->DisplayedMoneys->Money[2]
        );
          array_push($d[$k]["prices"], $a);
      } else:
      $a = array("master_price" => (string)$confItem->Price->ConvertedPriceWithoutSign, "master_price_usd" => (string)$confItem->Price->ConvertedPriceList->DisplayedMoneys->Money[2]);
        array_push($d[$k]["prices"], $a);
        endif;
        array_push($confItems, $d);
    };
    //file_put_contents("log.txt", print_r($confItems, true));
    $confs = array();
    foreach ($extproductItemsInfo->Attributes->ItemAttribute as $k=>$at) {
        $f = $at->IsConfigurator;
        if ((string)$f == 'true') {
            $confs[] = array(
        "id"=>(string)$at->attributes()['Pid'],
        "propname"=>(string)$at->PropertyName,
        "val"=>(string)$at->attributes()['Vid'],
      );
        }
    };

    $string = \Bitrix\Main\Web\Json::encode($confItems);
    //file_put_contents("log.txt", $string);




    $detailPicture = CFile::MakeFileArray($_SERVER["DOCUMENT_ROOT"]."/img/det_pict.jpg");

    $obElement = new CIBlockElement();

    $props = array();
    $props["ARTICLE"] = $_POST["extprodID"];
    $MorePhotoLinks = $extproductItemsInfo->Pictures;


    //���� � ������ ���� ��������� ����������, �� ������� ���������
    if ($ranges = $extproductItemsInfo->QuantityRanges) {
        $arRanges = array();
        foreach ($ranges->Range as $k=>$range) {
            //$nm = "RANGE".$k+1;
            $r = array(
      "quantity" => (int)$range->MinQuantity,
      "price" => (real)$range->Price->ConvertedPriceWithoutSign,
      "price_usd" => (real)$range->Price->ConvertedPriceList->DisplayedMoneys->Money[2]
    );

            $rr = $range->Price->ConvertedPriceList->DisplayedMoneys->Money[2];

            array_push($arRanges, $r);
        }
    } else {
        //���� ���������� ��� - ������� ������� ����
        $masterPrice = $extproductItemsInfo->Price->ConvertedPriceWithoutSign;
        $masterPriceUsd = $extproductItemsInfo->Price->ConvertedPriceList->DisplayedMoneys->Money[2];
    }

    //file_put_contents($_SERVER["DOCUMENT_ROOT"]."/qlog.txt", print_r($arRanges, true));
    $arMorePhotoLinks = array(
    (string)$MorePhotoLinks->ItemPicture[0]->Url,
    (string)$MorePhotoLinks->ItemPicture[1]->Url,
    (string)$MorePhotoLinks->ItemPicture[2]->Url
);
    $props["MORE_PHOTO_LINK"] = $arMorePhotoLinks;

    $arCHARACTERISTICS = $extproductItemsInfo->Attributes;

    //�������� ������ �������
    $arNew = array();
    //file_put_contents('./log.txt', print_r($arCHARACTERISTICS, true));
    foreach ($arCHARACTERISTICS->ItemAttribute as $character) {
        if ((string)$character->IsConfigurator == "true") {
            if ((string)$character->ImageUrl != '') {
                $arC = CFile::MakeFileArray((string)$character->ImageUrl);
                $cid = CFile::SaveFile($arC, '/');
                //$arNew["color"][] = array("PropertyName"=>htmlentities((string)$character->PropertyName), "VALUE"=>htmlentities((string)$character->Value), "IMG"=>$cid, "Pid"=>(int)$character->attributes()['Pid'], "Vid"=>htmlentities((string)$character->attributes()['Vid']));
                //$arNew[htmlentities((string)$character->PropertyName)][] = array("VALUE"=>(string)$character->Value, "IMG"=>$cid, "Pid"=>(int)$character->attributes()['Pid'], "Vid"=>htmlentities((string)$character->attributes()['Vid']));
                $arNew[
        mb_convert_encoding((string)$character->PropertyName, "cp1251")][] = array(
          "VALUE"=>mb_convert_encoding((string)$character->Value, "cp1251"),
          "IMG"=>$cid,
          "Pid"=>(int)$character->attributes()['Pid'],
          "Vid" => htmlentities((string)$character->attributes()['Vid'])
        );
            }
            //if((string)$character->attributes()['Pid'] == '450'||(string)$character->attributes()['Pid'] == '100019113'||(string)$character->attributes()['Pid'] == '449'||(string)$character->attributes()['Pid'] == '2340'||(string)$character->attributes()['Pid'] == '7853'){
            else {
                $arNew[
      mb_convert_encoding((string)$character->PropertyName, "cp1251")][] = array(
        "VALUE"=>mb_convert_encoding((string)$character->Value, "cp1251"),
        "Pid"=>(int)$character->attributes()['Pid'],
        "Vid" => htmlentities((string)$character->attributes()['Vid'])
      );
            }
        } else {
            $archar[] = array("VALUE"=>iconv("UTF-8", "WINDOWS-1251", (string)$character->PropertyName),"DESCRIPTION"=>iconv("UTF-8", "WINDOWS-1251", (string)$character->Value));
        }
    }
    $arNew = \Bitrix\Main\Web\Json::encode($arNew, $options=null);

    //file_put_contents("log.txt", $arNew);

    $link = "https://detail.1688.com/offer/".substr($extproductItemsInfo->Id, 4).".html";

    $arclrs = [];
    foreach ($archar['color'] as $imgsrc) {
        if ($imgsrc["DESCRIPTION"]!=="") {
            $arCOLOR = CFile::MakeFileArray((string)$imgsrc['DESCRIPTION']);

            $color_id = CFile::SaveFile($arCOLOR, '/');

            $arclrs[] = array("VALUE"=>$color_id, "DESCRIPTION"=> $imgsrc['VALUE']);
        } else {
            $arclrs[] = array("VALUE"=>"empty", "DESCRIPTION"=>$imgsrc['VALUE']);
        }
    };
    //$d = print_r($arclrs, true);
    //file_put_contents('./log.txt', print_r($arclrs, true));
    $props["CHARACTERISTICS"] = $archar;
    $props["AVAILABLE_Q"] = $extproductItemsInfo->MasterQuantity;
    $props["MIN_QUANTITY"] = $extproductItemsInfo->FirstLotQuantity;
    $props["NEXT_LOT_QUANTITY"] = $extproductItemsInfo->NextLotQuantity;
    $props["MAIN_PHOTO"] = $extproductItemsInfo->MainPictureUrl;
    $props["VENDOR_ID"] = $extproductItemsInfo->VendorId;
    $props["VENDOR_NAME"] = $extproductItemsInfo->VendorName;
    $props["COLOR"] = $arclrs;//$archar["color"];
    $props["SIZE"] = $archar["size"];
    $props["LINK_1688"] = $link;
    $props["SERIAL"] = $string; //������ � �������������� "�������� �����������". �������� json_decode � �������� ������.
$props["VARIATIONS"] = $arNew; //������ � ���������� "�������� �����������".
// echo "<pre>";
// var_dump ($props);
// echo "</pre>";

$DETAILPICTURE = CFile::MakeFileArray($extproductItemsInfo->MainPictureUrl);

    $arFilter = array("IBLOCK_ID"=>(string)$_POST["iblockID"], "UF_ID_CATEGORY"=>(string)$_POST["categoryID"]);
    $section_list = CIBlockSection::GetList(array("ID"=>"ASC"), $arFilter, false);
    //$a=$section_list->GetNext();
    //file_put_contents('log.txt', "CATE ".print_r('test', true));
    if ($a=$section_list->GetNext()) {
        $sect = (string)$a['ID'];//(string)$_POST["categoryID"];
    } else {
        $sect = '5318'; //��������� '���������'
    };
    //file_put_contents('log.txt', $sect);
    $arFields = array(
    "IBLOCK_ID"          => $_POST["iblockID"],
    "IBLOCK_SECTION_ID"  => $sect,//$_POST["categoryID"],
    "NAME"               => iconv("UTF-8", "CP1251", (string)$extproductItemsInfo->Title),
    "CODE"               => $_POST["extprodID"],
    "ACTIVE"             => "Y",
    "DETAIL_TEXT_TYPE"   => "html",
    "DETAIL_TEXT"        => $text,
    "PREVIEW_PICTURE"    => $DETAILPICTURE,
    "DETAIL_PICTURE"     => $DETAILPICTURE,
    "PROPERTY_VALUES"    => $props,
);



    if ($PRODUCT_ID = $obElement->Update($_POST["extID"], $arFields)) {
        $el_res= CIBlockElement::GetByID($_POST["extID"]);

        if ($el_arr= $el_res->GetNext()) {
            //file_put_contents($_SERVER["DOCUMENT_ROOT"].'/qlog.txt', print_r($el_arr, true));
            echo($el_arr['DETAIL_PAGE_URL']);
        }
        $arFields = array(
        "ID" => $_POST["extID"],
        "VAT_INCLUDED" => "Y",
 );

        //$obElement->Update($_POST["extID"], $arFields);

        // ������������ ���� ��� ������
        $PRICE_TYPE_ID = 3;
        $PRICE_TYPE_ID_USD = 2;

        if (count($arRanges)>0) {
            foreach ($arRanges as $k=>$range_item) {
                //$nm="RANGE".$k+1;
                if ($k+1==count($arRanges)) {
                    $arFields[] = array(
        "PRODUCT_ID" => $PRODUCT_ID,
            "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
            "PRICE" => $range_item["price"],//$extproductItemsInfo->Price->ConvertedPriceWithoutSign,
            "CURRENCY" => "RUB",
            "QUANTITY_FROM" => $range_item["quantity"],
            "QUANTITY_TO" => "INF"
      );
                    $arFieldsUsd[] = array(
        "PRODUCT_ID" => $PRODUCT_ID,
            "CATALOG_GROUP_ID" => $PRICE_TYPE_ID_USD,
            "PRICE" => $range_item["price_usd"],//$extproductItemsInfo->Price->ConvertedPriceWithoutSign,
            "CURRENCY" => "USD",
            "QUANTITY_FROM" => $range_item["quantity"],
            "QUANTITY_TO" => "INF"
      );
                } else {
                    $arFields[] = array(
        "PRODUCT_ID" => $PRODUCT_ID,
            "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
            "PRICE" => $range_item["price"],//$extproductItemsInfo->Price->ConvertedPriceWithoutSign,
            "CURRENCY" => "RUB",
            "QUANTITY_FROM" => $range_item["quantity"],
            "QUANTITY_TO" => $arRanges[$k+1]["quantity"]-1,
      );
                    $arFieldsUsd[] = array(
        "PRODUCT_ID" => $PRODUCT_ID,
            "CATALOG_GROUP_ID" => $PRICE_TYPE_ID_USD,
            "PRICE" => $range_item["price_usd"],//$extproductItemsInfo->Price->ConvertedPriceWithoutSign,
            "CURRENCY" => "USD",
            "QUANTITY_FROM" => $range_item["quantity"],
            "QUANTITY_TO" => $arRanges[$k+1]["quantity"]-1,
      );
                }
            }
        } else {
            $arFields[] = array(
        //"ID" => 3,
        "PRODUCT_ID" => $PRODUCT_ID,
        "CATALOG_GROUP_ID" => $PRICE_TYPE_ID,
        "PRICE" => $masterPrice,
        "CURRENCY" => "RUB",
        //"QUANTITY_FROM" => $arRanges["RANGE3"]["quantity"],
        //"QUANTITY_TO" => "INF"
    );
            $arFieldsUsd[] = array(
        //"ID" => 3,
        "PRODUCT_ID" => $PRODUCT_ID,
        "CATALOG_GROUP_ID" => $PRICE_TYPE_ID_USD,
        "PRICE" => $masterPriceUsd,
        "CURRENCY" => "USD",
        //"QUANTITY_FROM" => $arRanges["RANGE3"]["quantity"],
        //"QUANTITY_TO" => "INF"
    );
        }

        $res = CPrice::GetList(
  array(),
  array(
    "PRODUCT_ID" => $PRODUCT_ID,
    "CATALOG_GROUP_ID" => $PRICE_TYPE_ID
  )
);

        if ($arr = $res->Fetch()) {
            foreach ($arFields as $k=>$arField) {
                CPrice::Update($arr["ID"], $arField);
            }
        } else {
            foreach ($arFields as $arField) {
                CPrice::Add($arField);
            }
        }

        $res_usd = CPrice::GetList(
  array(),
  array(
    "PRODUCT_ID" => $PRODUCT_ID,
    "CATALOG_GROUP_ID" => $PRICE_TYPE_ID_USD
  )
);



        if ($arr_usd = $res_usd->Fetch()) {
            foreach ($arFieldsUsd as $k=>$arField) {
                CPrice::Update($arr_usd["ID"], $arField);
            }
        } else {
            foreach ($arFieldsUsd as $arField) {
                CPrice::Add($arField);
            }
        }
    } else {
        //$bbb = iconv("UTF-8", "WINDOWS-1251", $_POST["extprodNAME"]);
      //  file_put_contents($_SERVER["DOCUMENT_ROOT"]."/qlog_fields.txt", print_r($arFields, true));
      //  file_put_contents($_SERVER["DOCUMENT_ROOT"]."/qlog_AA.txt", print_r($_POST, true), FILE_APPEND);
        echo "Error: ".$obElement->LAST_ERROR;
        //$el = new CIBlockElement;
        $arLoadProductArray = Array("ACTIVE" => "N");
        $res = $obElement->Update($_POST["extID"], $arLoadProductArray);
    }
}
