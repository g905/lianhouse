<?php

require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

$productId = $_POST["id"];
$productPrice = $_POST["price"];
$productPriceCurrency = $_POST["currency"];
$quantity = $_POST["quantity"];
$productName = $_POST["name"];
$comment = $_POST["comment"];

if (CModule::IncludeModule("sale"))
{
    $arFields = array(
        "PRODUCT_ID" => $productId,
        "PRODUCT_PRICE_ID" => 1,
        "PRICE" => $productPrice,
        "CURRENCY" => $productPriceCurrency,
        "QUANTITY" => $quantity,
        "LID" => SITE_ID,
        "DELAY" => "N",
        "CAN_BUY" => "Y",
        "NAME" => $productName,
        "FUSER_ID" => CSaleBasket::GetBasketUserID()
    );

    $arProps = array();

    $arProps[] = array(
        "NAME" => "�����������",
        "CODE" => "COMMENT",
        "VALUE" => $comment
    );

    $arFields["PROPS"] = $arProps;

    return CSaleBasket::Add($arFields);
}