<?php

require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");
CModule::IncludeModule("iblock");

$mail = htmlspecialchars($_POST["mail"]);
$previewText = htmlspecialchars($_POST["message"]);

if (!$mail || !$previewText || !filter_var($mail, FILTER_VALIDATE_EMAIL)) {
    die(json_encode(array('result' => 0, 'message' => 'error-fields')));
}

$el = new CIBlockElement;

$arLoadProductArray = Array(
    "MODIFIED_BY"    => $USER->GetID(),
    "IBLOCK_SECTION_ID" => false,
    "IBLOCK_ID"      => IB_SUPPORT,
    "PROPERTY_VALUES"=> array(),
    "NAME"           => $mail,
    "ACTIVE"         => "Y",
    "PREVIEW_TEXT"   => mb_convert_encoding($previewText, "WINDOWS-1251")
);

$result = ($id = $el->Add($arLoadProductArray))
    ? array('result' => 1, 'id' => $id)
    : array('result' => 0, 'message' => $el->LAST_ERROR);


echo json_encode($result);
