<?php 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require('functions.php');
require('config.php');
header("Content-Type: text/html; charset=utf-8");


CModule::IncludeModule('iblock');
$arFilter = Array('IBLOCK_ID'=>8, 'DEPTH_LEVEL' => 2);
$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true, Array('IBLOCK_ID', 'ID', 'NAME', 'UF_ID_CATEGORY', 'DEPTH_LEVEL'));

while($ar_result = $db_list->GetNext())
{
    
  $categories =  'http://otapi.net/OtapiWebService2.asmx/GetCategorySubcategoryInfoList?instanceKey=' . CFG_SERVICE_INSTANCEKEY
        . '&language=' . CFG_REQUEST_LANGUAGE
        . '&parentCategoryId=' . $ar_result['UF_ID_CATEGORY']
        . '&sessionId=&blockList=';

  $itemCategoryInfo = pars($categories)->CategoryInfoList->Content->Item;

  //$categoryName = iconv('UTF-8', 'windows-1251', (string)$itemCategoryInfo->Name);
  foreach ($itemCategoryInfo as $sub_category) {


    $sub_category_name = iconv('UTF-8', 'windows-1251', (string)$sub_category->Name);
    $sub_category_id = iconv('UTF-8', 'windows-1251', (string)$sub_category->Id);
    $parent_category_id = iconv('UTF-8', 'windows-1251', (string)$sub_category->ParentId);


    $itemSection1Id = "";
    
    $arSelect = Array("ID");
    $arFilter = Array(
        "IBLOCK_ID" => 8,
        "UF_ID_CATEGORY" => $sub_category_id
    );
    $res = CIBlockSection::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arSelect);
    while($ob = $res->GetNextElement())
    {
        $arFields = $ob->GetFields();
        $arProperties = $ob->GetProperties();
      
        $itemSection1Id = $arFields["ID"];
    }

    if (empty($itemSection1Id)) {

    	if ($parent_category_id == $ar_result['UF_ID_CATEGORY']) {
      
	      $el = new CIBlockSection;

	      $arFields = [
	          'NAME'              => $sub_category_name,
	          'IBLOCK_ID'         => 8,
	          'ACTIVE'            => 'Y',
	          'IBLOCK_SECTION_ID' => $ar_result['ID'],
	          'UF_ID_CATEGORY' => $sub_category_id,
	      	];


	      $itemSection2Id = $el->Add($arFields);

	      echo "Новый раздел: ".$itemSection2Id."<br />";
	    }
    }
    /*else{

    	echo "Уже создан: ".$itemSection1Id."<br />";
    }*/


  }


}

