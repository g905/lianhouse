<?php
require('config.php');

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;use HL\HighloadBlockTable;
CModule::IncludeModule("highloadblock"); //
CModule::IncludeModule('iblock');

function pars($url, $data=[])
{

  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HEADER, 0);
  // ������ ����� Fiddler
  //curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');

  $result = curl_exec($curl);

  $today = date("d m Y H:i:s")."\n";
  //file_put_contents($_SERVER["DOCUMENT_ROOT"].'/otapi_log.txt', $today, FILE_APPEND);
  $log_data = $today.(string)$url."\n";
  //file_put_contents($_SERVER["DOCUMENT_ROOT"].'/otapi_log.txt', $log_data, FILE_APPEND);

  if ($data):
    //file_put_contents($_SERVER["DOCUMENT_ROOT"].'/otapi_log.txt', print_r($data, true), FILE_APPEND);
  endif;

  if ($result === false) {
      echo "cURL Error: " . curl_error($curl); die();
  }
  $xmlObject = simplexml_load_string($result);
  curl_close($curl);

  if ((string)$xmlObject->ErrorCode !== 'Ok') {
	  //echo "Error: " . $xmlObject->ErrorDescription; /*die();*/
  }

  return $xmlObject;
}

//������ �����
function writeToFile($fileName, $arr)
{

	$fp = fopen($fileName, 'w');

	fputcsv($fp, $arr);

	fclose($fp);
}

//�������� id (api) ��������� �� ������� �����������
function getCategoriesId($depthLevel)
{

	$arFilter = Array('IBLOCK_ID' => 8, 'DEPTH_LEVEL' => $depthLevel);
	$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true, Array('IBLOCK_ID ', 'UF_ID_CATEGORY'));

	$ids = [];
	$count = 0;
	$i = 1;

	while($ar_result = $db_list->GetNext())
	{
		$count++;

		$ids[] = $ar_result['UF_ID_CATEGORY'];

		/*if ($count == 10) {
			writeToFile('categoryId/CategoryIdSub'. $i++ .'.csv', $ids);

			$count = 0;
			$ids = [];
			continue;
		}*/

	}

	return $ids;
}

//�������� id (api) ���� ���������
function getAllCategoriesId()
{

  $arFilter = Array('IBLOCK_ID' => 8);
  $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true, Array('IBLOCK_ID ', 'UF_ID_CATEGORY'));

  $ids = [];
  $count = 0;
  $i = 1;

  while($ar_result = $db_list->GetNext()) {
    $count++;

    $ids[] = $ar_result['UF_ID_CATEGORY'];

   }

  return $ids;
}

//�������� id ����� ���������
function getCategory($id) {

  $arFilter = Array('IBLOCK_ID' => 8, 'UF_ID_CATEGORY' => $id);
  $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true, Array('IBLOCK_ID ', 'UF_ID_CATEGORY'));

  while($ar_result = $db_list->GetNext()) {

    $id = $ar_result['ID'];

   }

   return $id;
}

//�������� id (api) ���� ������� �� ���������� ���������
function getProduct($cat, $count, &$productId = [])
{

    $count = $count + 50;

    if ($count < 3000) {

	    $productItems = 'http://otapi.net/OtapiWebService2.asmx/FindCategoryItemSimpleInfoListFrame?instanceKey=' . CFG_SERVICE_INSTANCEKEY
	    . '&language=' . CFG_REQUEST_LANGUAGE
	   . '&categoryId=' . $cat
	   . '&categoryItemFilter='
	   . '&framePosition=' . $count
	   . '&frameSize=' . '50'
	   . '&sessionId=&blockList=';

	    $productItemsInfo = pars($productItems)->OtapiItemInfoSubList->Content->Item;
    } else {
    	unset($productItemsInfo);
    }

    if (count($productItemsInfo) > 0){

        foreach ($productItemsInfo as $product) {

            $productId[] = (string)$product->Id;

        }

        getProduct($cat, $count, $productId);

        return $productId;

    } else {
        return $productId;
    }

}

//�������� ������ ���������� � ������
function getProductFull($id)
{
  $productItems = 'http://otapi.net/OtapiWebService2.asmx/BatchGetItemFullInfo?instanceKey=' . CFG_SERVICE_INSTANCEKEY
  . '&language=' . CFG_REQUEST_LANGUAGE
  . '&itemId=' . $id
  . '&sessionId=&blockList=';

  $productInfo = pars($productItems)->Result->Item;

  return $productInfo;
}

//�������� ��� ������ ������ � ������� HighloadBlock
function getEntityDataClass($hlbl)
{

    $hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    $entityDataClass = $entity->getDataClass();

    return $entityDataClass;
}

function refreshPrices ($product, $ext_cat) {
  if(!is_array($product)) $product[] = $product;

  foreach($product as $p){
      $query_str .= $p["art"].";";
  }

  $query = 'http://otapi.net/OtapiWebService2.asmx/GetItemInfoList?'
      . 'instanceKey='. CFG_SERVICE_INSTANCEKEY
      . '&language=' . CFG_REQUEST_LANGUAGE
      . '&idsList=' . $query_str;
if ($_GET["test"]>0){
echo $query;
}

      $a = pars($query);
$objects = [];
foreach($product as $index=>$p){
  //print_r($p);
  foreach($a->OtapiItemInfoList->Content->Item as $item){
    //echo (boolean)$item->HasError;
    if((string)$item->HasError !== "false") continue;
    if ((string)$item->Id === $p["art"]) {
      $flag = false;
      foreach($p["prices"]["MATRIX"] as $key=>$price){
        foreach($price as $pr) {
          $prs[] = $pr["PRICE"];
          //echo "<b>".$pr["PRICE"]." - ".$item->Price->ConvertedPriceWithoutSign." = ".($pr["PRICE"] - (float)$item->Price->ConvertedPriceWithoutSign)."</b><br>";
          if (abs(($pr["PRICE"] - (float)$item->Price->ConvertedPriceWithoutSign)) == 0) {
            $flag=false;
            break;
          } else {
            $flag=true;
            $objects[$p["art"]] = array("id"=>$p["id"], "art"=>$p["art"], "name"=>mb_convert_encoding($item->Title, "Windows-1251"), "old"=>$price, "new"=>$item->Price->ConvertedPriceWithoutSign);
            continue;
          }
        }
      }
    }
  }
}
if ($_GET["test"]>0){
echo "<pre>";
print_r($objects);
echo "</pre>";
}
$count=0;

foreach ($objects as $o){

  $count++;

  $myCurl = curl_init();

  curl_setopt_array($myCurl, array(
      //CURLOPT_URL => $_SERVER["SERVER_NAME"].'/local/ajax/iaddextprod.php',
      CURLOPT_URL => $_SERVER["SERVER_NAME"].'/local/ajax/ipricerefresh.php',
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_POST => true,
      CURLOPT_POSTFIELDS => http_build_query(array("id" => $o["id"], "iblockID"=>8, "categoryID"=>$ext_cat, "extprodID"=>$o["art"], "extprodNAME"=>$o["name"])),
  ));
$response = curl_exec($myCurl);
curl_close($myCurl);
//sleep(1);
}
}
