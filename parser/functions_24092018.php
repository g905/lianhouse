<?php
require('config.php');

use Bitrix\Highloadblock as HL;
use Bitrix\Main\Entity;use HL\HighloadBlockTable;
CModule::IncludeModule("highloadblock"); // подключить инфоблоки
CModule::IncludeModule('iblock');

function pars($url, $data=[])
{

  $curl = curl_init();
  curl_setopt($curl, CURLOPT_URL, $url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_HEADER, 0);
  // Прокси через Fiddler
  //curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');

  $result = curl_exec($curl);

  $today = date("d m Y H:i:s")."\n";
  //file_put_contents($_SERVER["DOCUMENT_ROOT"].'/otapi_log.txt', $today, FILE_APPEND);
  $log_data = $today.(string)$url."\n";
	//file_put_contents($_SERVER["DOCUMENT_ROOT"].'/otapi_log.txt', $log_data, FILE_APPEND);

  if ($data):
    //file_put_contents($_SERVER["DOCUMENT_ROOT"].'/otapi_log.txt', print_r($data, true), FILE_APPEND);
  endif;

  if ($result === false) {
      echo "cURL Error: " . curl_error($curl); die();
  }
  $xmlObject = simplexml_load_string($result);
  curl_close($curl);

  if ((string)$xmlObject->ErrorCode !== 'Ok') {
	  echo "Error: " . mb_convert_encoding($xmlObject->ErrorDescription, "CP1251"); /*die();*/
  }

  return $xmlObject;
}

//запись файла
function writeToFile($fileName, $arr)
{

	$fp = fopen($fileName, 'w');

	fputcsv($fp, $arr);

	fclose($fp);
}

//получить id (api) категорий по уровням вложенности
function getCategoriesId($depthLevel)
{

	$arFilter = Array('IBLOCK_ID' => 8, 'DEPTH_LEVEL' => $depthLevel);
	$db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true, Array('IBLOCK_ID ', 'UF_ID_CATEGORY'));

	$ids = [];
	$count = 0;
	$i = 1;

	while($ar_result = $db_list->GetNext())
	{
		$count++;

		$ids[] = $ar_result['UF_ID_CATEGORY'];

		/*if ($count == 10) {
			writeToFile('categoryId/CategoryIdSub'. $i++ .'.csv', $ids);

			$count = 0;
			$ids = [];
			continue;
		}*/

	}

	return $ids;
}

//получить id (api) всех категорий
function getAllCategoriesId()
{

  $arFilter = Array('IBLOCK_ID' => 8);
  $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true, Array('IBLOCK_ID ', 'UF_ID_CATEGORY'));

  $ids = [];
  $count = 0;
  $i = 1;

  while($ar_result = $db_list->GetNext()) {
    $count++;

    $ids[] = $ar_result['UF_ID_CATEGORY'];

   }

  return $ids;
}

//получить id одной категории
function getCategory($id) {

  $arFilter = Array('IBLOCK_ID' => 8, 'UF_ID_CATEGORY' => $id);
  $db_list = CIBlockSection::GetList(Array($by=>$order), $arFilter, true, Array('IBLOCK_ID ', 'UF_ID_CATEGORY'));

  while($ar_result = $db_list->GetNext()) {

    $id = $ar_result['ID'];

   }

   return $id;
}

//получить id (api) всех товаров из переданной категории
function getProduct($cat, $count, &$productId = [])
{

    $count = $count + 50;

    if ($count < 3000) {

	    $productItems = 'http://otapi.net/OtapiWebService2.asmx/FindCategoryItemSimpleInfoListFrame?instanceKey=' . CFG_SERVICE_INSTANCEKEY
	    . '&language=' . CFG_REQUEST_LANGUAGE
	   . '&categoryId=' . $cat
	   . '&categoryItemFilter='
	   . '&framePosition=' . $count
	   . '&frameSize=' . '50'
	   . '&sessionId=&blockList=';

	    $productItemsInfo = pars($productItems)->OtapiItemInfoSubList->Content->Item;
    } else {
    	unset($productItemsInfo);
    }

    if (count($productItemsInfo) > 0){

        foreach ($productItemsInfo as $product) {

            $productId[] = (string)$product->Id;

        }

        getProduct($cat, $count, $productId);

        return $productId;

    } else {
        return $productId;
    }

}

//получить полную информацию о товаре
function getProductFull($id)
{
  $productItems = 'http://otapi.net/OtapiWebService2.asmx/BatchGetItemFullInfo?instanceKey=' . CFG_SERVICE_INSTANCEKEY
  . '&language=' . CFG_REQUEST_LANGUAGE
  . '&itemId=' . $id
  . '&sessionId=&blockList=';

  $productInfo = pars($productItems)->Result->Item;

  return $productInfo;
}

//получить все методы работы с блоками HighloadBlock
function getEntityDataClass($hlbl)
{

    $hlblock = HL\HighloadBlockTable::getById($hlbl)->fetch();
    $entity = HL\HighloadBlockTable::compileEntity($hlblock);
    $entityDataClass = $entity->getDataClass();

    return $entityDataClass;
}
