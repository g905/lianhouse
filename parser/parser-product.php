<?php 
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
require('functions.php');
require('config.php');

CModule::IncludeModule("highloadblock"); // подключить инфоблоки
header("Content-Type: text/html; charset=utf-8");


$arResult = [];
$productId = [];

//Подключаем функции классов
$highloadblock_id_2 = getEntityDataClass(2);

//Параметры фильтрации
$arOrder = Array("ID" => "ASC");
$arSelect = Array("*");
$arFilter = Array(
    "UF_PROCESSED" => 0,
);

//Отправляем запрос
$rsData = $highloadblock_id_2::getList(Array("select" => $arSelect, "filter" => $arFilter, "limit" => 1, "order" => $arOrder));
 
while ($result = $rsData->Fetch()) {

	$product = getProductFull($result['UF_PRODUCT_ID']);

	$categoryId = getCategory($result['UF_ID_CATEGORY']);

	$imageUrl = [];
	$images = [];
	$characteristics = [];
	$prop = [];
	$propertyName = [];
	$propertyValue = [];


	foreach ($product->Pictures->ItemPicture as $image) {
		$imageUrl[] = (string)$image->Url;

		$images[] = CFile::MakeFileArray((string)$image->Url);
	}

	foreach ($product->Attributes->ItemAttribute as $attribute) {

		if ((string)$attribute->IsConfigurator === 'false') {
			$propertyName = mb_convert_encoding($attribute->PropertyName, 'windows-1251', 'UTF-8');
			$propertyValue = mb_convert_encoding((string)$attribute->Value, 'windows-1251', 'UTF-8');

			$characteristics[] = ['VALUE' => $propertyName, 'DESCRIPTION' => $propertyValue];
		}
	}

	dd($product);

	$prop['MAIN_PHOTO'] = (string)$product->MainPictureUrl;
	$prop['MORE_PHOTO_LINK'] = $imageUrl;
	$prop['MORE_PHOTO'] = $images;
	$prop['ARTICLE'] = $product->Id;
	$prop['CHARACTERISTICS'] = $characteristics;
	$prop['MIN_QUANTITY'] = (string)$product->FirstLotQuantity;
	$prop['NEXT_LOT_QUANTITY'] = (string)$product->NextLotQuantity;
	$prop['VENDOR_ID'] = (string)$product->VendorId;
	$prop['VENDOR_NAME'] = (string)$product->VendorName;

	//dd($image);
	
    $itemName = iconv('UTF-8', 'windows-1251', (string)$product->Title);
    
    $arFields = [
        'NAME'              => $itemName,
        'CODE'				=> (string)$product->Id,
        'IBLOCK_ID'         => 8,
        'ACTIVE'            => 'Y',
        'DATE_CREATE' 		=> date("d.m.Y H:i:s"),
        'CREATED_BY' 		=> $GLOBALS['USER']->GetID(),
        'IBLOCK_SECTION' 	=> $categoryId,
        'PROPERTY_VALUES' 	=> $prop,
        'PREVIEW_PICTURE'	=> CFile::MakeFileArray((string)$product->MainPictureUrl),
        'DETAIL_PICTURE' 	=> CFile::MakeFileArray((string)$product->MainPictureUrl),
    ];

    $el = new CIBlockElement;

    if ($last_el_id = $el->Add($arFields)) {

        echo 'New ID: ' . $last_el_id . '<br>';

      	$arFields = array(
			'ID' => $last_el_id, 
			'VAT_INCLUDED' => 'Y',
			'QUANTITY' => (string)$product->MasterQuantity,
      	);

		if (CCatalogProduct::Add($arFields)) {

	        echo "Добавили параметры товара к элементу каталога " . $last_el_id . '<br>';
	         
	        $arFields = Array(
	            'PRODUCT_ID' => $last_el_id,
	            'CATALOG_GROUP_ID' => 3,
	            'PRICE' => (string)$product->Price->ConvertedPriceWithoutSign,
	            'CURRENCY' => "RUB",
	        );
	        CPrice::Add($arFields);
	    }

    } else {

        echo 'Error: ' . $el_ob->LAST_ERROR  . '<br>';
    }
}


